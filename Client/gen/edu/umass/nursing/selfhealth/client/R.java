/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package edu.umass.nursing.selfhealth.client;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int accent_dark_gray=0x7f040003;
        public static final int accent_light_gray=0x7f040002;
        public static final int accent_red=0x7f040004;
        public static final int action_bar=0x7f040005;
        /** *** PATIENT THEME COLORS ***
         */
        public static final int background=0x7f040000;
        public static final int background_header=0x7f040019;
        public static final int background_light=0x7f04000c;
        public static final int background_ultralight=0x7f04000b;
        /**  SH DEFINITIONS 
         */
        public static final int background_white=0x7f04000a;
        public static final int content_divider=0x7f04001d;
        public static final int dark_text=0x7f040009;
        public static final int foreground=0x7f040001;
        public static final int graph_line=0x7f04001b;
        public static final int graph_point_selected=0x7f04001c;
        /**  <color name="accent_red">#69D2E7</color> 
*** PATIENT TEXT COLORS ***
         */
        public static final int light_text=0x7f040008;
        public static final int list_divider=0x7f04001a;
        public static final int progress_fill_color=0x7f040007;
        public static final int row_background=0x7f04000d;
        public static final int row_pressed=0x7f04000f;
        public static final int row_selected=0x7f04000e;
        public static final int take_button_background=0x7f040010;
        public static final int take_button_pressed=0x7f040012;
        public static final int take_button_selected=0x7f040011;
        public static final int text_black=0x7f040013;
        public static final int text_dark=0x7f040014;
        public static final int text_light=0x7f040016;
        public static final int text_medium=0x7f040015;
        public static final int text_theme_accent=0x7f040018;
        public static final int text_white=0x7f040017;
        public static final int theme_color=0x7f040006;
    }
    public static final class dimen {
        public static final int action_bar_height=0x7f05000f;
        public static final int action_bar_text_size=0x7f05000e;
        /**  PORTED FROM PROVIDER 

         Example customization of dimensions originally defined in res/values/dimens.xml
         (such as screen margins) for screens with more than 820dp of available width. This
         would include 7" and 10" devices in landscape (~960dp and ~1280dp respectively).
    

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050003;
        public static final int activity_vertical_margin=0x7f050004;
        public static final int big_text=0x7f050002;
        public static final int button_left_of_toggle_margin_right=0x7f05000c;
        public static final int button_text_padding=0x7f05000b;
        public static final int button_text_size=0x7f05000a;
        public static final int button_top_padding=0x7f05000d;
        public static final int height_schedule_list=0x7f05003a;
        public static final int image_size_medication_menu_icon=0x7f050031;
        public static final int image_size_menu_icon=0x7f050030;
        public static final int image_size_schedule_icon=0x7f05002f;
        public static final int inset_list_divider=0x7f050039;
        public static final int instruction_text_size=0x7f050006;
        public static final int large_font_size=0x7f050010;
        public static final int large_font_zise=0x7f050011;
        public static final int margin_large=0x7f050038;
        public static final int margin_medium=0x7f050037;
        public static final int margin_tight=0x7f050036;
        public static final int margin_top=0x7f050013;
        public static final int medium_text=0x7f050012;
        public static final int padding_large=0x7f050034;
        public static final int padding_left_right_instruction_textview=0x7f050005;
        public static final int padding_medium=0x7f050033;
        public static final int padding_placeholder=0x7f050035;
        public static final int padding_tight=0x7f050032;
        public static final int rectangle_corner_radius=0x7f050007;
        /**  Default screen margins, per the Android Design guidelines. 
 <dimen name="activity_horizontal_margin">16dp</dimen>
    <dimen name="activity_vertical_margin">16dp</dimen> 
         */
        public static final int reminder_horizontal_margin=0x7f050000;
        public static final int reminder_vertical_margin=0x7f050001;
        public static final int row_divider_height=0x7f050008;
        public static final int row_height=0x7f050009;
        public static final int subtitle_text_size=0x7f050014;
        public static final int text_size_confirm=0x7f05002e;
        public static final int text_size_header_subtitle=0x7f050016;
        /**  SH DEFINITIONS 
         */
        public static final int text_size_header_title=0x7f050015;
        public static final int text_size_health_record_component=0x7f050027;
        public static final int text_size_health_record_date_time=0x7f050029;
        public static final int text_size_health_record_measurement=0x7f05002a;
        public static final int text_size_health_record_unit=0x7f050028;
        public static final int text_size_measurement_pane_edit_text=0x7f05002b;
        public static final int text_size_medication_header_dosage=0x7f050026;
        public static final int text_size_medication_header_name=0x7f050025;
        public static final int text_size_medication_menu_item_dosage=0x7f050024;
        public static final int text_size_medication_menu_item_name=0x7f050023;
        public static final int text_size_medication_record_date=0x7f050020;
        public static final int text_size_medication_record_date_detail=0x7f050021;
        public static final int text_size_medication_record_time=0x7f050022;
        public static final int text_size_menu_item_description=0x7f050017;
        public static final int text_size_placeholder=0x7f05002c;
        public static final int text_size_schedule_item_description=0x7f050018;
        public static final int text_size_schedule_item_time=0x7f050019;
        public static final int text_size_survey_button=0x7f05001f;
        public static final int text_size_survey_question=0x7f05001d;
        public static final int text_size_survey_record_date=0x7f05001a;
        public static final int text_size_survey_record_score=0x7f05001c;
        public static final int text_size_survey_record_time=0x7f05001b;
        public static final int text_size_survey_response=0x7f05001e;
        public static final int text_size_take_survey_button=0x7f05002d;
        public static final int thickness_content_divider=0x7f05003d;
        public static final int thickness_graph_line=0x7f05003c;
        public static final int thickness_list_divider=0x7f05003b;
    }
    public static final class drawable {
        public static final int activity_icon=0x7f020000;
        public static final int back_icon=0x7f020001;
        public static final int blood_glucose_icon=0x7f020002;
        public static final int blood_oxygen_icon=0x7f020003;
        public static final int blood_pressure_icon=0x7f020004;
        public static final int border=0x7f020005;
        public static final int border_no_top=0x7f020006;
        public static final int border_thick=0x7f020007;
        public static final int check_icon_gray=0x7f020008;
        public static final int check_icon_white_fill=0x7f020009;
        public static final int clear=0x7f02000a;
        public static final int clock_icon_gray=0x7f02000b;
        public static final int clock_icon_white_fill=0x7f02000c;
        public static final int color_row_selector=0x7f02000d;
        public static final int color_text_selector=0x7f02000e;
        public static final int edit=0x7f02000f;
        public static final int facebook_icon=0x7f020010;
        public static final int forward_arrow=0x7f020011;
        public static final int graph_gradient=0x7f020012;
        public static final int happy_frame_icon=0x7f020013;
        public static final int health_icon_outline=0x7f020014;
        public static final int health_icon_white_fill=0x7f020015;
        public static final int heart_rate_icon_fill=0x7f020016;
        public static final int home_icon=0x7f020017;
        public static final int ic_launcher=0x7f020018;
        public static final int ic_stat_dismiss=0x7f020019;
        public static final int ic_stat_notification=0x7f02001a;
        public static final int ic_stat_snooze=0x7f02001b;
        public static final int lumos_patient_icon=0x7f02001c;
        public static final int meal_icon=0x7f02001d;
        public static final int medication_icon_outline=0x7f02001e;
        public static final int medication_icon_white_fill=0x7f02001f;
        public static final int note_big=0x7f020020;
        public static final int popup_black=0x7f020021;
        public static final int project=0x7f020022;
        public static final int provider_icon_gray=0x7f020023;
        public static final int sh_activity=0x7f020024;
        public static final int sh_adl=0x7f020025;
        public static final int sh_attitude=0x7f020026;
        public static final int sh_blood_glucose=0x7f020027;
        public static final int sh_blood_oxygen=0x7f020028;
        public static final int sh_blood_pressure=0x7f020029;
        public static final int sh_button_selector=0x7f02002a;
        public static final int sh_completed=0x7f02002b;
        public static final int sh_contacts=0x7f02002c;
        public static final int sh_empty_completed=0x7f02002d;
        public static final int sh_empty_history=0x7f02002e;
        public static final int sh_empty_schedule=0x7f02002f;
        public static final int sh_export=0x7f020030;
        public static final int sh_frame_thin=0x7f020031;
        public static final int sh_health=0x7f020032;
        public static final int sh_heart_rate=0x7f020033;
        public static final int sh_iadl=0x7f020034;
        public static final int sh_inset_list_divider=0x7f020035;
        public static final int sh_jads=0x7f020036;
        public static final int sh_lsns=0x7f020037;
        public static final int sh_meal=0x7f020038;
        public static final int sh_medication=0x7f020039;
        public static final int sh_row_selector=0x7f02003a;
        public static final int sh_schedule=0x7f02003b;
        public static final int sh_socs=0x7f02003c;
        public static final int sh_surveys=0x7f02003d;
        public static final int sh_water=0x7f02003e;
        public static final int sh_weight=0x7f02003f;
        public static final int skype_icon=0x7f020040;
        public static final int sun=0x7f020041;
        public static final int survey_icon_outline=0x7f020042;
        public static final int survey_icon_white_fill=0x7f020043;
        public static final int water_fill=0x7f020044;
        public static final int weight_icon=0x7f020045;
    }
    public static final class id {
        public static final int BackArrow=0x7f080001;
        public static final int BackButton=0x7f080000;
        public static final int BackText=0x7f080002;
        public static final int ComponentText=0x7f08001d;
        public static final int DateText=0x7f08001f;
        public static final int HomeButton=0x7f080003;
        public static final int HomeButtonIcon=0x7f080004;
        public static final int LinearLayout1=0x7f08002e;
        public static final int MedicationDosageButton=0x7f080011;
        public static final int MedicationDosageLabel=0x7f080013;
        public static final int MedicationFormButton=0x7f08000e;
        public static final int MedicationFormLabel=0x7f080010;
        public static final int MedicationLabel=0x7f08000b;
        public static final int MedicationNameContainer=0x7f08000d;
        public static final int MedicationNameEditButton=0x7f08001a;
        public static final int MedicationNameLabel=0x7f08001b;
        public static final int MedicationQuantityButton=0x7f080017;
        public static final int MedicationQuantityLabel=0x7f080019;
        public static final int MedicationUnitButton=0x7f080014;
        public static final int MedicationUnitLabel=0x7f080016;
        public static final int RelativeLayout1=0x7f080006;
        public static final int TextView02=0x7f080018;
        public static final int TextView04=0x7f080012;
        public static final int TextView06=0x7f08000f;
        public static final int TitleText=0x7f080005;
        public static final int UnitText=0x7f08001e;
        public static final int ValueText=0x7f080020;
        public static final int add=0x7f080046;
        public static final int componentText=0x7f08004d;
        public static final int confirmButton=0x7f080030;
        public static final int confirmationButton=0x7f08003a;
        public static final int confirmationPane=0x7f08003f;
        public static final int confirmationPrompt=0x7f080039;
        public static final int confirmationSubtitle=0x7f08003c;
        public static final int confirmationTitle=0x7f08003b;
        public static final int contentContainer=0x7f08002d;
        public static final int contentDivider=0x7f08002b;
        public static final int contentPanel=0x7f08002c;
        public static final int decimal=0x7f080025;
        public static final int facebook=0x7f08006a;
        public static final int header=0x7f080045;
        public static final int headerSubtitle=0x7f08004a;
        public static final int headerTitle=0x7f080049;
        public static final int healthComponent=0x7f080055;
        public static final int healthComponentMeasurement=0x7f080054;
        public static final int healthComponentUnit=0x7f080056;
        public static final int healthRecordComponent=0x7f080057;
        public static final int healthRecordComponentContainer=0x7f080052;
        public static final int healthRecordDateTime=0x7f080053;
        public static final int healthRecordMeasurement=0x7f080058;
        public static final int healthRecordUnit=0x7f080059;
        public static final int horizontalScrollView=0x7f080041;
        public static final int hundreds=0x7f080027;
        public static final int hundredths=0x7f080029;
        public static final int imageButton1=0x7f08006b;
        public static final int instructionText=0x7f080007;
        public static final int measurementComponentText=0x7f080022;
        public static final int measurementContainer=0x7f080042;
        public static final int measurementEdit=0x7f08004f;
        public static final int measurementEditText=0x7f080023;
        public static final int measurementsContainer=0x7f08000a;
        public static final int measurementsScrollView=0x7f080008;
        public static final int medicationHeader=0x7f080044;
        public static final int medicationHeaderDosage=0x7f08004b;
        public static final int medicationHeaderName=0x7f08004c;
        public static final int medicationMenuDosage=0x7f08005b;
        public static final int medicationMenuIcon=0x7f08005a;
        public static final int medicationMenuName=0x7f08005c;
        public static final int medicationRecordDate=0x7f08005e;
        public static final int medicationRecordDetail=0x7f08005d;
        public static final int medicationRecordTime=0x7f08005f;
        public static final int medication_buttons=0x7f08000c;
        public static final int menuItemDescription=0x7f080061;
        public static final int menuItemIcon=0x7f080060;
        public static final int message=0x7f08003e;
        public static final int nextButton=0x7f080037;
        public static final int ones=0x7f080026;
        public static final int placeholderImage=0x7f080051;
        public static final int placeholderText=0x7f080050;
        public static final int previousButton=0x7f080036;
        public static final int questionBody=0x7f080048;
        public static final int questionHeader=0x7f080047;
        public static final int questionPanel=0x7f080032;
        public static final int questionResponseContainer=0x7f080038;
        public static final int reminderTitle=0x7f08002f;
        public static final int responsePanel=0x7f080034;
        public static final int responsePanelContainer=0x7f080033;
        public static final int right_arrow=0x7f08001c;
        public static final int row_background=0x7f080021;
        public static final int saveButton=0x7f080009;
        public static final int scheduleItemDescription=0x7f080063;
        public static final int scheduleItemIcon=0x7f080062;
        public static final int scheduleItemTime=0x7f080064;
        public static final int scrollView=0x7f080043;
        public static final int sidePanel=0x7f08002a;
        public static final int skype=0x7f080069;
        public static final int snoozeButton=0x7f080031;
        public static final int surveyButtonContainer=0x7f080035;
        public static final int surveyRecordDate=0x7f080066;
        public static final int surveyRecordDetail=0x7f080065;
        public static final int surveyRecordScore=0x7f080067;
        public static final int surveyResponse=0x7f080068;
        public static final int takeSurveyButton=0x7f080040;
        public static final int tens=0x7f080028;
        public static final int tenths=0x7f080024;
        public static final int textView1=0x7f080015;
        public static final int title=0x7f08003d;
        public static final int unitText=0x7f08004e;
    }
    public static final class layout {
        public static final int action_bar=0x7f030000;
        public static final int activity_graph_test=0x7f030001;
        public static final int activity_take_measurement=0x7f030002;
        public static final int fragment_medication=0x7f030003;
        public static final int graph_label=0x7f030004;
        public static final int measurement_row=0x7f030005;
        public static final int number_picker_dosage=0x7f030006;
        public static final int number_picker_quantity=0x7f030007;
        public static final int sh_activity_dual_pane=0x7f030008;
        public static final int sh_activity_home=0x7f030009;
        public static final int sh_activity_reminder_popup=0x7f03000a;
        public static final int sh_activity_single_pane=0x7f03000b;
        public static final int sh_activity_survey=0x7f03000c;
        public static final int sh_confirmation_pane=0x7f03000d;
        public static final int sh_dialog_prn_prompt=0x7f03000e;
        public static final int sh_fragment_confirmation=0x7f03000f;
        public static final int sh_fragment_health_history=0x7f030010;
        public static final int sh_fragment_measurement_landscape=0x7f030011;
        public static final int sh_fragment_measurement_portrait=0x7f030012;
        public static final int sh_fragment_medication_history=0x7f030013;
        public static final int sh_fragment_menu=0x7f030014;
        public static final int sh_fragment_schedule=0x7f030015;
        public static final int sh_fragment_survey_history=0x7f030016;
        public static final int sh_fragment_survey_question=0x7f030017;
        public static final int sh_fragment_survey_responses=0x7f030018;
        public static final int sh_header=0x7f030019;
        public static final int sh_header_medication=0x7f03001a;
        public static final int sh_measurement_pane=0x7f03001b;
        public static final int sh_placeholder_completed=0x7f03001c;
        public static final int sh_placeholder_history=0x7f03001d;
        public static final int sh_placeholder_schedule=0x7f03001e;
        public static final int sh_row_health_metric_record=0x7f03001f;
        public static final int sh_row_health_metric_record_component=0x7f030020;
        public static final int sh_row_health_record=0x7f030021;
        public static final int sh_row_medication_menu_item=0x7f030022;
        public static final int sh_row_medication_record=0x7f030023;
        public static final int sh_row_menu_item=0x7f030024;
        public static final int sh_row_schedule_item=0x7f030025;
        public static final int sh_row_survey_record=0x7f030026;
        public static final int sh_row_survey_response=0x7f030027;
        public static final int sh_socialmedia_dialog=0x7f030028;
    }
    public static final class string {
        public static final int action_settings=0x7f060001;
        public static final int app_name=0x7f060000;
        public static final int dialog_message=0x7f060008;
        /**  dummy text 
         */
        public static final int dialog_title=0x7f060007;
        public static final int hello_world=0x7f060002;
        public static final int sleep_error=0x7f060005;
        /**  logging 
         */
        public static final int snoozing=0x7f060003;
        public static final int timer_finished=0x7f060006;
        public static final int timer_start=0x7f060004;
    }
    public static final class style {
        /**  Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here. 

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070001;
        /**  Application theme. 
         */
        public static final int AppTheme=0x7f070002;
        public static final int BodyText=0x7f070003;
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    
 <item name="android:windowActionBar">false</item>
        <item name="android:windowNoTitle">false</item> 
         */
        public static final int NoActionBar=0x7f070000;
        public static final int NotificationText=0x7f070004;
        public static final int NotificationTitle=0x7f070005;
        public static final int Theme_Transparent=0x7f070006;
    }
}

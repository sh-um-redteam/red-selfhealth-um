/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricComponent;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;
import edu.umass.nursing.selfhealth.client.viewComponents.SHMeasurementPane;

public class SHMeasurementFragment extends Fragment {
	
	private LinearLayout measurementContainer;
	private ArrayList<SHMeasurementPane> measurementPanes = new ArrayList<SHMeasurementPane>();
	private ArrayList<String> temporaryMeasurementStrings;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
    	View rootView;
    		
    	if(landscape()) {
    		// LANDSCAPE
    		rootView = inflater.inflate(R.layout.sh_fragment_measurement_landscape, container, false);
    		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
    	} else {
    		rootView = inflater.inflate(R.layout.sh_fragment_measurement_portrait, container, false);
    		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    	}
    	
        measurementContainer = (LinearLayout) rootView.findViewById(R.id.measurementContainer);
        
        if(savedInstanceState != null) {
        	temporaryMeasurementStrings = savedInstanceState.getStringArrayList("TEMP_STRINGS");
        } else {
        	temporaryMeasurementStrings = new ArrayList<String>();
        }
        return rootView;
    }
    
    @Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		temporaryMeasurementStrings.clear();
		for(int i = 0; i < measurementPanes.size(); i++) {
			temporaryMeasurementStrings.add(measurementPanes.get(i).getMeasurement());
		}
		outState.putSerializable("TEMP_STRINGS", temporaryMeasurementStrings);
	}
    
    // Called by the containing activity
    public void initializeMeasurementComponentPanes(SHHealthMetricID healthMetricID) {
    	
    	measurementContainer.removeAllViews();
    	
    	ArrayList<SHHealthMetricComponent> components = SHHealthMetricID.generateHealthMetric(healthMetricID).components();
    	
    	for(int i = 0; i < components.size(); i++) {
    		SHMeasurementPane pane = new SHMeasurementPane(getActivity(), null, landscape());
    		SHHealthMetricComponent component = components.get(i);
    		pane.setComponent(component.toString());
    		pane.setUnit(component.unit().toString());
    		pane.setTag(component);
    		
    		if(temporaryMeasurementStrings.size() > 0) {
    			pane.restoreMeasurement(temporaryMeasurementStrings.get(i));
    		}
    		
    		measurementPanes.add(pane);
    		measurementContainer.addView(pane);
    	}
    	measurementContainer.requestFocus();
    }
    
    // Called by the containing activity when the user attempts to save the measurements
    public HashMap<SHHealthMetricComponent, Number> getMeasurements() {
    	
    	HashMap<SHHealthMetricComponent, Number> measurements = new HashMap<SHHealthMetricComponent, Number>();
    	for(SHMeasurementPane pane : measurementPanes) {
    		
    		try{
    			double value = Double.valueOf(pane.getMeasurement());
    			measurements.put((SHHealthMetricComponent) pane.getTag(), value);
    		} catch (NumberFormatException e) {
    			
					// Notify the User that they should fix the mistake
					Toast toast = Toast.makeText(getActivity(), "Enter a valid " + ((SHHealthMetricComponent) pane.getTag()).toString() + " value.", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					LinearLayout toastLayout = (LinearLayout) toast.getView();
					TextView toastTV = (TextView) toastLayout.getChildAt(0);
					toastTV.setTextSize(30);
					toast.show();
					return null;
    		}
    	}
    	return measurements;
    }
    
    private boolean landscape() {
    	int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
    	if(rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270)
    		return true;
    	return false;
    }
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHHealthOverviewActivity;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;
import edu.umass.nursing.selfhealth.client.tools.SHHealthMetricTool;
import edu.umass.nursing.selfhealth.client.viewComponents.SHMenuRow;

public class SHHealthMenuFragment extends ListFragment {
	
	private int selectedIndex;
	private HealthMenuAdapter adapter;
	private SHHealthOverviewActivity activity;
	private ArrayList<SHHealthMetricTool> shHealthTools;
	private OnHealthToolSelectedListener listener;
	
	public interface OnHealthToolSelectedListener {
		public void onHealthToolSelected(SHHealthMetricID healthMetricID);
	}
	
	@Override
    public void onAttach(Activity activity1) {
        super.onAttach(activity1);
        	listener = (OnHealthToolSelectedListener) activity1;
        	this.activity = (SHHealthOverviewActivity) activity1;
    }
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("SELECTED_INDEX", selectedIndex);
		outState.putSerializable("HEALTH_TOOLS", shHealthTools);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_menu, container, false);
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		if(savedInstanceState == null) {
			shHealthTools = SHController.getInstance(getActivity()).getActiveHealthMetricTools();
        } else {
        	selectedIndex = savedInstanceState.getInt("SELECTED_INDEX");
        	shHealthTools = (ArrayList<SHHealthMetricTool>) savedInstanceState.getSerializable("HEALTH_TOOLS");
        }
		
		if(adapter == null) { 
        	adapter = new HealthMenuAdapter(getActivity()); 
        }
        setListAdapter(adapter);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		if(shHealthTools.size() > 0) {
			if(activity.isDualPane()) {
	            getListView().setItemChecked(selectedIndex, true);
	            listener.onHealthToolSelected(shHealthTools.get(selectedIndex).getID());
	        } else {
	        	getListView().setItemChecked(selectedIndex, false);
	        }
		}
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	
    	if(position != selectedIndex || !activity.isDualPane()) {
    		l.setItemChecked(position, true);
    		selectedIndex = position;
        	listener.onHealthToolSelected(shHealthTools.get(position).getID());
    	}
    }
    
 	private class HealthMenuAdapter extends BaseAdapter {

 		public HealthMenuAdapter(Context context) { 
 			super(); 
 		}
		@Override
        public long getItemId(int position) { 
			return position; 
		}
 		@Override
        public SHHealthMetricTool getItem(int position) { 
 			return shHealthTools.get(position); 
 		}
 		@Override
        public int getCount() { 
 			return shHealthTools.size(); 
 		}
 		@Override
        public View getView(int position, View convertView, ViewGroup parent) {
 			
 			final SHMenuRow view;
 		    if (convertView == null) {
 		        view = new SHMenuRow(getActivity(), null);
 		    } else {
 		        view = (SHMenuRow) convertView;
 		    }
 		    
// 		    // TODO : Speed up resource retrieval.  Async Task. -- Uncomment when implemented
// 		    view.setIcon(IconFactory.getInstance().getSensorIconResource(healthTool));
 		    
 		    final SHHealthMetricTool healthTool = getItem(position);
		    view.setDescription(healthTool.getName());
		    view.setIcon(healthTool.getIcon());
		    
 		    return view;
 		}
 	}
}

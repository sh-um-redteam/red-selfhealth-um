/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHHomeActivity.OnScheduleToggledListener;
import edu.umass.nursing.selfhealth.client.schedule.SHScheduleItem;
import edu.umass.nursing.selfhealth.client.viewComponents.SHCompletedPlaceholder;
import edu.umass.nursing.selfhealth.client.viewComponents.SHHeader;
import edu.umass.nursing.selfhealth.client.viewComponents.SHScheduleItemRow;
import edu.umass.nursing.selfhealth.client.viewComponents.SHSchedulePlaceholder;

public class SHScheduleFragment extends ListFragment implements OnScheduleToggledListener {

	// UI Components
	private SHHeader header;
	private ViewGroup container;
	
	// Local Logic
	private boolean viewingScheduledEvents;
	private OnScheduleItemSelectedListener scheduleItemSelectedListener;
	
	public interface OnScheduleItemSelectedListener {
		public void onScheduleItemSelected(int scheduleItemIndex);
	}
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        	scheduleItemSelectedListener = (OnScheduleItemSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnScheduleItemSelectedListener");
        }
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        if(savedInstanceState != null) {
        	this.viewingScheduledEvents = savedInstanceState.getBoolean("VIEWING_SCHEDULED_EVENTS");
        } else { viewingScheduledEvents = true; }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container1, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container1, savedInstanceState);
    	
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_schedule, container1, false);
        this.header = (SHHeader) rootView.findViewById(R.id.header);
        
        this.container = container1;
        
        return rootView;
    }
    
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("VIEWING_SCHEDULED_EVENTS", viewingScheduledEvents);
	}
	
	// INTERFACE CALLBACKS =====================================================================
	
	@Override
	public void onViewSchedule(ArrayList<SHScheduleItem> scheduledItems) {
		
		if(scheduledItems.isEmpty()) {
			if(this.container.findViewWithTag("SCHEDULE_PLACEHOLDER") == null) {
				View emptySchedule = new SHSchedulePlaceholder(getActivity(), null);
				emptySchedule.setTag("SCHEDULE_PLACEHOLDER");
				this.container.addView(emptySchedule);
			}
			this.getListView().setEmptyView(this.container.findViewWithTag("SCHEDULE_PLACEHOLDER"));
			header.setVisibility(View.GONE);
		} else {
			header.setTitle("Today's Schedule");
			header.setSubtitle("Time Scheduled");
			header.setVisibility(View.VISIBLE);
		}
		
		this.viewingScheduledEvents = true;
		adaptScheduleListView(scheduledItems);
	}

	@Override
	public void onViewCompleted(ArrayList<SHScheduleItem> completedItems) {
		
		if(completedItems.isEmpty()) {
			if(this.container.findViewWithTag("COMPLETED_PLACEHOLDER") == null) {
				View emptyCompleted = new SHCompletedPlaceholder(getActivity(), null);
				emptyCompleted.setTag("COMPLETED_PLACEHOLDER");
				this.container.addView(emptyCompleted);
			}
			this.getListView().setEmptyView(this.container.findViewWithTag("COMPLETED_PLACEHOLDER"));
			header.setVisibility(View.GONE);
		} else {
			header.setTitle("Completed Today");
			header.setSubtitle("Time Completed");
			header.setVisibility(View.VISIBLE);
		}
		
		this.viewingScheduledEvents = false;
		adaptScheduleListView(completedItems);
	}
	
	// ========================================================================================
	
	private void adaptScheduleListView(ArrayList<SHScheduleItem> items) {
		setListAdapter(new ScheduleAdapter(getActivity(), R.layout.sh_row_schedule_item, items));
	}
	
	private class ScheduleAdapter extends ArrayAdapter<SHScheduleItem> {
		
 		private ArrayList<SHScheduleItem> items;
 		
 		public ScheduleAdapter(Context context, int listItemResource, ArrayList<SHScheduleItem> items) {
 			super(context, listItemResource);
 			this.items = items;
 		}
 		@Override
 		public boolean isEnabled(int position) {
 			if (viewingScheduledEvents) {
 				return true;
 			} else {
 				return false;
 			}
 		}
 		@Override
 		public boolean isEmpty(){ return items.size() == 0; }
 		@Override
 		public SHScheduleItem getItem(int position) { return items.get(position); }
 		@Override
 		public int getCount() { return items.size(); }
 		@Override
 		public View getView(final int position, View convertView, ViewGroup parent) {
 			
 			SHScheduleItemRow view;
 		    if (convertView == null) {
 		        view = new SHScheduleItemRow(getActivity(), null);
 		        convertView = view;
 		    } else {
 		        view = (SHScheduleItemRow) convertView;
 		    }
 		    
 		    final SHScheduleItem item = getItem(position);
 		    
	    	view.setIcon(item.getTool().getIcon());
			view.setDescription(item.toString());
			view.setTime(item.displayTime());
			
 		    if (viewingScheduledEvents) {
	        	view.setOnClickListener(new OnClickListener() {
	                @Override
	                public void onClick(View v) {
	                	scheduleItemSelectedListener.onScheduleItemSelected(position);
	                }
	            });
 		    } 
 		    return convertView;
 		}
 	}
}

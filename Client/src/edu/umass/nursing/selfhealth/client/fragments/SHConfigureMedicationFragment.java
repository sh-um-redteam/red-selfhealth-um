package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;
import java.util.Arrays;

import org.joda.time.LocalDateTime;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.medication.SHMedicationID;
import edu.umass.nursing.selfhealth.client.records.SHMedicationRecord;

public class SHConfigureMedicationFragment extends DialogFragment{
	
	// TODO : BUG*** When the screen turns off for configuring a new medication, the values in the 
	// text fields are reset to nothing. Fragment Life-cycle.
	
	// TODO : The prepopulated lists of units + forms etc should be stored in the DB and new additions should be added to the DB.
	//			Then they should be retrieved with a controller call instead of hard-coded in here.

/****************** UI COMPONENTS ******************/
	
	// Text Views
	private TextView medicationTextView;
	private TextView formTextView;
	private TextView dosageTextView;
	private TextView unitTextView;
	private TextView quantityTextView;
	
	// Buttons
	private ImageButton editButton;
	private LinearLayout formButton;
	private LinearLayout dosageButton;
	private LinearLayout unitButton;
	private LinearLayout quantityButton;
	
/****************** DATA FIELDS ******************/
	//private SHMedicationID id:
	private final String DEFAULT_FIELD = "Set";
	
	public String name;
	public String form;
	public String dosage;
	public String unit;
	public String quantity;
	
	final String[] medications = new String[] { 
			"Furosemide", 
			"Hydrochlorothiazide",
			"Lisinopril", 
			"Amlodipine", 
			"Digoxin", 
			"Propranolol", 
			"Levemir",
			"Lantus",
			"Glucophage",
			"Atorvastatin",
			"Warfarin",
			"Citalopram",
			"Sertraline",
			"Hydrocodone",
			"Tramadol",
			"Acetaminophen",
			"Advair",
			"Albuterol",
			"Timolol",
			"Travoprost",
			"Ciprofloxacin",
			"Levofloxacin",
			"Cephalexin",
	};
	
	final String[] forms = new String[] { 
			"Pill", 
			"Tablet",
			"Capsule", 
			"Oral", 
			"Inhalant", 
			"Topical", 
	};
	final String[] units = new String[] { 
			"mg", 
			"mL",
	};
	

/****************** LISTENERS ******************/
	
	@Override
	public Dialog onCreateDialog( Bundle savedInstanceState ) {

		Arrays.sort(medications);
		Arrays.sort(forms);
		Arrays.sort(units);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		final Activity activity = getActivity();
		final LayoutInflater inflater = getActivity().getLayoutInflater();
		
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_medication,null);
		builder.setView(view);
		
		medicationTextView = (TextView) view.findViewById(R.id.MedicationNameLabel);
		formTextView = (TextView) view.findViewById(R.id.MedicationFormLabel);
		dosageTextView = (TextView) view.findViewById(R.id.MedicationDosageLabel);
		unitTextView = (TextView) view.findViewById(R.id.MedicationUnitLabel);
		quantityTextView = (TextView) view.findViewById(R.id.MedicationQuantityLabel);
		
		editButton = (ImageButton) view.findViewById(R.id.MedicationNameEditButton);
		formButton = (LinearLayout) view.findViewById(R.id.MedicationFormButton);
		dosageButton = (LinearLayout) view.findViewById(R.id.MedicationDosageButton);
		unitButton = (LinearLayout) view.findViewById(R.id.MedicationUnitButton);
		quantityButton = (LinearLayout) view.findViewById(R.id.MedicationQuantityButton);
		
		editButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				AlertDialog alertdialog;
            	
            	final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Medication");
                
                // Datasource for populating the list of medication choices
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                		getActivity(),
                        android.R.layout.simple_list_item_1, medications);
                
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                
                builder.setPositiveButton("New",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            	
                            	final EditText input = new EditText(getActivity());
                            	builder.setMessage("Enter a medication name").setView(input);
                			    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                			        @Override
                                    public void onClick(DialogInterface dialog1, int whichButton) {
                			            
                			        	Editable value = input.getText(); 
                			            
                			        	if (value.toString().trim()=="" || value.toString().trim()==null || value.toString().isEmpty()) {
                			            	Toast toast = Toast.makeText(getActivity(), "Please enter valid information.", Toast.LENGTH_SHORT);
                			    			toast.setGravity(Gravity.CENTER, 0, 0);
                			    			LinearLayout toastLayout = (LinearLayout) toast.getView();
                			    			TextView toastTV = (TextView) toastLayout.getChildAt(0);
                			    			toastTV.setTextSize(30);
                			    			toast.show();
                			            } else {
                			            	updateName(value.toString());
                			            }}}).show();}});
                
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateName(arrayAdapter.getItem(which));
                        dialog.dismiss();
                    }
                });
                
                alertdialog = builder.create();
                alertdialog.show();
                alertdialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, 600);
			}
		});
		
		// Uses a Number Picker for selecting the dosage value (3 significant digits)
		dosageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

        		int hundreds, tens, ones, tenths, hundredths;
	        	double dosageValue = 100.0;
	        	//Log.i("PROVIDER", dosage);
	        	//System.out.println(dosageValue);
	        	
	        	if(dosageValue > 0) {
	        		hundreds 	= (int) (dosageValue*.01 % 10);
	        		tens 		= (int) (dosageValue*.1 % 10);
	        		ones 		= (int) (dosageValue % 10);
	        		tenths 		= (int) (dosageValue*10 % 10);
    				hundredths 	= (int) (dosageValue*100 % 10);
        		} else {
        			hundreds = 1; tens = 0; ones = 0; tenths = 0; hundredths = 0;
        		}
        		
                View pickerFrame = inflater.inflate(R.layout.number_picker_dosage, null);
                final NumberPicker pickerHundreds 	= (NumberPicker) pickerFrame.findViewById(R.id.hundreds);
                final NumberPicker pickerTens 		= (NumberPicker) pickerFrame.findViewById(R.id.tens);
                final NumberPicker pickerOnes 		= (NumberPicker) pickerFrame.findViewById(R.id.ones);
                final NumberPicker pickerTenths 	= (NumberPicker) pickerFrame.findViewById(R.id.tenths);
                final NumberPicker pickerHundredths	= (NumberPicker) pickerFrame.findViewById(R.id.hundredths);

                ArrayList<NumberPicker> pickers = new ArrayList<NumberPicker>();
                pickers.add(pickerHundreds); pickers.add(pickerTens); pickers.add(pickerOnes); pickers.add(pickerTenths); pickers.add(pickerHundredths);
                
                for(NumberPicker picker : pickers) {
                	picker.setMaxValue(9);
                	picker.setMinValue(0);
                	picker.setWrapSelectorWheel(false);
                	picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                }
                pickerHundreds.setValue(hundreds);
                pickerTens.setValue(tens);
                pickerOnes.setValue(ones);
                pickerTenths.setValue(tenths);
                pickerHundredths.setValue(hundredths);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(pickerFrame)
            	.setTitle("Dosage")
            	.setPositiveButton(DEFAULT_FIELD, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	
                        int hundreds1 	= pickerHundreds.getValue();
                        int tens1 		= pickerTens.getValue();
                        int ones1 		= pickerOnes.getValue();
                        int tenths1 		= pickerTenths.getValue();
                        int hundredths1 	= pickerHundredths.getValue();
                        
                        updateDosage(String.valueOf(Double.valueOf(hundreds1 + "" + tens1 + "" + ones1 + "." + tenths1 + "" + hundredths1)));
                    }
            	})
            	.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            		
                    @Override
                    public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
            	})
            	.create().show();
            }
        });
		
		// Uses a Number Picker for selecting the quantity (single digit)
		quantityButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
            public void onClick(View v) {

        		int ones, tenths, hundredths;
	        	double quantityValue = 0.0;
	        	
	        	if(quantityValue > 0) {
	        		ones 		= (int) (quantityValue % 10);
	        		tenths 		= (int) (quantityValue*10 % 10);
    				hundredths 	= (int) (quantityValue*100 % 10);
        		} else {
        			ones = 0; tenths = 0; hundredths = 0;
        		}
        		
                View pickerFrame = inflater.inflate(R.layout.number_picker_quantity, null);
                final NumberPicker pickerOnes 		= (NumberPicker) pickerFrame.findViewById(R.id.ones);
                final NumberPicker pickerTenths 	= (NumberPicker) pickerFrame.findViewById(R.id.tenths);
                final NumberPicker pickerHundredths	= (NumberPicker) pickerFrame.findViewById(R.id.hundredths);

                ArrayList<NumberPicker> pickers = new ArrayList<NumberPicker>();
                pickers.add(pickerOnes); pickers.add(pickerTenths); pickers.add(pickerHundredths);
                
                for(NumberPicker picker : pickers) {
                	picker.setMaxValue(9);
                	picker.setMinValue(0);
                	picker.setWrapSelectorWheel(false);
                	picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                }
                pickerOnes.setValue(ones);
                pickerTenths.setValue(tenths);
                pickerHundredths.setValue(hundredths);

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(pickerFrame)
            	.setTitle("Dosage")
            	.setPositiveButton(DEFAULT_FIELD, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	
                        int ones1 		= pickerOnes.getValue();
                        int tenths1 		= pickerTenths.getValue();
                        int hundredths1 	= pickerHundredths.getValue();
                        
                        updateQuantity(String.valueOf(Double.valueOf(ones1 + "." + tenths1 + "" + hundredths1)));
                    }
            	})
            	.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            		
                    @Override
                    public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
            	})
            	.create().show();
            }
        });
		
        formButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	AlertDialog alertdialog;
            	
            	final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Form");
                
                // Datasource for populating the list of form choices
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                		getActivity(),
                        android.R.layout.simple_list_item_1, forms);
                
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                
                builder.setPositiveButton("New",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            	
                            	final EditText input = new EditText(getActivity());
                            	builder.setMessage("Enter a medication form").setView(input);
                			    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                			        @Override
                                    public void onClick(DialogInterface dialog1, int whichButton) {
                			            
                			        	Editable value = input.getText(); 
                			            
                			            if (value.toString().trim()=="" || value.toString().trim()==null || value.toString().isEmpty()) {
                			            	Toast toast = Toast.makeText(getActivity(), "Please enter valid information.", Toast.LENGTH_SHORT);
                			    			toast.setGravity(Gravity.CENTER, 0, 0);
                			    			LinearLayout toastLayout = (LinearLayout) toast.getView();
                			    			TextView toastTV = (TextView) toastLayout.getChildAt(0);
                			    			toastTV.setTextSize(30);
                			    			toast.show();
                			            } else {
                			            	updateForm(value.toString());
                			            }}}).show();}});
                
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateForm(arrayAdapter.getItem(which));
                        dialog.dismiss();
                    }
                });
                
                alertdialog = builder.create();
                alertdialog.show();
                alertdialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, 600);
            }
        });
        
        unitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	final AlertDialog.Builder builder = new AlertDialog.Builder(
                        getActivity());
                builder.setTitle("Unit");
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                		getActivity(),
                        android.R.layout.simple_list_item_1, units);
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.setPositiveButton("New",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            	final EditText input = new EditText(getActivity());
                            	builder.setMessage("Enter a measurement unit").setView(input)
                			    .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                			        @Override
                                    public void onClick(DialogInterface dialog1, int whichButton) {
                			            Editable value = input.getText(); 
                			            
                			            if (value.toString().trim()=="" || value.toString().trim()==null || value.toString().isEmpty()) {
                			            	Toast toast = Toast.makeText(getActivity(), "Please enter valid information.", Toast.LENGTH_SHORT);
                			    			toast.setGravity(Gravity.CENTER, 0, 0);
                			    			LinearLayout toastLayout = (LinearLayout) toast.getView();
                			    			TextView toastTV = (TextView) toastLayout.getChildAt(0);
                			    			toastTV.setTextSize(30);
                			    			toast.show();
                			            } else {
                			            	updateUnit(value.toString());
                			            }}}).show();}});
                builder.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                updateUnit(arrayAdapter.getItem(which));
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }
        });
        
        builder.setNegativeButton("Cancel" , new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss(); 
			}
		}); 
        builder.setPositiveButton("Save" , new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				LocalDateTime time = new LocalDateTime();
				SHMedicationID medID = new SHMedicationID(999);
				SHMedicationRecord record = new SHMedicationRecord(medID, time);
				Integer scheduleID = (Integer) 1;
				SHController.getInstance(getActivity()).saveMedicationRecord(record, scheduleID);
				showToastWithText("The Medication has been saved.");
				dialog.dismiss(); 
			 
			}
		});    

        return builder.create();
		
	}
	
	
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
	
	public void updateName(String name1) {
		this.name = name1;
		if(name1 == null) {
			this.medicationTextView.setTextColor(getResources().getColor(R.color.accent_light_gray));
			this.medicationTextView.setText("Enter Medication Name");
		} else {
			this.medicationTextView.setTextColor(getResources().getColor(R.color.dark_text));
			this.medicationTextView.setText(name1);
		}
	}
	public void updateForm(String form1) {
		this.form = form1;
		if(form1 == null) {
			this.formTextView.setTextColor(getResources().getColor(R.color.accent_light_gray));
			this.formTextView.setText(DEFAULT_FIELD);
		} else {
			this.formTextView.setText(form1);
			this.formTextView.setTextColor(getResources().getColor(R.color.dark_text));
		}
	}
	public void updateDosage(String dosageString) {
		this.dosage = dosageString;
		if(Double.parseDouble(dosageString) <= 0) {
			this.dosageTextView.setTextColor(getResources().getColor(R.color.accent_light_gray));
			this.dosageTextView.setText(DEFAULT_FIELD);
		} else {
			this.dosageTextView.setText(dosageString);
			this.dosageTextView.setTextColor(getResources().getColor(R.color.dark_text));
		}
	}
	public void updateUnit(String unit1) {
		this.unit = unit1;
		if(unit1 == null) {
			this.unitTextView.setTextColor(getResources().getColor(R.color.accent_light_gray));
			this.unitTextView.setText(DEFAULT_FIELD);
		} else {
			this.unitTextView.setText(unit1);
			this.unitTextView.setTextColor(getResources().getColor(R.color.dark_text));
		}
		
	}
	public void updateQuantity(String quantityString) {
		this.quantity = quantityString;
		if(Double.parseDouble(quantityString) <= 0) {
			this.quantityTextView.setTextColor(getResources().getColor(R.color.accent_light_gray));
			this.quantityTextView.setText(DEFAULT_FIELD);
		} else {
			this.quantityTextView.setText(quantityString+"x");
			this.quantityTextView.setTextColor(getResources().getColor(R.color.dark_text));
		}
		
	}
	
	private void showToastWithText(String msg) {
		Toast toast = Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER,0,0);
		toast.show();
	}
	
}



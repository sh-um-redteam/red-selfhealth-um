/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHSurveyActivity.OnResponseSetChangedListener;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyResponse;
import edu.umass.nursing.selfhealth.client.viewComponents.SHSurveyResponseRow;

public class SHSurveyResponseFragment extends ListFragment implements OnResponseSetChangedListener {
	
	
	private int selectedIndex;
	private final int NO_SELECTION = -1;
	
	private SurveyResponseAdapter adapter;
	private ArrayList<SHSurveyResponse> responseOptions;
	
	private OnResponseSelectedListener listener;
	
	public interface OnResponseSelectedListener {
		public void onResponseSelected(boolean selected, int index, int responseID);
	}
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        	listener = (OnResponseSelectedListener) activity;
    }
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("SELECTED_INDEX", selectedIndex);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_survey_responses, container, false);
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		responseOptions = new ArrayList<SHSurveyResponse>();
		if(savedInstanceState == null) {
			selectedIndex = NO_SELECTION;
        } else {
        	selectedIndex = savedInstanceState.getInt("SELECTED_INDEX");
        }
		if(adapter == null) { 
        	adapter = new SurveyResponseAdapter(getActivity()); 
        }
        setListAdapter(adapter);
	}
	
	@Override
    public void onResponseSetChanged(Integer selectedIndex1, ArrayList<SHSurveyResponse> responseSet) {
		getListView().setItemChecked(this.selectedIndex, false);
		this.responseOptions = responseSet;
		adapter.notifyDataSetChanged();
		if(selectedIndex1 != null) {
			this.selectedIndex = selectedIndex1;
			getListView().setItemChecked(this.selectedIndex, true);
		} else {
			this.selectedIndex = NO_SELECTION;
		}
	}
	
	@Override
	public void onStart() {
		super.onStart();
			if(selectedIndex != NO_SELECTION)
				getListView().setItemChecked(selectedIndex, true);
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	
    	if(position != selectedIndex) {
    		l.setItemChecked(position, true);
    		selectedIndex = position;
    		listener.onResponseSelected(true, position, responseOptions.get(position).getResponseID());
    	} else {
    		l.setItemChecked(position, false);
    		selectedIndex = NO_SELECTION;
    		listener.onResponseSelected(false, position, SHSurveyResponse.NO_RESPONSE_ID);
    	}
    }
    
 	private class SurveyResponseAdapter extends BaseAdapter {

 		public SurveyResponseAdapter(Context context) { 
 			super(); 
 		}
		@Override
        public long getItemId(int position) { 
			return position; 
		}
 		@Override
        public SHSurveyResponse getItem(int position) { 
 			return responseOptions.get(position); 
 		}
 		@Override
        public int getCount() { 
 			return responseOptions.size(); 
 		}
 		@Override
        public View getView(int position, View convertView, ViewGroup parent) {
 			
 			final SHSurveyResponseRow view;
 		    if (convertView == null) {
 		        view = new SHSurveyResponseRow(getActivity(), null);
 		    } else {
 		        view = (SHSurveyResponseRow) convertView;
 		    }
 		    view.setResponse(getItem(position).response());
 		    return view;
 		}
 	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHHealthOverviewActivity.OnViewHealthHistoryListener;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricComponent;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;
import edu.umass.nursing.selfhealth.client.records.SHHealthMetricRecord;
import edu.umass.nursing.selfhealth.client.records.SHMeasurableHealthMetricRecord;
import edu.umass.nursing.selfhealth.client.viewComponents.SHConfirmableRecordRow;
import edu.umass.nursing.selfhealth.client.viewComponents.SHHealthRecordRow;

public class SHHealthHistoryFragment extends ListFragment implements OnViewHealthHistoryListener {

	private SHHealthMetricID healthMetricID;
	private HealthHistoryAdapter adapter;
	private ArrayList<SHHealthMetricRecord> shHistory;
    
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
        	shHistory = new ArrayList<SHHealthMetricRecord>();
        } else {
        	shHistory = (ArrayList<SHHealthMetricRecord>) savedInstanceState.getSerializable("HEALTH_HISTORY");
        	healthMetricID = (SHHealthMetricID) savedInstanceState.getSerializable("HEALTH_METRIC_ID");
        }
        
        if(adapter == null) { 
        	adapter = new HealthHistoryAdapter(getActivity()); 
        }
        setListAdapter(adapter);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_health_history, container, false);
        return rootView;
    }
    
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("HEALTH_HISTORY", shHistory);
		outState.putSerializable("HEALTH_METRIC_ID", healthMetricID);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		onViewHealthHistory(healthMetricID);
	}
	
	@Override
    public void onViewHealthHistory(SHHealthMetricID healthMetricID1) {
		
		if(healthMetricID1 == null) return;
		this.healthMetricID = healthMetricID1;
		
		shHistory = SHController.getInstance(getActivity()).getHealthMetricRecords(healthMetricID1, null);
		adapter = new HealthHistoryAdapter(getActivity()); 
		setListAdapter(adapter);
	}
	
 	private class HealthHistoryAdapter extends BaseAdapter {

 		public HealthHistoryAdapter(Context context) { super(); }
 		@Override
		public long getItemId(int position) { return position; }
 		@Override
 		public boolean isEnabled(int position) {  return false; }
 		@Override
 		public boolean isEmpty() {  return shHistory.size() == 0; }
 		@Override
 		public int getCount() { return shHistory.size(); }
 		@Override
        public SHHealthMetricRecord getItem(int position) { return shHistory.get(position); }
 		@Override
        public View getView(final int position, View convertView, ViewGroup parent) {

 			SHHealthMetricRecord record = getItem(position);
 			boolean isConfirmable = record.isConfirmable();
 			if (convertView == null) {
 				if(isConfirmable) {
 		    		convertView = new SHConfirmableRecordRow(getActivity(), null);
 		    	} else {
 		    		convertView = new SHHealthRecordRow(getActivity(), null);
 		    	}
 		    } else {
 		    	if(isConfirmable) {
 		    		convertView = (SHConfirmableRecordRow) convertView;
 		    	} else {
 		    		convertView = (SHHealthRecordRow) convertView;
 		    	}
 		    }
 			
 			if(isConfirmable) {
 				((SHConfirmableRecordRow) convertView).setDateTimeCompleted(record.getTimeCompleted());
		    } else {
		    	((SHHealthRecordRow) convertView).setDateTimeCompleted(record.getTimeCompleted());
	 		    
	 		    ArrayList<String> components = new ArrayList<String>();
	 		    ArrayList<String> units = new ArrayList<String>();
	 		    ArrayList<Number> measurements = new ArrayList<Number>();
	 		    
	 		    for(final SHHealthMetricComponent component : ((SHMeasurableHealthMetricRecord) record).healthMetric().components()) {
	 		    	components.add(component.toString());
	 		    	units.add(component.unit().toString());
	 		    	measurements.add(((SHMeasurableHealthMetricRecord) record).componentValue(component).intValue());
	 		    }
	 		   ((SHHealthRecordRow) convertView).updateComponents(components, units, measurements);
 			}
 		   
 		    return convertView;
 		}
 	}
}

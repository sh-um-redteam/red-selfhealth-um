/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHConfirmationActivity.OnSetupPaneInformationListener;
import edu.umass.nursing.selfhealth.client.viewComponents.SHConfirmationPane;

public class SHConfirmationFragment extends Fragment implements OnSetupPaneInformationListener {
	
	private SHConfirmationPane confirmationPane;
	private OnConfirmationListener listener;
	
	public interface OnConfirmationListener {
		public void onConfirm();
	}
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        	listener = (OnConfirmationListener) activity;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_confirmation, container, false);
        confirmationPane = (SHConfirmationPane) rootView.findViewById(R.id.confirmationPane);
        confirmationPane.setOnClickListener(new OnClickListener() {

			@Override
            public void onClick(View v) {
				listener.onConfirm();
			}
        });
		
        return rootView;
    }
    
    @Override
    public void onSetupPaneInformation(String prompt, String title, String subtitle) {
    	confirmationPane.setHeaderPrompt(prompt);
    	confirmationPane.setTitle(title);
    	confirmationPane.setSubtitle(subtitle);
    }
}

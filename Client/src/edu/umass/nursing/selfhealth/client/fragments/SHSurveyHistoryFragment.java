/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHSurveyOverviewActivity.OnViewSurveyHistorListener;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.records.SHSurveyRecord;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyID;
import edu.umass.nursing.selfhealth.client.viewComponents.SHHeader;
import edu.umass.nursing.selfhealth.client.viewComponents.SHSurveyRecordRow;

public class SHSurveyHistoryFragment extends ListFragment implements OnViewSurveyHistorListener {

	//	// UI Components
	//  TODO : Uncomment when attempting to resolve graph issues.
	//	private LineGraph graph;

	private SHHeader header;
	private int selectedIndex;
	private SHSurveyID surveyID;
	private SurveyHistoryAdapter adapter;
	private ArrayList<SHSurveyRecord> shSurveyHistory;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if(savedInstanceState != null) {
			this.selectedIndex = savedInstanceState.getInt("SELECTED_INDEX");
			this.surveyID = (SHSurveyID) savedInstanceState.getSerializable("SURVEY_ID");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		View rootView = inflater.inflate(R.layout.sh_fragment_survey_history, container, false);

		header = (SHHeader) rootView.findViewById(R.id.header);
		header.setTitle("Date Completed");
		header.setSubtitle("Score");

		if(shSurveyHistory == null) 	{ shSurveyHistory = new ArrayList<SHSurveyRecord>(); }
		if(adapter == null) 		{ adapter = new SurveyHistoryAdapter(getActivity()); }
		setListAdapter(adapter);

		//        TODO : Uncomment when attempting to resolve graph issues.
		//        graph = (LineGraph) rootView.findViewById(R.id.graph);

		return rootView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("SELECTED_INDEX", selectedIndex);
		outState.putSerializable("SURVEY_ID", surveyID);
	}

	@Override
	public void onStart() {
		super.onStart();
		onViewSurveyHistory(surveyID);
	}

	@Override
	public void onViewSurveyHistory(SHSurveyID surveyID1) {

		if(surveyID1 == null) return;
		this.surveyID = surveyID1;

		shSurveyHistory = SHController.getInstance(getActivity()).getSurveyRecords(surveyID1, null);

		if(shSurveyHistory.size() == 0) {
			header.setVisibility(View.GONE);
		} else {
			header.setVisibility(View.VISIBLE);
		}
		//		TODO : Uncomment when attempting to resolve graph issues.
		//		setupGraphViews();

		adapter.notifyDataSetChanged();
	}

	//	TODO : Uncomment when attempting to implement the graph.  It is painful... you might want to write your own graph library
	//	// TODO : The graph is not displaying its points or lines properly...
	//	private void setupGraphViews() {
	//		
	//		if(shSurveyHistory.size() < 4) {
	//			graph.setVisibility(View.GONE);
	//			return;
	//		} 
	//		graph.setVisibility(View.VISIBLE);
	//		
	//		final Resources resources = getResources();
	//		
	//		Line l = new Line();
	////		l.setUsingDips(true);
	//		
	//		int minValue = 99999;
	//		int maxValue = -99999;
	//		
	//		for(int i = 0; i < shSurveyHistory.size(); i++) {
	//			
	//			int x = i;
	//			int y = shSurveyHistory.get(i).getScore();
	//			
	//			Log.i("GRAPH", "CURRENT POINT: " + "(" + x + ", " + y + ")");
	//			
	//			if(y < minValue) {
	//				minValue = y;
	//			} else if(y > maxValue) {
	//				maxValue = y;
	//			}
	//			
	//			LinePoint p = new LinePoint();
	//			p.setX(x);
	//			p.setY(y);
	//			p.setColor(resources.getColor(R.color.graph_line));
	//			p.setSelectedColor(resources.getColor(R.color.graph_point_selected));
	//			l.addPoint(p);
	//		}
	//		
	//		l.setColor(resources.getColor(R.color.graph_line));
	//		l.setStrokeWidth(R.dimen.thickness_graph_line);
	//		
	//		graph.addLine(l);
	//		// TODO : This is commented out because it causes the graph to not display properly.
	//		// But when the graph actually shows up, the range is incorrect and the entire view is shaded.
	//		
	////		graph.setRangeY(minValue-10, maxValue+10);
	//		graph.setLineToFill(0);
	////		graph.setBackgroundColor(resources.getColor(R.color.background_white));
	//		graph.setOnPointClickedListener(new OnPointClickedListener() {
	//		
	//			 @Override
	//			 public void onClick(int lineIndex, int pointIndex) {
	//			 	
	//			 	for(int i = 0; i < graph.getLine(0).getSize(); i++) {
	//			 		if(i == pointIndex) {
	//			 			LinePoint clicked = graph.getLine(0).getPoint(i);
	//			 			clicked.setColor(resources.getColor(R.color.action_bar));
	//			 		} else {
	//			 			LinePoint notClicked = graph.getLine(0).getPoint(i);
	//			 			notClicked.setColor(resources.getColor(R.color.accent_light_gray));
	//			 		}
	//			 	}
	//				selectedIndex = pointIndex;
	//				adapter.notifyDataSetChanged();
	//			 }
	//		});
	//	}

	private class SurveyHistoryAdapter extends BaseAdapter {

		public SurveyHistoryAdapter(Context context) { super(); }
		@Override
		public long getItemId(int position) { return position; }
		@Override
		public boolean isEmpty() { return shSurveyHistory.size() == 0; }
		@Override
		public boolean isEnabled(int position) { return false; }
		@Override
		public SHSurveyRecord getItem(int position) { 

			// 			TODO : Uncomment when attempting to resolve graph issues.
			// 			if(shSurveyHistory.size() > 3)
			// 				return shSurveyHistory.get(selectedIndex);  
			//			else 
			return shSurveyHistory.get(position); 
		}
		@Override
		public int getCount() { 

			// 			TODO : Uncomment when attempting to resolve graph issues.
			//			if(shSurveyHistory.size() > 3)
			// 				return 1; 
			//			else 
			return shSurveyHistory.size(); 
		}
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			SHSurveyRecordRow view;
			if (convertView == null) {
				view = new SHSurveyRecordRow(getActivity(), null);
			} else {
				view = (SHSurveyRecordRow) convertView;
			}
			final SHSurveyRecord record = getItem(position);
			view.setDateTimeCompleted(record.getTimeCompleted());
			view.setScore(record.getScore());
			/*if(record.getScore() < 1000){
				//AlertDialog alertdialog;
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				//builder.setView();
				builder.setTitle("PRN Prompt");
				builder.setMessage("Your score on this survey surpassed the threshold. Would you like to" +
						" take an advil?");
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						Toast toast = Toast.makeText(getActivity(), "You declined the medication", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER,0,0);
						toast.show();
					}
				});
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						Toast toast = Toast.makeText(getActivity(), "The medication has been logged.", Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER,0,0);
						toast.show();
					}
				});
				
				AlertDialog dialog= builder.create();
				dialog.getWindow().setGravity(Gravity.CENTER);
				dialog.show();	
			}
*/
			return view;
		}
	}
}

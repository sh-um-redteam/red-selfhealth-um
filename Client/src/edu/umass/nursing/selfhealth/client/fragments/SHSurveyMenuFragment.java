/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHSurveyOverviewActivity;
import edu.umass.nursing.selfhealth.client.activities.SHSurveyOverviewActivity.SurveyHistoryRequestedListener;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyID;
import edu.umass.nursing.selfhealth.client.tools.SHSurveyTool;
import edu.umass.nursing.selfhealth.client.viewComponents.SHMenuRow;

//TODO : Follow Pattern as implemented in {@link SHMedicationMenuFragment}

public class SHSurveyMenuFragment extends ListFragment implements SurveyHistoryRequestedListener {
	
	private int selectedIndex;
	private SurveyMenuAdapter adapter;
	private SHSurveyOverviewActivity activity;
	private ArrayList<SHSurveyTool> shSurveys;
	
	// Listeners
	private OnSurveySelectedListener listener;
	
	// Interfaces
	public interface OnSurveySelectedListener {
		public void onSurveySelected(SHSurveyID surveyID);
	}
	
	@Override
    public void onAttach(Activity activity1) {
        super.onAttach(activity1);
        try {
        	listener = (OnSurveySelectedListener) activity1;
        	this.activity = (SHSurveyOverviewActivity) activity1;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity1.toString() + " must implement OnSurveySelectedListener");
        }
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_menu, container, false);
        
        if(shSurveys == null) { shSurveys = SHController.getInstance(getActivity()).getActiveSurveyTools(); }
        if(adapter == null) { adapter = new SurveyMenuAdapter(getActivity()); }
        setListAdapter(adapter);
        
        return rootView;
    }
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("SELECTED_INDEX", selectedIndex);
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
        	this.selectedIndex = savedInstanceState.getInt("SELECTED_INDEX");
        }
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		if(shSurveys.size() > 0) {
			if(activity.isDualPane()) {
	            getListView().setItemChecked(selectedIndex, true);
	        } else {
	        	getListView().setItemChecked(selectedIndex, false);
	        }
		}
	}
	
	@Override
	public void onSurveyHistoryRequested() {
		listener.onSurveySelected(shSurveys.get(selectedIndex).getID());
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	
    	if(position != selectedIndex || !activity.isDualPane()) {
    		l.setItemChecked(position, true);
    		selectedIndex = position;
    		listener.onSurveySelected(shSurveys.get(position).getID());
    	}
    }
    
 	private class SurveyMenuAdapter extends BaseAdapter {

 		public SurveyMenuAdapter(Context context) { super(); }
 		@Override
		public long getItemId(int position) { return position; }
 		@Override
 		public SHSurveyTool getItem(int position) { return shSurveys.get(position); }
 		@Override
 		public int getCount() { return shSurveys.size(); }
 		@Override
 		public View getView(int position, View convertView, ViewGroup parent) {
 			
 			final SHMenuRow view;
 		    if (convertView == null) {
 		        view = new SHMenuRow(getActivity(), null);
 		    } else {
 		        view = (SHMenuRow) convertView;
 		    }
 		    
 		    final SHSurveyTool surveyTool = getItem(position);
		    view.setDescription(surveyTool.getName());
		    view.setIcon(surveyTool.getIcon());
 		    return view;
 		}
 	}
}

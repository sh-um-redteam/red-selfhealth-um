/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHMedicationOverviewActivity;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.viewComponents.SHMedicationMenuRow;

public class SHMedicationMenuFragment extends ListFragment {
	
	private ArrayList<SHMedication> shMedications;
	private int selectedIndex;
	private MedicationMenuAdapter adapter;
	private SHMedicationOverviewActivity activity;
	private OnMedicationSelectedListener listener;
	
	public interface OnMedicationSelectedListener {
		public void onMedicationSelected(SHMedication medication);
	}
	
	@Override
    public void onAttach(Activity activity1) {
        super.onAttach(activity1);
        	listener = (OnMedicationSelectedListener) activity1;
        	this.activity = (SHMedicationOverviewActivity) activity1;
    }
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("SELECTED_INDEX", selectedIndex);
		outState.putSerializable("MEDICATIONS", shMedications);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_menu, container, false);
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		if(savedInstanceState == null) {
			shMedications = SHController.getInstance(getActivity()).getActiveMedications();
        } else {
        	selectedIndex = savedInstanceState.getInt("SELECTED_INDEX");
        	shMedications = (ArrayList<SHMedication>) savedInstanceState.getSerializable("MEDICATIONS");
        }
		
		if(adapter == null) { 
        	adapter = new MedicationMenuAdapter(getActivity()); 
        }
        setListAdapter(adapter);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		if(shMedications.size() > 0) {
			if(activity.isDualPane()) {
	            getListView().setItemChecked(selectedIndex, true);
	            listener.onMedicationSelected(shMedications.get(selectedIndex));
	        } else {
	        	getListView().setItemChecked(selectedIndex, false);
	        }
		}
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	
    	if(position != selectedIndex || !activity.isDualPane()) {
    		l.setItemChecked(position, true);
    		selectedIndex = position;
        	listener.onMedicationSelected(shMedications.get(position));
    	}
    }
    
 	private class MedicationMenuAdapter extends BaseAdapter {

 		public MedicationMenuAdapter(Context context) { 
 			super(); 
 		}
		@Override
        public long getItemId(int position) { 
			return position; 
		}
		@Override
        public SHMedication getItem(int position) { 
 			return shMedications.get(position); 
 		}
 		@Override
        public int getCount() { 
 			return shMedications.size(); 
 		}
 		@Override
        public View getView(int position, View convertView, ViewGroup parent) {
 			
 			final SHMedicationMenuRow view;
 		    if (convertView == null) {
 		        view = new SHMedicationMenuRow(getActivity(), null);
 		    } else {
 		        view = (SHMedicationMenuRow) convertView;
 		    }
 		    
 		    final SHMedication medication = getItem(position);
		    view.setName(medication.getName());
		    view.setDosage(medication.getDetails());
 		    return view;
 		}
 	}
}

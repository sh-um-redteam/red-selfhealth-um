/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHMedicationOverviewActivity;
import edu.umass.nursing.selfhealth.client.activities.SHMedicationOverviewActivity.OnViewMedicationHistorListener;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.records.SHMedicationRecord;
import edu.umass.nursing.selfhealth.client.viewComponents.SHConfirmableRecordRow;
import edu.umass.nursing.selfhealth.client.viewComponents.SHHeader;
import edu.umass.nursing.selfhealth.client.viewComponents.SHMedicationHeader;

public class SHMedicationHistoryFragment extends ListFragment implements OnViewMedicationHistorListener {

	private SHHeader header;
	private SHMedicationHeader medicationHeader;
	private MedicationHistoryAdapter adapter;
	private ArrayList<SHMedicationRecord> medicationHistory;
	private SHMedication medication;
	private Button addMedicationButton;
    
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
        	medicationHistory = new ArrayList<SHMedicationRecord>();
        } else {
        	medicationHistory = (ArrayList<SHMedicationRecord>) savedInstanceState.getSerializable("HISTORY");
        	medication = (SHMedication) savedInstanceState.getSerializable("MEDICATION");
        }
        
        updateMedicationHeader();
        if(adapter == null) { 
        	adapter = new MedicationHistoryAdapter(getActivity()); 
        }
        setListAdapter(adapter);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_medication_history, container, false);
        
        medicationHeader = (SHMedicationHeader) rootView.findViewById(R.id.medicationHeader);
        
        header = (SHHeader) rootView.findViewById(R.id.header);
        header.setTitle("Date Completed");
		header.setSubtitle("Time Completed");
		addMedicationButton = (Button) rootView.findViewById(R.id.add);
		
		addMedicationButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SHConfigureMedicationFragment dialog = new SHConfigureMedicationFragment();
				dialog.show(getFragmentManager(), "Configure PRN Medication");
				
			}
		});
		
        return rootView;
    }
    
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("HISTORY", medicationHistory);
		outState.putSerializable("MEDICATION", medication);
	}
	
	@Override
	public void onViewMedicationHistory(SHMedication medication1) {
		
		if(this.medication != medication1) {
			
			this.medication = medication1;
			updateMedicationHeader();
			medicationHistory = SHController.getInstance(getActivity()).getMedicationRecords(medication1.getID(), null);
			
			if(medicationHistory.size() == 0) 	header.setVisibility(View.GONE);
			else 								header.setVisibility(View.VISIBLE);
			
			adapter.notifyDataSetChanged();
		}
	}
	
	private void updateMedicationHeader() {
		if(!((SHMedicationOverviewActivity) this.getActivity()).isDualPane() && medication != null) {
			
			medicationHeader.setVisibility(View.VISIBLE);
        	medicationHeader.setMedicationName(medication.getName());
        	medicationHeader.setMedicationDosage(medication.getDetails());
        } else {
        	medicationHeader.setVisibility(View.GONE);
        }
	}
	
 	private class MedicationHistoryAdapter extends BaseAdapter {

 		public MedicationHistoryAdapter(Context context) { super(); }
 		@Override
		public long getItemId(int position) { return position; }
		@Override
 		public boolean isEnabled(int position) { return false; }
 		@Override
 		public boolean isEmpty(){ return medicationHistory.size() == 0; }
 		@Override
        public SHMedicationRecord getItem(int position) {  return medicationHistory.get(position); }
 		@Override
 		public int getCount() { return medicationHistory.size(); }
 		@Override
 		public View getView(final int position, View convertView, ViewGroup parent) {

 			final SHMedicationRecord record = getItem(position);
 			
 			SHConfirmableRecordRow view;
 		    if (convertView == null) {
 		        view = new SHConfirmableRecordRow(getActivity(), null);
 		    } else {
 		        view = (SHConfirmableRecordRow) convertView;
 		    }
 		   view.setDateTimeCompleted(record.getTimeCompleted());

 		    return view;
 		}
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHSurveyActivity.OnQuestionSelectedListener;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyQuestion;

public class SHSurveyQuestionFragment extends Fragment implements OnQuestionSelectedListener {
	
	private TextView questionHeader;
	private TextView questionBody;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_survey_question, container, false);
        questionHeader 	= (TextView) rootView.findViewById(R.id.questionHeader);
        questionBody 	= (TextView) rootView.findViewById(R.id.questionBody);
		
        return rootView;
    }
    
    @Override
    public void onQuestionSelected(SHSurveyQuestion question) {
    	questionHeader.setText("Question " + (question.getQuestionID()));
    	questionBody.setText(question.getQuestion());
    }
}

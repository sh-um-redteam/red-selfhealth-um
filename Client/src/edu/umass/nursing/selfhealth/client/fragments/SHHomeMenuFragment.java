/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHContactOverviewActivity;
import edu.umass.nursing.selfhealth.client.activities.SHHealthOverviewActivity;
import edu.umass.nursing.selfhealth.client.activities.SHMedicationOverviewActivity;
import edu.umass.nursing.selfhealth.client.activities.SHSurveyOverviewActivity;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.tools.SHSelfManagementToolCategory;
import edu.umass.nursing.selfhealth.client.viewComponents.SHMenuRow;

public class SHHomeMenuFragment extends ListFragment {
	
	private OnScheduleViewToggledListener listener;
	private int selectedIndex;
	private HomeMenuAdapter adapter;
	
	private ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
	
	ArrayList<SHSelfManagementToolCategory> categories;
	
	public interface OnScheduleViewToggledListener {
		public void viewSchedule();
		public void viewCompleted();
	}
	
	private class MenuItem {
		boolean staysPressed;
		String description;
		int iconResource;
		Runnable action;
		
		MenuItem(boolean staysPressed, String description, int iconResource, Runnable action) {
			this.staysPressed = staysPressed;
			this.description = description;
			this.iconResource = iconResource;
			this.action = action;
		}
	}
	
	private final MenuItem VIEW_SCHEDULE = new MenuItem(
			true,
			"Schedule",
			R.drawable.sh_schedule,
			new Runnable() { @Override
            public void run() {
			    	listener.viewSchedule();
			    }
			});
	
	private final MenuItem VIEW_COMPLETED = new MenuItem(
			true,
			"Completed",
			R.drawable.sh_completed,
			new Runnable() { @Override
            public void run() {
			    	listener.viewCompleted();
			    }
			});
	
	private final MenuItem VIEW_HEALTH = new MenuItem(
			false,
			"Health",
			R.drawable.sh_health,
			new Runnable() { @Override
            public void run() {
					Intent intent = new Intent(getActivity(), SHHealthOverviewActivity.class);
					startActivity(intent);
			    }
			});
	
	private final MenuItem VIEW_MEDICATION = new MenuItem(
			false,
			"Medication",
			R.drawable.sh_medication,
			new Runnable() { @Override
            public void run() {
					Intent intent = new Intent(getActivity(), SHMedicationOverviewActivity.class);
					startActivity(intent);
			    }
			});
	
	private final MenuItem VIEW_SURVEYS = new MenuItem(
			false,
			"Surveys",
			R.drawable.sh_surveys,
			new Runnable() { @Override
            public void run() {
					Intent intent = new Intent(getActivity(), SHSurveyOverviewActivity.class);
					startActivity(intent);
			    }
			});
	
	private final MenuItem VIEW_CONTACTS = new MenuItem(
			false,
			"Contacts",
			R.drawable.sh_contacts,
			new Runnable() { @Override
            public void run() {
					Intent intent = new Intent(getActivity(), SHContactOverviewActivity.class);
					startActivity(intent);
			    }
			});
	
	private final MenuItem EXPORT_HISTORY = new MenuItem(
			false,
			"Export",
			R.drawable.sh_export,
			new Runnable() { @Override
            public void run() {
				SHController.getInstance(getActivity()).exportPatientData();
			    }
			});
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        	listener = (OnScheduleViewToggledListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnScheduleViewToggledListener");
        }
    }
	
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
    		this.selectedIndex = savedInstanceState.getInt("SELECTED_INDEX");
        } 
        
        if(menuItems.isEmpty()) {
        	menuItems.add(VIEW_SCHEDULE);
        	menuItems.add(VIEW_COMPLETED);
        	menuItems.add(VIEW_CONTACTS);
			categories = SHController.getInstance(getActivity()).getActiveToolCategories();
			if(categories.contains(SHSelfManagementToolCategory.HEALTH_METRIC)) menuItems.add(VIEW_HEALTH);
			if(categories.contains(SHSelfManagementToolCategory.MEDICATION)) menuItems.add(VIEW_MEDICATION);
			if(categories.contains(SHSelfManagementToolCategory.SURVEY)) menuItems.add(VIEW_SURVEYS);
        	menuItems.add(EXPORT_HISTORY);
        }
        
        if(adapter == null) {
        	adapter = new HomeMenuAdapter(getActivity());
        }
        setListAdapter(adapter);
        
        if(menuItems.get(selectedIndex).staysPressed) {
        	getListView().setItemChecked(selectedIndex, true);
        }
	}
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	super.onCreateView(inflater, container, savedInstanceState);
    	
        View rootView = inflater.inflate(R.layout.sh_fragment_menu, container, false);
        return rootView;
    }
    
    @Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("SELECTED_INDEX", selectedIndex);
	}
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    	
    	// TODO : Cleanup this logic loop--it's doing extra work...
    	if(menuItems.get(position).staysPressed) {
    		selectedIndex = position;
    	} 
    	l.setItemChecked(selectedIndex, true);
    	menuItems.get(position).action.run();
    }
    
 	private class HomeMenuAdapter extends BaseAdapter {

 		public HomeMenuAdapter(Context context) { super(); }
 		@Override
		public long getItemId(int position) { return position; }
 		@Override
 		public MenuItem getItem(int position) { return menuItems.get(position); }
 		@Override
 		public int getCount() { return menuItems.size(); }
 		@Override
 		public View getView(int position, View convertView, ViewGroup parent) {
 			
 			final SHMenuRow view;
 		    if (convertView == null) {
 		        view = new SHMenuRow(getActivity(), null);
 		    } else {
 		        view = (SHMenuRow) convertView;
 		    }
 		    
 		    final MenuItem item = getItem(position);
 		    
	    	view.setIcon(item.iconResource);
 		    view.setDescription(item.description);
 		    
 		    return view;
 		}
 	}
}

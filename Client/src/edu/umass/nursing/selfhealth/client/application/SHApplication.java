/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.application;

import android.content.Context;

/**
 *	A bypass for instantiating the DatabaseController using a Context
 *	Useful when a Context is needed by a non-Activity
 */
public class SHApplication extends android.app.Application {

    private static SHApplication instance;

    public SHApplication() {
    	instance = this;
    }

    public static Context getContext() {
    	return instance;
    }
}

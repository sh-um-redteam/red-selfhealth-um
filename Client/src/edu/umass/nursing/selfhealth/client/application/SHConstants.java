/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.application;

public class SHConstants {
	
	public static final String NOTIFICATION_ID 			= "notification_id";
	public static final String REMINDER_ID 				= "reminder_id";
	public static final String REMINDER_MESSAGE 		= "reminder_message";
	public static final String ACTION_SEND_REMINDER 	= "send_reminder";
	public static final String ACTION_CONFIRM_REMINDER 	= "confirm_reminder";
	public static final String ACTION_SCHEDULE_UPDATE 	= "schedule_update";
	public static final String ACTION_BOOT_COMPLETED 	= "android.intent.action.BOOT_COMPLETED";
	
	public static final String AD_HOC 					= "ad_hoc"; 
	public static final String SCHEDULE_ID 				= "schedule_id";
	public static final String COMPLETION_ID 			= "completion_id";
	
	public static final String HEALTH_METRIC_ID 		= "health_metric_id";
	public static final String SURVEY_ID 				= "health_metric_id";
	public static final String MEDICATION 				= "medication";
	
	public static final int DEFAULT_NOTIFICATION_ID 	= -1;
	public static final int SNOOZE_INTERVAL_SHORT 		= 60000; //one minute
	public static final int SNOOZE_INTERVAL_LONG		= SNOOZE_INTERVAL_SHORT * 9; //9 minutes
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.application;

import java.util.Locale;

/** Represents days of the week **/
public enum SHDay {

	MONDAY 		(0),
	TUESDAY 	(1),
	WEDNESDAY 	(2),
	THURSDAY 	(3),
	FRIDAY 		(4),
	SATURDAY 	(5),
	SUNDAY 		(6),;
	
	private int weekPostion;
	
    SHDay(int weekPosition) {
    	this.weekPostion = weekPosition;
    }
    
    public int getWeekPosition() {
    	return this.weekPostion;
    }
    
    public static SHDay getEnum(int weekPosition) {
    	switch(weekPosition) {
		
		case 0 :
			return MONDAY;
		case 1 :
			return TUESDAY;
		case 2 :
			return WEDNESDAY;
		case 3 :
			return THURSDAY;
		case 4 :
			return FRIDAY;
		case 5 :
			return SATURDAY;
		case 6 :
			return SUNDAY;
		default :
			return null;
    	}
    }
    
    public static SHDay fromString(String string) {
    	
    	string = string.toLowerCase(Locale.US);
    	if(string.equals("monday")) {
    		return MONDAY;
    	} else if(string.equals("tuesday")) {
    		return TUESDAY;
    	} else if(string.equals("wednesday")) {
    		return WEDNESDAY;
    	} else if(string.equals("thursday")) {
    		return THURSDAY;
    	} else if(string.equals("friday")) {
    		return FRIDAY;
    	} else if(string.equals("saturday")) {
    		return SATURDAY;
    	} else if(string.equals("sunday")) {
    		return SUNDAY;
    	} else {
    		return null;
    	}
    }
    
    @Override
    public String toString() {
    	
		switch(this) {
			case SUNDAY :
				return "Sunday";
			case MONDAY :
				return "Monday";
			case TUESDAY :
				return "Tuesday";
			case WEDNESDAY :
				return "Wednesday";
			case THURSDAY :
				return "Thursday";
			case FRIDAY :
				return "Friday";
			case SATURDAY :
				return "Saturday";
			default :
				return null;
		}
    }
    
    public String toShorthandString() {
    	
		switch(this) {
			case SUNDAY :
				return "Sun";
			case MONDAY :
				return "Mon";
			case TUESDAY :
				return "Tue";
			case WEDNESDAY :
				return "Wed";
			case THURSDAY :
				return "Thu";
			case FRIDAY :
				return "Fri";
			case SATURDAY :
				return "Sat";
			default :
				return null;
		}
    }
    
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.medication;

import java.io.Serializable;

import edu.umass.nursing.selfhealth.client.application.SHApplication;
import edu.umass.nursing.selfhealth.client.controller.SHController;

/**
 *	Key for accessing an SHMedication
 */
@SuppressWarnings("serial")
public class SHMedicationID implements Serializable{

	private int medicationID;

	public SHMedicationID(int medicationID) {
		this.medicationID = medicationID;
	}
	
	public int value() {
		return this.medicationID;
	}
	
	/**
	 * @param id
	 * @return Generates the matching medication for the specified ID
	 */
	public static SHMedication medication(SHMedicationID id) {
		return SHController.getInstance(SHApplication.getContext()).getMedication(id);
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.medication;

import java.io.Serializable;

/**
 *	Structured representation of a medication
 */
@SuppressWarnings("serial")
public class SHMedication implements Serializable {

	private SHMedicationID id; 
	private String name;
	private String form;
	private String units;
	private double dosage;
	private double quantity;
	
	public SHMedication(SHMedicationID id, String name, String form, String units, double dosage, double quantity) {

		this.id = id;
		this.name = name;
		this.form = form;
		this.units = units;
		this.dosage = dosage;
		this.quantity = quantity;
	}

	public SHMedicationID getID() 	{ return id; }
	public String getName() 		{ return name; }
	public String getForm() 		{ return form; }
	public String getUnits() 		{ return units; }
	public double getDosage() 		{ return dosage; }
	public double getQuantity()		{ return quantity; }
	
	/**
	 * @return A string representing the finer details of the medication
	 * (quantity, dosage, units, form)
	 */
	public String getDetails() {
		return (quantity + "x " + dosage + units + " " + form);
	}
	
	@Override
	public String toString() {
		return name + ": " + getDetails();
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

/**
 *	Measurable component of a health metric and its associated units.
 *	Any new additional components must be appended to this list--modification to the 
 *	existing arguments will result in improper metrics being dispensed throughout the application.
 */
public enum SHHealthMetricComponent {

	STEPS				(0, "Steps"),
	DISTANCE			(1, "Distance"),
	CALORIES			(2, "Calories"),
	BLOOD_GLUCOSE 		(3, "Blood Glucose"),
	OXYGEN_SATURATION 	(4, "Oxygen"),
	SYSTOLIC 			(5, "Systolic"),
	DIASTOLIC 			(6, "Diastolic"),
	HEART_RATE 			(7, "Heart Rate"),
	WEIGHT 				(8, "Weight");
	
	private int id;
	private String typeString; 
	
    private SHHealthMetricComponent(int id, String typeString) { 
    	this.id = id;
        this.typeString = typeString; 
    }
    
    public SHHealthMetricUnit unit() {
    	switch (this) {
    		case STEPS:
    			return SHHealthMetricUnit.STEPS;
    		case DISTANCE:
    			return SHHealthMetricUnit.FEET;
    		case CALORIES:
    			return SHHealthMetricUnit.CALORIES;
    		case BLOOD_GLUCOSE:
    			return SHHealthMetricUnit.BLOOD_GLUCOSE;
    		case OXYGEN_SATURATION:
    			return SHHealthMetricUnit.OXYGEN_SATURATION;
    		case SYSTOLIC:
    			return SHHealthMetricUnit.SYSTOLIC;
    		case DIASTOLIC:
    			return SHHealthMetricUnit.DIASTOLIC;
    		case HEART_RATE:
    			return SHHealthMetricUnit.HEART_RATE;
    		case WEIGHT:
    			return SHHealthMetricUnit.WEIGHT;
			default:
				break;
    	}
    	return null;
    }
    
    public static SHHealthMetricComponent fromID(int id) {
    	switch(id) {
	    	case(0): return STEPS;
			case(1): return DISTANCE;
			case(2): return CALORIES;
			case(3): return BLOOD_GLUCOSE;
			case(4): return OXYGEN_SATURATION;
			case(5): return SYSTOLIC;
			case(6): return DIASTOLIC;
			case(7): return HEART_RATE;
			case(8): return WEIGHT;
			default: return null;
    	}
    }
    
    public int id() {
    	return this.id;
    }
    
    @Override 
    public String toString(){ 
        return typeString; 
    } 
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;


public class SHBloodGlucoseMetric extends SHHealthMetric {
	
	public SHBloodGlucoseMetric() {
		super();
		super.addComponent(SHHealthMetricComponent.BLOOD_GLUCOSE);
	}
	
	@Override
	public void addComponent(SHHealthMetricComponent component) { }
}

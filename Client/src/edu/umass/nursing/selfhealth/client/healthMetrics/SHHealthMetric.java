/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

import java.util.ArrayList;

/**
 *	Structured representation of a measurable health metric.
 *	Each Metric is composed of one or more components.
 *	For example, SHBloodPressureMetric is composed of systolic and diastolic
 *	whereas Weight is a single component--weight.
 *
 *	A health metric is generated from a {@link SHHealthMetricID} so that the 
 *	user can see what components are required when they are taking a measurement, 
 *	or so we know what measurement components to retrieve from a 
 * {@link SHMeasurableHealthMetricRecord} when a user views their history.
 */
public class SHHealthMetric {
	
	private ArrayList<SHHealthMetricComponent> components;
	
	public SHHealthMetric() {
		this.components = new ArrayList<SHHealthMetricComponent>();
	}
	
	public void addComponent(SHHealthMetricComponent component) {
		this.components.add(component);
	}
	
	public ArrayList<SHHealthMetricComponent> components() {
		return components;
	}
}

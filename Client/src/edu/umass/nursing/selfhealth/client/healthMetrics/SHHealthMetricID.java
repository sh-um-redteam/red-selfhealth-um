/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

import edu.umass.nursing.selfhealth.client.R;

/**
 * 	Key for specifying the type of health metric to be measured.
 * 	Any additional health metrics must be appended to this list.
 */
public enum SHHealthMetricID {

	ACTIVITY		("Activity"),
	BLOOD_GLUCOSE	("Blood Glucose"),
	BLOOD_OXYGEN	("Blood Oxygen"),
	BLOOD_PRESSURE	("Blood Pressure"),
	HEART_RATE		("Heart Rate"),
	WEIGHT			("Weight"),
	MEAL			("Meal"),
	WATER			("Water");

	private String healthMetricString; 
	
    private SHHealthMetricID(String healthMetricString) { 
        this.healthMetricString = healthMetricString; 
    } 
    
    /**
     * @return Whether the metric confirmable--eg, not measurable
     */
    public boolean isConfirmable() {
    	return (this == MEAL || this == WATER);
    }
    
    public static SHHealthMetricID fromString(String string) {
    	if(string.equals(ACTIVITY.toString())) {
    		return ACTIVITY;
    	} else if(string.equals(BLOOD_GLUCOSE.toString())) {
    		return BLOOD_GLUCOSE;
    	} else if(string.equals(BLOOD_OXYGEN.toString())) {
    		return BLOOD_OXYGEN;
    	} else if(string.equals(BLOOD_PRESSURE.toString())) {
    		return BLOOD_PRESSURE;
    	} else if(string.equals(HEART_RATE.toString())) {
    		return HEART_RATE;
    	} else if(string.equals(WEIGHT.toString())) {
    		return WEIGHT;
    	} else if(string.equals(MEAL.toString())) {
    		return MEAL;
    	} else if(string.equals(WATER.toString())) {
    		return WATER;
    	} else {
    		return null;
    	}
    }
    
    /**
     * @param healthMetricID 
     * @return Generates the matching health metric for the specified ID
     */
    public static SHHealthMetric generateHealthMetric(SHHealthMetricID healthMetricID) {
    	if(healthMetricID.isConfirmable()) return null;
    	return SHHealthMetricFactory.INSTANCE.healthMetric(healthMetricID);
    }
    
    @Override 
    public String toString(){ 
        return healthMetricString; 
    }

	public Integer icon() {
		switch(this) {
			case ACTIVITY:
				return R.drawable.sh_activity;
			case BLOOD_GLUCOSE:
				return R.drawable.sh_blood_glucose;
			case BLOOD_OXYGEN:
				return R.drawable.sh_blood_oxygen;
			case BLOOD_PRESSURE:
				return R.drawable.sh_blood_pressure;
			case HEART_RATE:
				return R.drawable.sh_heart_rate;
			case WEIGHT:
				return R.drawable.sh_weight;
			case MEAL:
				return R.drawable.sh_meal;
			case WATER:
				return R.drawable.sh_water;
			default:
				return -1;
		}
	} 
}

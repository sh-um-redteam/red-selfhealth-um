/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

/**
 *	Measurement unit for a health metric component.
 *	Additional units should be appended to this list.
 */
public enum SHHealthMetricUnit {

	STEPS				("Steps"),
	MILES				("mi"),
	FEET				("ft"),
	KILOMETERS			("km"),
	METERS				("m"),
	CALORIES			("cal"),
	BLOOD_GLUCOSE 		("mg/dL"),
	OXYGEN_SATURATION 	("SpO2"),
	SYSTOLIC 			("mmHG"),
	DIASTOLIC 			("mmHG"),
	HEART_RATE 			("bpm"),
	WEIGHT 				("lbs");
	
	private String unitString; 
	
    private SHHealthMetricUnit(String unitString) { 
        this.unitString = unitString; 
    } 
    
    @Override 
    public String toString(){ 
        return unitString; 
    } 
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

public class SHHeartRateMetric extends SHHealthMetric {
	
	public SHHeartRateMetric() {
		super();
		super.addComponent(SHHealthMetricComponent.HEART_RATE);
	}
	
	@Override
	public void addComponent(SHHealthMetricComponent component) { }
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

public class SHActivityMetric extends SHHealthMetric {
	
	public SHActivityMetric() {
		super();
		super.addComponent(SHHealthMetricComponent.STEPS);
	}
	
	@Override
	public void addComponent(SHHealthMetricComponent component) { }
}
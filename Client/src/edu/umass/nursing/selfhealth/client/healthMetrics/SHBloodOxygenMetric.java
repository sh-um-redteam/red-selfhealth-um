/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

public class SHBloodOxygenMetric extends SHHealthMetric {
	
	public SHBloodOxygenMetric() {
		super();
		super.addComponent(SHHealthMetricComponent.OXYGEN_SATURATION);
	}
	
	@Override
	public void addComponent(SHHealthMetricComponent component) { }
}

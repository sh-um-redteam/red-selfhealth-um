/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

public class SHBloodPressureMetric extends SHHealthMetric {
	
	public SHBloodPressureMetric() {
		super();
		super.addComponent(SHHealthMetricComponent.SYSTOLIC);
		super.addComponent(SHHealthMetricComponent.DIASTOLIC);
	}
	
	@Override
	public void addComponent(SHHealthMetricComponent component) { }
}

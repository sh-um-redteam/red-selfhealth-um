/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

/**
 * 	Provides access the the Health Metrics that may be used by the application.
 */
public enum SHHealthMetricFactory {
	
	INSTANCE;

	private static final SHActivityMetric activityMetric 			= new SHActivityMetric();
	private static final SHBloodGlucoseMetric bloodGlucoseMetric 	= new SHBloodGlucoseMetric();
	private static final SHBloodOxygenMetric bloodOxygenMetric 		= new SHBloodOxygenMetric();
	private static final SHBloodPressureMetric bloodPressureMetric	= new SHBloodPressureMetric();
	private static final SHHeartRateMetric heartRateMetric 			= new SHHeartRateMetric();
	private static final SHWeightMetric weightMetric 				= new SHWeightMetric();
	
	public SHHealthMetric healthMetric(SHHealthMetricID id) {
		
		switch(id) {
			case ACTIVITY :
				return activityMetric;
			case BLOOD_GLUCOSE :
				return bloodGlucoseMetric;
			case BLOOD_OXYGEN :
				return bloodOxygenMetric;
			case BLOOD_PRESSURE :
				return bloodPressureMetric;
			case HEART_RATE :
				return heartRateMetric;
			case WEIGHT :
				return weightMetric;
			default:
				break;
		}
		return null;
	}
}

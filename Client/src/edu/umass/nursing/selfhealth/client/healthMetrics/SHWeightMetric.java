/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.healthMetrics;

public class SHWeightMetric extends SHHealthMetric {
	
	public SHWeightMetric() {
		super();
		super.addComponent(SHHealthMetricComponent.WEIGHT);
	}
	
	@Override
	public void addComponent(SHHealthMetricComponent component) { }
}

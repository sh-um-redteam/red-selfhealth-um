/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.patient;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Represents the SHPatient for whom the application is configured. Name, gender,
 * and date of birth are used by the provider to identify the patient.
 */
public class SHPatient {

	private int id;
	private String firstName;
	private String lastName;
	private String gender;
	private LocalDate dateOfBirth;

	public SHPatient(int id, String firstName, String lastName, String gender, LocalDate dateOfBirth) {
		this.setId(id);
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
	}

	public int getId() 										{ return id; }
	public void setId(int id) 								{ this.id = id; }
	public void setName(String firstName, String lastName) 	{ this.firstName = firstName; this.lastName = lastName; }
	public void setGender(String gender) 					{ this.gender = gender; }
	public void setDateOfBirth(LocalDate dateOfBirth) 		{ this.dateOfBirth = dateOfBirth; }
	public String getFirstName() 							{ return firstName; }
	public String getLastName() 							{ return lastName; }
	public String getGender() 								{ return gender; }
	public LocalDate getDateOfBirth() 						{ return dateOfBirth; }

	/**
	 * @return The age of the SHPatient--convenience method that calculates the
	 *         patient age based on current date and their date of birth.
	 */
	public int getAge() {
		LocalDate now = new LocalDate();
		Years age = Years.yearsBetween(dateOfBirth, now);
		return age.getYears();
	}
	
	/**
	 * 	Constructor for parsing a JSON object in the configuration file.
	 */
	public static SHPatient fromJSON(JSONObject json) {
		
		try{
			int 		patientID 		= json.getInt("patientID");
			String 		firstName		= json.getString("firstName");
			String 		lastName		= json.getString("lastName");
			String 		gender			= json.getString("gender");
			LocalDate	dateOfBirth		= DateTimeFormat.forPattern("YYYY-M-d").parseLocalDate(json.getString("dateOfBirth"));
			
			return new SHPatient(patientID, firstName, lastName, gender, dateOfBirth);
		}
		catch(JSONException jse)
		{
			Log.e("PATIENT CONFIGURATION", "Error creating a patient from JSON:/n" + jse.toString());
			return null;
		}
	}
}

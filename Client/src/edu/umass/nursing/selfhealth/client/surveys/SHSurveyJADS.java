/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

import java.util.LinkedHashMap;

import android.util.Pair;

/**
 *	NOTE: Changes to existing question/responses ordering will invalidate any existing response maps in the database.
 */
public class SHSurveyJADS extends SHSurvey {

	public SHSurveyJADS() {
		super();
		setup();
	}
	
	private void setup() {
		
		LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>> surveyStructure
		 = new LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>>();
		
		String[] questions = new String[] {
				"I have been polite to other people.",
				"I have tried not to judge people before I get to know them.",
				"I have tried to do things for other people.",
				"I have considered other people's feelings before speaking.",
				"I believe other people have treated me as an equal.",
				"People have enjoyed my company.",
				"I have been reliable.",
				"I have been sensitive to the needs of others.",
				"I believe people have respected me.",
				"I have had a sense of purpose.",
				"My life has had meaning.",
				"I have been honest.",
				"I have treated other people with respect.",
				"I have laughed at myself.",
				"I have respected myself.",
				"I have avoided saying or doing things that might hurt other people.",
				"My manners have been important to me.",
				"I think I have made a difference."
		};
		
		/////////// BUILD SURVEY BELOW //////////////////////////////////////////////////////////////
		
		LinkedHashMap<Integer, SHSurveyResponse> rs1 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs1.put(1, new SHSurveyResponse(1, "Untrue", 1));
		rs1.put(2, new SHSurveyResponse(2, "Somewhat untrue", 2));
		rs1.put(3, new SHSurveyResponse(3, "Mostly true", 3));
		rs1.put(4, new SHSurveyResponse(4, "Completely true", 4));
		
		for(int i = 0; i<questions.length; i++) {
			int id = i+1;
			final SHSurveyQuestion q = new SHSurveyQuestion(id, questions[i]);
			final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p 
				= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q, rs1);
			surveyStructure.put(q.getQuestionID(), p);
		}
		
		/////////// BUILD SURVEY ABOVE //////////////////////////////////////////////////////////////
		
		super.initialize(surveyStructure);
	}
}

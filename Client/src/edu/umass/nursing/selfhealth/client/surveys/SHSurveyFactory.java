/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

/**
 * Dispenses the appropriate survey for the given SurveyID
 */
public enum SHSurveyFactory {
	
	INSTANCE;

	private static final SHSurveyADL adl 			= new SHSurveyADL();
	private static final SHSurveyAttitude attitude 	= new SHSurveyAttitude();
	private static final SHSurveyIADL iadl 			= new SHSurveyIADL();
	private static final SHSurveyJADS jads 			= new SHSurveyJADS();
	private static final SHSurveyLSNS lsns 			= new SHSurveyLSNS();
	private static final SHSurveySOCS socs 			= new SHSurveySOCS();
	
	public SHSurvey getSurvey(SHSurveyID surveyID) {
		
		switch(surveyID) {
			case ADL :
				return adl;
			case ATTITUDE :
				return attitude;
			case JADS :
				return jads;
			case IADL :
				return iadl;
			case LSNS :
				return lsns;
			case SOCS :
				return socs;
			default:
				break;
		}
	return null;
	
	}
	
}

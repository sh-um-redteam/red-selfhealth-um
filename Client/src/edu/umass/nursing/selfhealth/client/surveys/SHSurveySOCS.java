/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

import java.util.LinkedHashMap;

import android.util.Pair;

/**
 *	NOTE: Changes to existing question/responses ordering will invalidate any existing response maps in the database.
 */
public class SHSurveySOCS extends SHSurvey {

	public SHSurveySOCS() {
		super();
		setup();
	}
	
	private void setup() {
		
		LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>> surveyStructure
		 = new LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>>();
		
		String[] questions = new String[] {
				"I do not have the ability to handle events that are occurring in my life.",
				"The responsibilities I have are too much to bear.",
				"My resources are not adequate to handle what I have to deal with right now.",
				"My current situation is under control.",
				"I do not think I can do what is required of me.",
				"I have adequate coping skills to use to meet current demands.",
				"I accomplish things in my daily life that are important to me.",
				"I cannot cope with my current situation.",
				"Everything is running smoothly.",
				"The situation in which I am now is too difficult for me to handle.",
				"I do not have any say in what is happening in my life.",
				"I am on top of things.",
				"Things are going along as planned.",
				"I have the resources I need to deal with my situation.",
				"I can't hang in there much longer."
		};
		
		/////////// BUILD SURVEY BELOW //////////////////////////////////////////////////////////////
		
		LinkedHashMap<Integer, SHSurveyResponse> rs1 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs1.put(1, new SHSurveyResponse(1, "Strongly agree", 1));
		rs1.put(2, new SHSurveyResponse(2, "Moderately agree", 2));
		rs1.put(3, new SHSurveyResponse(3, "Moderately disagree", 3));
		rs1.put(4, new SHSurveyResponse(4, "Strongly disagree", 4));
		rs1.put(5, new SHSurveyResponse(5, "Not Applicable", 9));
		
		for(int i = 0; i<questions.length; i++) {
			int id = i+1;
			final SHSurveyQuestion q = new SHSurveyQuestion(id, questions[i]);
			final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p 
				= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q, rs1);
			surveyStructure.put(q.getQuestionID(), p);
		}
		
		/////////// BUILD SURVEY ABOVE //////////////////////////////////////////////////////////////
		
		super.initialize(surveyStructure);
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

import edu.umass.nursing.selfhealth.client.R;

/**
 *	Key for accessing an SHSurvey
 */
public enum SHSurveyID {

	ADL			("Activities of Daily Living"),					
	ATTITUDE	("Attitude Assessment"),				
	IADL		("Instrumental Activities"),
	JADS		("Dignity Scale"),
	LSNS		("Social Network"),						
	SOCS		("Sense Of Control");			

	private String surveyString; 
	
    private SHSurveyID(String surveyString) { 
        this.surveyString = surveyString; 
    } 
    
    public static SHSurveyID fromString(String string) {
    	
    	if(string.equals(ADL.toString())) {
    		return ADL;
    	} else if(string.equals(ATTITUDE.toString())) {
    		return ATTITUDE;
    	} else if(string.equals(IADL.toString())) {
    		return IADL;
    	} else if(string.equals(JADS.toString())) {
    		return JADS;
    	} else if(string.equals(LSNS.toString())) {
    		return LSNS;
    	} else if(string.equals(SOCS.toString())) {
    		return SOCS;
    	} else {
    		return null;
    	}
    }
    
    public static SHSurvey generateSurvey(SHSurveyID surveyID) {
    	return SHSurveyFactory.INSTANCE.getSurvey(surveyID);
    }
    
    @Override 
    public String toString(){ 
        return surveyString; 
    }
    
    public String toTitleString() {
    	switch(this) {
		case ADL:
			return "Daily Activities";
		case ATTITUDE:
			return "Attitude Assessment";
		case IADL:
			return "Instrumental Activities";
		case JADS:
			return "Dignity Scale";
		case LSNS:
			return "Social Network";
		case SOCS:
			return "Sense Of Control";
		default:
			return null;
	}
    }

	public Integer icon() {
		switch(this) {
			case ADL:
				return R.drawable.sh_adl;
			case ATTITUDE:
				return R.drawable.sh_attitude;
			case IADL:
				return R.drawable.sh_iadl;
			case JADS:
				return R.drawable.sh_jads;
			case LSNS:
				return R.drawable.sh_lsns;
			case SOCS:
				return R.drawable.sh_socs;
			default:
				return -1;
		}
	} 
}

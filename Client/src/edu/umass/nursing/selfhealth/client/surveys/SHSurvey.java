/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.annotation.SuppressLint;
import android.util.Pair;

/**
 *	A survey--includes questions, responses, and logic for scoring and response mapping.
 *
 *	NOTE: Changes to existing concrete subclass survey question/responses will invalidate the response map in the database.
 */
@SuppressLint("UseSparseArrays")
public abstract class SHSurvey {

	// Main Data Structure
	private LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>> surveyStructure;
		
	private final Integer POINTS_NOT_INITIALIZED = -999;
	private int maximumPossiblePoints = POINTS_NOT_INITIALIZED;
	private int maxThreshold = 0;
	private int minThreshold = 0;
	
	public SHSurvey() {
		surveyStructure = new LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>>();
	}
	
	public void initialize(LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>> surveyStructure1) {
		this.surveyStructure = surveyStructure1;
	}
	
	/**
	 * 	Should be called once during initialization of an Activity for taking surveys.
	 */
	public ArrayList<Pair<SHSurveyQuestion, ArrayList<SHSurveyResponse>>> getQuestionsAndResponseOptions() {
		
		ArrayList<Pair<SHSurveyQuestion, ArrayList<SHSurveyResponse>>> questionAndResponseOptions
			= new ArrayList<Pair<SHSurveyQuestion, ArrayList<SHSurveyResponse>>>();
			
		for(Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> pair : surveyStructure.values()) {
			
			questionAndResponseOptions.add(
					new Pair<SHSurveyQuestion, ArrayList<SHSurveyResponse>>(
							pair.first, new ArrayList<SHSurveyResponse>(pair.second.values())));
		}
		return questionAndResponseOptions;
	}
	
	/**
	 * 	Returns a compact question-response pair
	 * 	Null response if there was no response selected by the user
	 * 	Useful for reconstructing a user's responses to a survey.  This can be used when exporting data
	 *  so the Provider has a human-readable format of the Patient's survey response data.
	 */
	public Pair<SHSurveyQuestion, SHSurveyResponse> getQuestionAndResponse(int questionID, int responseID) {
		
		Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> pair = surveyStructure.get(questionID);
		
		if(responseID == SHSurveyResponse.NO_RESPONSE_ID) {
			return new Pair<SHSurveyQuestion, SHSurveyResponse>(pair.first, null);
		} else {
			return new Pair<SHSurveyQuestion, SHSurveyResponse>(pair.first, pair.second.get(responseID));
		}
	}
	
	/**
	 * @param questionID	The ID of the question in the survey
	 * @param responseID	The ID of the response for which a value is desired
	 * @return				The point-value of the response
	 */
	public int getValueForResponse(int questionID, int responseID) {
		if(responseID == SHSurveyResponse.NO_RESPONSE_ID) {
			return SHSurveyResponse.NO_RESPONSE_VALUE;
		} else {
			return surveyStructure.get(questionID).second.get(responseID).value();
		}
	}
	
	
	/**
	 * @return The max value among responses for a particular question
	 */
	public int getMaxPossibleValue(int questionID) {
		
		int maxValue = 0;
		for(SHSurveyResponse sr : surveyStructure.get(questionID).second.values()) {
			int value = sr.value();
			if(value > maxValue) {
				maxValue = value;
			}
		}
		return maxValue;
	}
	
	/**
	 * @return	The total points possible for the survey
	 */
	public int getMaximumPossibleScore() {
		
		// Only compute once--expensive operation
		if(maximumPossiblePoints == POINTS_NOT_INITIALIZED) {
			for(Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> pair : surveyStructure.values()) {
				for(SHSurveyResponse sr : pair.second.values()) {
					maximumPossiblePoints += sr.value();
				} 
			}
		}
		return maximumPossiblePoints;
	}

	/**
	 * @return	The minimum PRN threshold for the survey
	 */
	public int getMinThreshold() {
		return minThreshold;
	}
	
	/**
	 * @return	The maximum PRN threshold for the survey
	 */
	public int getMaxThreshold() {
		return maxThreshold;
	}
	
	/**
	 * @return	The number of questions in the survey
	 */
	public int size() {
		return surveyStructure.size();
	}
}

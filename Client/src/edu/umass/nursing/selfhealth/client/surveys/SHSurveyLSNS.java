/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

import java.util.LinkedHashMap;

import android.util.Pair;

/**
 *	NOTE: Changes to existing question/responses ordering will invalidate any existing response maps in the database.
 */
public class SHSurveyLSNS extends SHSurvey {

	public SHSurveyLSNS() {
		super();
		setup();
	}
	
	private void setup() {
		
		LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>> surveyStructure
		 = new LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>>();
		
		/////////// BUILD SURVEY BELOW //////////////////////////////////////////////////////////////
		
		// Numeric Responses
		LinkedHashMap<Integer, SHSurveyResponse> numericResponses = new LinkedHashMap<Integer, SHSurveyResponse>();
		numericResponses.put(1, new SHSurveyResponse(1, "Zero", 0));
		numericResponses.put(2, new SHSurveyResponse(2, "One", 1));
		numericResponses.put(3, new SHSurveyResponse(3, "Two", 2));
		numericResponses.put(4, new SHSurveyResponse(4, "Three or four", 3));
		numericResponses.put(5, new SHSurveyResponse(5, "Five to eight", 4));
		numericResponses.put(6, new SHSurveyResponse(6, "Nine or more", 5));
		
		// Frequency Responses 1
		LinkedHashMap<Integer, SHSurveyResponse> frequencyOptions1 = new LinkedHashMap<Integer, SHSurveyResponse>();
		frequencyOptions1.put(1, new SHSurveyResponse(1, "Always", 5));
		frequencyOptions1.put(2, new SHSurveyResponse(2, "Very often", 4));
		frequencyOptions1.put(3, new SHSurveyResponse(3, "Often", 3));
		frequencyOptions1.put(4, new SHSurveyResponse(4, "Sometimes", 2));
		frequencyOptions1.put(5, new SHSurveyResponse(5, "Seldom", 1));
		frequencyOptions1.put(6, new SHSurveyResponse(6, "Never", 0));
		
		// Frequency Responses 2
		LinkedHashMap<Integer, SHSurveyResponse> frequencyOptions2 = new LinkedHashMap<Integer, SHSurveyResponse>();
		frequencyOptions2.put(1, new SHSurveyResponse(1, "Less than monthly", 0));
		frequencyOptions2.put(2, new SHSurveyResponse(2, "Monthly", 1));
		frequencyOptions2.put(3, new SHSurveyResponse(3, "A few times a month", 2));
		frequencyOptions2.put(4, new SHSurveyResponse(4, "Weekly", 3));
		frequencyOptions2.put(5, new SHSurveyResponse(5, "A few times a week", 4));
		frequencyOptions2.put(6, new SHSurveyResponse(6, "Daily", 5));
		
		// Boolean Responses
		LinkedHashMap<Integer, SHSurveyResponse> yesnoOptions = new LinkedHashMap<Integer, SHSurveyResponse>();
		yesnoOptions.put(1, new SHSurveyResponse(1, "Yes", 1));
		yesnoOptions.put(2, new SHSurveyResponse(2, "No", 0));
		
		// With Who Responses
		LinkedHashMap<Integer, SHSurveyResponse> withWhoOptions = new LinkedHashMap<Integer, SHSurveyResponse>();
		withWhoOptions.put(1, new SHSurveyResponse(1, "With spouse", 5));
		withWhoOptions.put(2, new SHSurveyResponse(2, "With other relatives/friends", 4));
		withWhoOptions.put(3, new SHSurveyResponse(3, "Unrelated indiviaduals (paid help)", 1));
		withWhoOptions.put(4, new SHSurveyResponse(4, "Alone", 0));
		
		// Question 1
		final SHSurveyQuestion q1 = new SHSurveyQuestion(1, "How many relatives do you see or hear from at least once a week? (note: include in-law relatives)");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p1 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q1, numericResponses);
		surveyStructure.put(q1.getQuestionID(), p1);
		
		// Question 2
		final SHSurveyQuestion q2 = new SHSurveyQuestion(2, "How often in a week do you see or hear from the relative whom you have most contact with?");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p2 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q2, numericResponses);
		surveyStructure.put(q2.getQuestionID(), p2);
		
		// Question 3
		final SHSurveyQuestion q3 = new SHSurveyQuestion(3, "How many relatives do you feel close to? That is, how many of them do you feel at ease with, can talk to about private matters, or can call for help?");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p3 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q3, numericResponses);
		surveyStructure.put(q3.getQuestionID(), p3);
		
		// Question 4
		final SHSurveyQuestion q4 = new SHSurveyQuestion(4, "Do you have any close friends? That is, do you have any friends with whom you feel at ease, can talk to about private matters, or can call for help? If so, how many?");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p4 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q4, numericResponses);
		surveyStructure.put(q4.getQuestionID(), p4);
		
		// Question 5
		final SHSurveyQuestion q5 = new SHSurveyQuestion(5, "How many of these friends do you see or hear from at least once a week?");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p5 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q5, numericResponses);
		surveyStructure.put(q5.getQuestionID(), p5);
		
		// Question 6
		final SHSurveyQuestion q6 = new SHSurveyQuestion(6, "How often do you see or hear from the friend with whom you have the most contact?");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p6 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q6, frequencyOptions2);
		surveyStructure.put(q6.getQuestionID(), p6);
		
		// Question 7
		final SHSurveyQuestion q7 = new SHSurveyQuestion(7, "When you have an important decision to make, do you have someone you can talk to about it?");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p7 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q7, frequencyOptions1);
		surveyStructure.put(q7.getQuestionID(), p7);
		
		// Question 8
		final SHSurveyQuestion q8 = new SHSurveyQuestion(8, "When other people you know have an important decision to make, do they talk to you about it?");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p8 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q8, frequencyOptions1);
		surveyStructure.put(q8.getQuestionID(), p8);
		
		// Question 9
		final SHSurveyQuestion q9 = new SHSurveyQuestion(9, "Does anybody rely on you to do something for them each day (e.g. shopping, cooking dinner, doing repairs, cleaning house, providing child care, etc.)?");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p9 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q9, yesnoOptions);
		surveyStructure.put(q9.getQuestionID(), p9);
		
		// Question 10
		final SHSurveyQuestion q10 = new SHSurveyQuestion(10, "Do you help anybody with things like shopping, filling out forms, doing repair, and providing childcare? If someone relies on you, select 'Always'.");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p10 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q10, frequencyOptions1);
		surveyStructure.put(q10.getQuestionID(), p10);
		
		// Question 11
		final SHSurveyQuestion q11 = new SHSurveyQuestion(11, "Do you live alone or with other people? (note: include in-laws with the relatives)");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p11 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q11, withWhoOptions);
		surveyStructure.put(q11.getQuestionID(), p11);
		
		/////////// BUILD SURVEY ABOVE //////////////////////////////////////////////////////////////
		
		super.initialize(surveyStructure);
	}
}

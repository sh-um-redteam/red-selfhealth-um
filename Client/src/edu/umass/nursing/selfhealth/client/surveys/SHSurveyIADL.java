/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

import java.util.LinkedHashMap;

import android.util.Pair;

/**
 *	NOTE: Changes to existing question/responses ordering will invalidate any existing response maps in the database.
 */
public class SHSurveyIADL extends SHSurvey {

	public SHSurveyIADL() {
		super();
		setup();
	}
	
	private void setup() {
		
		LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>> surveyStructure
		 = new LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>>();
		
		/////////// BUILD SURVEY BELOW //////////////////////////////////////////////////////////////
		
		// Question 1
		LinkedHashMap<Integer, SHSurveyResponse> rs1 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs1.put(1, new SHSurveyResponse(1, "Look up and dial numbers.", 1));
		rs1.put(2, new SHSurveyResponse(2, "Only dial a few well known numbers.", 1));
		rs1.put(3, new SHSurveyResponse(3, "Will answer but not dial.", 1));
		rs1.put(4, new SHSurveyResponse(4, "Do not use the telephone.", 0));
		final SHSurveyQuestion q1 = new SHSurveyQuestion(1, "When using the telephone, I...");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p1 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q1, rs1);
		surveyStructure.put(q1.getQuestionID(), p1);
		
		// Question 2
		LinkedHashMap<Integer, SHSurveyResponse> rs2 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs2.put(1, new SHSurveyResponse(1, "Take care of all shopping independently.", 1));
		rs2.put(2, new SHSurveyResponse(2, "Shop independently for small purchases.", 0));
		rs2.put(3, new SHSurveyResponse(3, "Need to be accompanied.", 0));
		rs2.put(4, new SHSurveyResponse(4, "Am unable to shop.", 0));
		final SHSurveyQuestion q2 = new SHSurveyQuestion(2, "When shopping, I...");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p2 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q2, rs2);
		surveyStructure.put(q2.getQuestionID(), p2);
		
		// Question 3
		LinkedHashMap<Integer, SHSurveyResponse> rs3 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs3.put(1, new SHSurveyResponse(1, "Plan and serve meals independently.", 1));
		rs3.put(2, new SHSurveyResponse(2, "Am able to cook if supplied with ingredients.", 0));
		rs3.put(3, new SHSurveyResponse(3, "Do not maintain an adequate diet.", 0));
		rs3.put(4, new SHSurveyResponse(4, "Need to have meals prepared and served for me.", 0));
		final SHSurveyQuestion q3 = new SHSurveyQuestion(3, "When preparing food, I...");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p3 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q3, rs3);
		surveyStructure.put(q3.getQuestionID(), p3);
		
		// Question 4
		LinkedHashMap<Integer, SHSurveyResponse> rs4 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs4.put(1, new SHSurveyResponse(1, "Maintain alone with occasional assistance.", 1));
		rs4.put(2, new SHSurveyResponse(2, "Perform light daily tasks.", 1));
		rs4.put(3, new SHSurveyResponse(3, "Cannot maintain an acceptable level of cleanliness.", 1));
		rs4.put(4, new SHSurveyResponse(4, "Need help with all maintenance tasks.", 1));
		rs4.put(5, new SHSurveyResponse(5, "Do not participate.", 0));
		final SHSurveyQuestion q4 = new SHSurveyQuestion(4, "When keeping up with the house, I...");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p4 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q4, rs4);
		surveyStructure.put(q4.getQuestionID(), p4);
		
		// Question 5
		LinkedHashMap<Integer, SHSurveyResponse> rs5 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs5.put(1, new SHSurveyResponse(1, "I can do all of it completely.", 1));
		rs5.put(2, new SHSurveyResponse(2, "I launder small items like socks.", 1));
		rs5.put(3, new SHSurveyResponse(3, "Someone must do it for me.", 0));
		final SHSurveyQuestion q5 = new SHSurveyQuestion(5, "When doing laundry...");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p5 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q5, rs5);
		surveyStructure.put(q5.getQuestionID(), p5);
		
		// Question 6
		LinkedHashMap<Integer, SHSurveyResponse> rs6 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs6.put(1, new SHSurveyResponse(1, "Drive my own car or use public transit.", 1));
		rs6.put(2, new SHSurveyResponse(2, "Arrange my travel via taxi.", 1));
		rs6.put(3, new SHSurveyResponse(3, "Use public transit when accompanied with someone.", 1));
		rs6.put(4, new SHSurveyResponse(4, "Use a taxi or car with assistance.", 0));
		rs6.put(5, new SHSurveyResponse(5, "Do not travel at all.", 0));
		final SHSurveyQuestion q6 = new SHSurveyQuestion(6, "When traveling, I...");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p6 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q6, rs6);
		surveyStructure.put(q6.getQuestionID(), p6);
		
		// Question 7
		LinkedHashMap<Integer, SHSurveyResponse> rs7 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs7.put(1, new SHSurveyResponse(1, "Am responsible for managing times and dosages.", 1));
		rs7.put(2, new SHSurveyResponse(2, "Need it prepared and separated in advance.", 0));
		rs7.put(3, new SHSurveyResponse(3, "Am unable to dispense my own.", 0));
		final SHSurveyQuestion q7 = new SHSurveyQuestion(7, "When managing medication, I...");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p7 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q7, rs7);
		surveyStructure.put(q7.getQuestionID(), p7);
		
		// Question 8
		LinkedHashMap<Integer, SHSurveyResponse> rs8 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs8.put(1, new SHSurveyResponse(1, "Track my purchases and income independently.", 1));
		rs8.put(2, new SHSurveyResponse(2, "Need help with banking and major purchases.", 1));
		rs8.put(3, new SHSurveyResponse(3, "Am unable to manage my money.", 0));
		final SHSurveyQuestion q8 = new SHSurveyQuestion(8, "When handling finances, I...");
		final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p8 
			= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q8, rs8);
		surveyStructure.put(q8.getQuestionID(), p8);
		
		/////////// BUILD SURVEY ABOVE //////////////////////////////////////////////////////////////
		
		super.initialize(surveyStructure);
	}
}

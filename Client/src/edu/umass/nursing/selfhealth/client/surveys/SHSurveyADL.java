/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

import java.util.LinkedHashMap;

import android.util.Pair;

/**
 *	NOTE: Changes to existing question/responses ordering will invalidate any existing response maps in the database.
 */
public class SHSurveyADL extends SHSurvey {

	public SHSurveyADL() {
		super();
		setup();
	}
	
	private void setup() {
		
		LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>> surveyStructure
		 = new LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>>();
		
		String[] questions = new String[] {
				"Can you drink from a cup and eat from a dish?",
				"Can you dress your upper body?",
				"Can you dress your lower body?",
				"Can you put on your brace or prosthesis?",
				"Can you groom yourself?",
				"Can you bathe yourself?",
				"Can you control your bladder?",
				"Can you control your bowels?",
				"Can you care for your clothing/perineum at toilet?",
				"Can you transfer to a chair?",
				"Can you transfer to the toilet?",
				"Can you transfer to the shower or tub?",
				"Can you walk on level ground 50 yards or more?",
				"Can you walk up a flight of stairs?",
				"Can you move yourself 50 yards in a wheelchair?"
		};
		
		/////////// BUILD SURVEY BELOW //////////////////////////////////////////////////////////////
		
		LinkedHashMap<Integer, SHSurveyResponse> rs1 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs1.put(1, new SHSurveyResponse(1, "Yes", 10));
		rs1.put(2, new SHSurveyResponse(2, "With set up", 5));
		rs1.put(3, new SHSurveyResponse(3, "With someone's help", 1));
		rs1.put(4, new SHSurveyResponse(4, "Cannot", 1));
		rs1.put(5, new SHSurveyResponse(5, "Not Applicable", 0));
		
		for(int i = 0; i<questions.length; i++) {
			int id = i+1;
			final SHSurveyQuestion q = new SHSurveyQuestion(id, questions[i]);
			final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p 
				= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q, rs1);
			surveyStructure.put(q.getQuestionID(), p);
		}
		
		/////////// BUILD SURVEY ABOVE //////////////////////////////////////////////////////////////
		
		super.initialize(surveyStructure);
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

/**
 *	A response and score value for a particular survey question.
 */
public class SHSurveyResponse {

	public static int NO_RESPONSE_ID = -1;
	public static int NO_RESPONSE_VALUE = 0;
	
	private int responseID;
	private String response;
	private int value;
	
	/**
	 * @param response		The string that will be displayed as a response option for a survey question
	 * @param value			The points rewarded for a user selecting this response
	 */
	public SHSurveyResponse(int responseID, String response, int value) {
		this.responseID = responseID;
		this.response = response;
		this.value = value;
	}
	
	public int getResponseID() {
		return this.responseID;
	}
	
	public String response() {
		return this.response;
	}
	
	public int value() {
		return this.value;
	}
	
	
	
}

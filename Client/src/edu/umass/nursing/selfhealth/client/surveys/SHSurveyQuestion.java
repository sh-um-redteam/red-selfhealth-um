/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

/**
 * A question for use in a survey.
 */
public class SHSurveyQuestion {

	private int questionID;
	private String question;
	
	public SHSurveyQuestion(int questionID, String question) {
		this.questionID = questionID;
		this.question = question;
	}
	
	public int getQuestionID() {
		return this.questionID;
	}
	
	public String getQuestion() {
		return this.question;
	}
}

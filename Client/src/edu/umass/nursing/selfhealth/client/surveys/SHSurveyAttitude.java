/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.surveys;

import java.util.LinkedHashMap;

import android.util.Pair;

/**
 *	NOTE: Changes to existing question/responses ordering will invalidate any existing response maps in the database.
 */
public class SHSurveyAttitude extends SHSurvey {

	public SHSurveyAttitude() {
		super();
		setup();
	}
	
	private void setup() {
		
		LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>> surveyStructure
		 = new LinkedHashMap<Integer, Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>>();
		
		String[] questions = new String[] {
				"I usually manage one way or another.",
				"I feel proud that I have accomplished things in life.",
				"I usually take things in stride.",
				"I am friends with myself.",
				"I feel that I can handle many things at a time.",
				"I am determined.",
				"I can get through difficult times because I've experienced difficulty before.",
				"I have self-discipline.",
				"I keep interested in things.",
				"I can usually find something to laugh about.",
				"My belief in myself gets me through hard times.",
				"In an emergency, I'm someone people can generally rely on.",
				"My life has meaning.",
				"When I'm in a difficult situation, I can usually find my way out of it."
		};
		
		/////////// BUILD SURVEY BELOW //////////////////////////////////////////////////////////////
		
		LinkedHashMap<Integer, SHSurveyResponse> rs1 = new LinkedHashMap<Integer, SHSurveyResponse>();
		rs1.put(1, new SHSurveyResponse(1, "Strongly Agree", 7));
		rs1.put(2, new SHSurveyResponse(2, "Agree", 6));
		rs1.put(3, new SHSurveyResponse(3, "Somewhat Agree", 5));
		rs1.put(4, new SHSurveyResponse(4, "Neutral", 4));
		rs1.put(5, new SHSurveyResponse(5, "Somewhat Disagree", 3));
		rs1.put(6, new SHSurveyResponse(6, "Disagree", 2));
		rs1.put(7, new SHSurveyResponse(7, "Strongly Disagree", 1));
		
		for(int i = 0; i<questions.length; i++) {
			int id = i+1;
			final SHSurveyQuestion q = new SHSurveyQuestion(id, questions[i]);
			final Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>> p 
				= new Pair<SHSurveyQuestion, LinkedHashMap<Integer, SHSurveyResponse>>(q, rs1);
			surveyStructure.put(q.getQuestionID(), p);
		}
		
		/////////// BUILD SURVEY ABOVE //////////////////////////////////////////////////////////////
		
		super.initialize(surveyStructure);
	}
}

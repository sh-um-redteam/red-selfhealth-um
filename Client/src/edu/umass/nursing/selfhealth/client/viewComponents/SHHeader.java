/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHHeader extends RelativeLayout {
	
    private TextView headerTitle;
    private TextView headerSubTitle;

	public SHHeader(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_header, this);
        headerTitle 	= (TextView) findViewById(R.id.headerTitle);
        headerSubTitle 	= (TextView) findViewById(R.id.headerSubtitle);
	}
 
    public void setTitle(String title) {
    	headerTitle.setText(title);
    }
 
    public void setSubtitle(String subtitle) {
    	headerSubTitle.setText(subtitle);
    }
}

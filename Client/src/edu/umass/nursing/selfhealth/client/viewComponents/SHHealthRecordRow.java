/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import java.util.ArrayList;

import org.joda.time.LocalDateTime;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHHealthRecordRow extends RelativeLayout {
	
	private int componentCount;
	private Context context;
    private TextView dateTimeCompleted;
    private LinearLayout componentContainer;

	public SHHealthRecordRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		this.context = context;
		
        LayoutInflater.from(context).inflate(R.layout.sh_row_health_metric_record, this);
        dateTimeCompleted 	= (TextView) findViewById(R.id.healthRecordDateTime);
        componentContainer	= (LinearLayout) findViewById(R.id.healthRecordComponentContainer);
	}
 
    public void setDateTimeCompleted(LocalDateTime dateTimeCompleted) {
    	this.dateTimeCompleted.setText(dateTimeCompleted.toString("MMMM d - h:mm a"));
    }
    
    public void updateComponents(ArrayList<String> components, ArrayList<String> units, ArrayList<Number> measurements) {
    	
    	if(components.size() < componentCount) {
    		for(int i = componentCount-1; i == components.size(); i--) {
    			if(componentCount > 1) {
    				componentContainer.removeViewAt(i-1);
            	}
    			componentContainer.removeViewAt(i);
    			componentCount--;
    		}
    	} else if(components.size() > componentCount) {
    		for(int i = componentCount; i < components.size(); i++) {
    			if(componentCount > 0) {
            		View divider = new View(context);
            		divider.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,(int) getResources().getDimension(R.dimen.thickness_list_divider)));
            		divider.setBackgroundColor(getResources().getColor(R.color.list_divider));
            		componentContainer.addView(divider);
            	}
    			componentContainer.addView(new SHHealthRecordComponentRow(context, null));
    			componentCount++;
    		}
    	}
    	int componentIndex;
    	for(int i = 0; i < componentCount; i++) {
    		
    		if(i > 0)
    			componentIndex = i+2;
    		else
    			componentIndex = i+1;
    			
    			
    		SHHealthRecordComponentRow componentRow = (SHHealthRecordComponentRow) componentContainer.getChildAt(componentIndex);
    		componentRow.setComponent(components.get(i));
    		componentRow.setUnit(units.get(i));
    		componentRow.setMeasurement(measurements.get(i));
    	}
    }
}

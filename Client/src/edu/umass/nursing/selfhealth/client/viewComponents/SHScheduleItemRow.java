/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import org.joda.time.LocalTime;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHScheduleItemRow extends RelativeLayout {
	
	private ImageView scheduleItemIcon;
    private TextView scheduleItemDescription;
    private TextView scheduleItemTime;

	public SHScheduleItemRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_row_schedule_item, this);
        scheduleItemIcon 		= (ImageView) findViewById(R.id.scheduleItemIcon);
        scheduleItemDescription = (TextView) findViewById(R.id.scheduleItemDescription);
        scheduleItemTime 		= (TextView) findViewById(R.id.scheduleItemTime);
	}
 
    public void setIcon(int resourceID) {
    	scheduleItemIcon.setImageResource(resourceID);
    }
 
    public void setDescription(String description) {
    	scheduleItemDescription.setText(description);
    }
 
    public void setTime(LocalTime time) {
    	scheduleItemTime.setText(time.toString("h:mm a"));
    }
}

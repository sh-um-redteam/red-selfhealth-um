/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHSurveyResponseRow extends RelativeLayout {
	
    private TextView response;

	public SHSurveyResponseRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_row_survey_response, this);
        response = (TextView) findViewById(R.id.surveyResponse);
	}
 
    public void setResponse(String response) {
    	this.response.setText(response);
    }
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHMenuRow extends RelativeLayout {
	
	private ImageView menuItemIcon;
    private TextView menuItemDescription;

	public SHMenuRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_row_menu_item, this);
        menuItemIcon 		= (ImageView) findViewById(R.id.menuItemIcon);
        menuItemDescription = (TextView) findViewById(R.id.menuItemDescription);
        
        menuItemIcon.setVisibility(View.GONE);
	}
 
    public void setIcon(int resourceID) {
    	menuItemIcon.setImageResource(resourceID);
    	menuItemIcon.setVisibility(View.VISIBLE);
    }
 
    public void setDescription(String description) {
    	menuItemDescription.setText(description);
    }
}

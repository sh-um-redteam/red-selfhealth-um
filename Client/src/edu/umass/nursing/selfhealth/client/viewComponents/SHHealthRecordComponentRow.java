/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;
/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/

public class SHHealthRecordComponentRow extends RelativeLayout {
	
    private TextView component;
    private TextView unit;
    private TextView measurement;

	public SHHealthRecordComponentRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_row_health_metric_record_component, this);
        component			= (TextView) findViewById(R.id.healthComponent);
        unit 				= (TextView) findViewById(R.id.healthComponentUnit);
        measurement 		= (TextView) findViewById(R.id.healthComponentMeasurement);
	}
 
    public void setComponent(String component) {
    	if(component == null)
    		this.component.setText("");
    	else
    		this.component.setText(component);
    }
    
    public void setUnit(String unit) {
    	if(unit == null)
    		this.unit.setText("");
    	else 
    		this.unit.setText(unit);
    }
    
    public void setMeasurement(Number measurement) {
    	if(measurement == null)
    		this.measurement.setText("");
    	else
    		this.measurement.setText(measurement.toString());
    }
}

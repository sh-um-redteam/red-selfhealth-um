/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHConfirmableRecordRow extends RelativeLayout {
	
    private TextView dateCompleted;
    private TextView dateDetail;
    private TextView timeCompleted;

	public SHConfirmableRecordRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_row_medication_record, this);
        dateCompleted	= (TextView) findViewById(R.id.medicationRecordDate);
        dateDetail 		= (TextView) findViewById(R.id.medicationRecordDetail);
        timeCompleted 	= (TextView) findViewById(R.id.medicationRecordTime);
	}
 
    public void setDateTimeCompleted(LocalDateTime dateTimeCompleted) {
    	
    	this.dateCompleted.setText(dateTimeCompleted.toString("MMMM d"));
    	setDateDetail(dateTimeCompleted);
    	setTimeCompleted(dateTimeCompleted.toLocalTime());
    }
 
    /**
     * Lifted from @author Lea Verou on the web: http://lea.verou.me/2009/04/java-pretty-dates/
     */
    private void setDateDetail(LocalDateTime dateTime) {
    	
		long current = (new LocalDateTime().toDate()).getTime(),
			timestamp = dateTime.toDate().getTime(),
			diff = (current - timestamp)/1000;
		int	amount = 0;
		String	what = "";

		/**
		 * Second counts
		 * 3600: hour
		 * 86400: day
		 * 604800: week
		 * 2592000: month
		 * 31536000: year
		 */

		if(diff > 31536000) {
			amount = (int)(diff/31536000);
			what = "year";
		}
		else if(diff > 31536000) {
			amount = (int)(diff/31536000);
			what = "month";
		}
		else if(diff > 604800) {
			amount = (int)(diff/604800);
			what = "week";
		}
		else if(diff > 86400) {
			amount = (int)(diff/86400);
			what = "day";
		}
		else if(diff > 3600) {
			amount = (int)(diff/3600);
			what = "hour";
		}
		else if(diff > 60) {
			amount = (int)(diff/60);
			what = "minute";
		}
		else {
			amount = (int)diff;
			what = "second";
			if(amount < 6) {
				dateDetail.setText("Just now");
				return;
			}
		}

		if(amount == 1) {
			if(what.equals("day")) {
				dateDetail.setText("Yesterday");
				return;
			}
			else if(what.equals("week") || what.equals("month") || what.equals("year")) {
				dateDetail.setText("Last " + what);
				return;
			}
		}
		else {
			what += "s";
		}

		dateDetail.setText(amount + " " + what + " ago");
		return;
	}
    
    private void setTimeCompleted(LocalTime time) {
    	this.timeCompleted.setText(time.toString("h:mm a"));
    }
}

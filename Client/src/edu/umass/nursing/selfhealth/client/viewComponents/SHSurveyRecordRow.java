/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHSurveyRecordRow extends RelativeLayout {
	
    private TextView dateCompleted;
    private TextView dateDetail;
    private TextView score;

	public SHSurveyRecordRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_row_survey_record, this);
        dateCompleted	= (TextView) findViewById(R.id.surveyRecordDate);
        dateDetail 		= (TextView) findViewById(R.id.surveyRecordDetail);
        score 			= (TextView) findViewById(R.id.surveyRecordScore);
	}
 
    public void setDateTimeCompleted(LocalDateTime dateTimeCompleted) {
    	
    	this.dateCompleted.setText(dateTimeCompleted.toString("MMMM d"));
    	setDateDetail(dateTimeCompleted.toLocalTime());
    }
 
    private void setDateDetail(LocalTime time) {
    	this.dateDetail.setText(time.toString("h:mm a"));
    }
    
    public void setScore(int score) {
    	this.score.setText(Integer.toString(score));
    }
}

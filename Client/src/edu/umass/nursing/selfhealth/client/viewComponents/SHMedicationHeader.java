/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHMedicationHeader extends RelativeLayout {
	
    private TextView medicationName;
    private TextView medicationDosage;

	public SHMedicationHeader(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_header_medication, this);
        medicationName 	= (TextView) findViewById(R.id.medicationHeaderName);
        medicationDosage= (TextView) findViewById(R.id.medicationHeaderDosage);
	}
 
    public void setMedicationName(String name) {
    	medicationName.setText(name);
    }
    
    public void setMedicationDosage(String dosage) {
    	medicationDosage.setText(dosage);
    }
}

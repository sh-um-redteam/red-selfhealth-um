/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import edu.umass.nursing.selfhealth.client.R;

public class SHMeasurementPane extends RelativeLayout {
	
	private EditText measurementEditText;
    private TextView componentTitle;
    private TextView unitSubtitle;

	public SHMeasurementPane(final Context context, AttributeSet attrs, boolean landscape) {
		super(context, attrs);
		
		if(landscape) {
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1);  
	        super.setLayoutParams(layoutParams);   
		}
		
        LayoutInflater.from(context).inflate(R.layout.sh_measurement_pane, this);
        measurementEditText	= (EditText) findViewById(R.id.measurementEdit);
        componentTitle 		= (TextView) findViewById(R.id.componentText);
        unitSubtitle		= (TextView) findViewById(R.id.unitText);
        
        // When pressing the 'Done' button on the keypad, remove focus from the editText and hide the keypad
        measurementEditText.setOnEditorActionListener(new OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event)
            {
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                	view.clearFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return true;
                }
    			return false;
            }
        });
	}
 
    public void setComponent(String component) {
    	componentTitle.setText(component);
    }
    
    public void setUnit(String unit) {
    	unitSubtitle.setText(unit);
    }
    
    public String getMeasurement() {
    	return measurementEditText.getText().toString();
    }
    
    public void restoreMeasurement(String value) {
    	measurementEditText.setText(value);
    }
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import edu.umass.nursing.selfhealth.client.R;

public class SHSchedulePlaceholder extends RelativeLayout {

	public SHSchedulePlaceholder(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_placeholder_schedule, this);
	}
}

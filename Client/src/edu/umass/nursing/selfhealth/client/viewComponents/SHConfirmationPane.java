/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHConfirmationPane extends RelativeLayout {
	
	private Button confirmationButton;
	private TextView confirmationHeader;
    private TextView confirmationTitle;
    private TextView confirmationSubtitle;

	public SHConfirmationPane(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_confirmation_pane, this);
        confirmationHeader		= (TextView) findViewById(R.id.confirmationPrompt);
        confirmationTitle 		= (TextView) findViewById(R.id.confirmationTitle);
        confirmationSubtitle	= (TextView) findViewById(R.id.confirmationSubtitle);
        confirmationButton		= (Button) findViewById(R.id.confirmationButton);
	}
 
    public void setHeaderPrompt(String prompt) {
    	confirmationHeader.setText(prompt);
    }
    
    public void setTitle(String title) {
    	confirmationTitle.setText(title);
    }
    
    public void setSubtitle(String subtitle) {
    	confirmationSubtitle.setText(subtitle);
    }
    
    @Override
    public void setOnClickListener(OnClickListener listener) {
    	confirmationButton.setOnClickListener(listener);
    }
}

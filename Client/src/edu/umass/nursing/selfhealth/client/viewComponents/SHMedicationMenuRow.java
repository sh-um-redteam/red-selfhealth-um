/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.viewComponents;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;

public class SHMedicationMenuRow extends RelativeLayout {
	
	private ImageView medicationIcon;
    private TextView medicationName;
    private TextView medicationDosage;

	public SHMedicationMenuRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		
        LayoutInflater.from(context).inflate(R.layout.sh_row_medication_menu_item, this);
        medicationIcon	= (ImageView) findViewById(R.id.medicationMenuIcon);
        medicationName 	= (TextView) findViewById(R.id.medicationMenuName);
        medicationDosage= (TextView) findViewById(R.id.medicationMenuDosage);
        
        medicationIcon.setVisibility(View.GONE);
	}
 
    public void setIcon(int resourceID) {
    	medicationIcon.setImageResource(resourceID);
    	medicationIcon.setVisibility(View.VISIBLE);
    }
 
    public void setName(String name) {
    	medicationName.setText(name);
    }
    
    public void setDosage(String dosage) {
    	medicationDosage.setText(dosage);
    }
}

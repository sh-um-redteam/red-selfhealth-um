/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.tools;

import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;

/**
 *	A Health Metric tool representation
 */
@SuppressWarnings("serial")
public abstract class SHHealthMetricTool extends SHSelfManagementTool {

	private SHHealthMetricID id;
	
	public SHHealthMetricTool(SHHealthMetricID healthID) {
		super(healthID.toString(), null);
		this.id = healthID;
	}

	@Override
	public int getIcon() {
		return id.icon();
	}
	
	public SHHealthMetricID getID() {
		return id;
	}
	
	@Override
	public String toString() {
		return id.toString();
	}
}

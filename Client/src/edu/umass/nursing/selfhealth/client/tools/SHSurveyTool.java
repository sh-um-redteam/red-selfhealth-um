/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.tools;

import android.content.Context;
import android.content.Intent;

import edu.umass.nursing.selfhealth.client.activities.SHSurveyActivity;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyID;

/**
 *	A survey tool
 */
@SuppressWarnings("serial")
public class SHSurveyTool extends SHSelfManagementTool {

	private SHSurveyID id;
	
	public SHSurveyTool(SHSurveyID surveyID) {
		super(surveyID.toTitleString(), null);
		this.id = surveyID;
	}

	@Override
	public int getIcon() {
		return id.icon();
	}
	
	public SHSurveyID getID() {
		return id;
	}
	
	@Override
    public Intent getIntent(Context c) {
		
		Intent intent = new Intent(c, SHSurveyActivity.class);
		intent.putExtra(SHConstants.SURVEY_ID, id);
		return intent;
	}
	
	@Override
	public String toString() {
		return id.toTitleString();
	}
}

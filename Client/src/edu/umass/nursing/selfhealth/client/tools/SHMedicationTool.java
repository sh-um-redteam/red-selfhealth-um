/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.tools;

import android.content.Context;
import android.content.Intent;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.activities.SHConfirmationActivity;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.medication.SHMedicationID;

/**
 *	A Medication tool that contains the appropriate medication
 */
@SuppressWarnings("serial")
public class SHMedicationTool extends SHSelfManagementTool {

	private SHMedication medication;
	
	public SHMedicationTool(SHMedication medication) {
		super(medication.getName() + " " + medication.getDetails(), null);
		this.medication = medication;
	}

	@Override
	public int getIcon() {
		return R.drawable.sh_medication;
	}
	
	public SHMedicationID getID() {
		return medication.getID();
	}
	
	@Override
    public Intent getIntent(Context c) {
		
		Intent intent = new Intent(c, SHConfirmationActivity.class);
		intent.putExtra(SHConstants.MEDICATION, medication);
		return intent;
	}
	
	@Override
	public String toString() {
		return medication.toString();
	}
}

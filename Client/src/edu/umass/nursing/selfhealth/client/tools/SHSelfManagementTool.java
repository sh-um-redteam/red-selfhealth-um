/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.tools;

import java.io.Serializable;

import android.content.Context;
import android.content.Intent;

/**
 *	Abstract representation of a self-management tool. 
 *	Provides access to resources such as tool name, description, icon.
 *	Intents for interacting with the tool must be implemented in subclasses. 
 */
@SuppressWarnings("serial")
public abstract class SHSelfManagementTool implements Serializable {

	private String toolName;
	private String toolDescription;
	
	public SHSelfManagementTool(String name, String description) {
		this.toolName = name;
		this.toolDescription = description;
	}
	
	public String getName() {
		return toolName;
	}
	
	public String getDescription() {
		return toolDescription;
	}
	
	public abstract int getIcon();
	
	public abstract Intent getIntent(Context c);
	
	@Override
	public abstract String toString();
}

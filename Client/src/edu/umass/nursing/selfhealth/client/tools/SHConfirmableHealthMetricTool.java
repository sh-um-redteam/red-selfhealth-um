/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.tools;

import android.content.Context;
import android.content.Intent;

import edu.umass.nursing.selfhealth.client.activities.SHConfirmationActivity;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;

/**
 *	A Health Metric tool that requires confirmation
 */
@SuppressWarnings("serial")
public class SHConfirmableHealthMetricTool extends SHHealthMetricTool {

	public SHConfirmableHealthMetricTool(SHHealthMetricID healthID) {
		super(healthID);
	}
	
	@Override
	public Intent getIntent(Context c) {
		
		Intent intent = new Intent(c, SHConfirmationActivity.class);
		intent.putExtra(SHConstants.HEALTH_METRIC_ID, this.getID());
		return intent;
	}
}

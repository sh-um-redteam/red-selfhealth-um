/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.tools;

import org.json.JSONObject;

import edu.umass.nursing.selfhealth.client.configuration.SHConfiguration;
import edu.umass.nursing.selfhealth.client.configuration.SHMedicationConfiguration;

/**
 *	Defines the Tool Categories available to the application
 *	These are used as keys for generating the appropriate {@link SHConfiguration}
 */
public enum SHSelfManagementToolCategory {

	HEALTH_METRIC ("HEALTH_METRIC"),
	MEDICATION ("MEDICATION"),
	SURVEY ("SURVEY"),
	GOAL ("GOAL");
	
	private String toolCateogry; 
	
    private SHSelfManagementToolCategory(String toolCateogry) { 
        this.toolCateogry = toolCateogry; 
    }
	
	public static SHSelfManagementToolCategory fromString(String toolCategory) {
		if(toolCategory.equals(HEALTH_METRIC.toString())) {
			return HEALTH_METRIC;
		} else if(toolCategory.equals(MEDICATION.toString())) {
			return MEDICATION;
		} else if(toolCategory.equals(SURVEY.toString())) {
			return SURVEY;
		} else if(toolCategory.equals(GOAL.toString())) {
			return GOAL;
		} else {
			return null;
		}
	}
	
	public static SHConfiguration generateConfiguration(SHSelfManagementToolCategory category, JSONObject configurationJSON) {
		switch(category) {
			case HEALTH_METRIC:
				return SHConfiguration.fromJSON(configurationJSON);
			case MEDICATION:
				return SHMedicationConfiguration.fromJSON(configurationJSON);
			case SURVEY:
				return SHConfiguration.fromJSON(configurationJSON);
			case GOAL:
				return null;
			default:
				return null;
		}
	}
	
	@Override
	public String toString() {
		return toolCateogry;
	}
}

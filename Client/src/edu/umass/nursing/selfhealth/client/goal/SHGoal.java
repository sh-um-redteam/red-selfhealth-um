/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.goal;

// TODO : This feature is not yet available.  Maybe it can be useful down the road.

/**
 *	Represents a goal that the user may work toward completing.
 *	Goals may be spawned by the application to try and help improve
 *	the user's self management techniques, or the user may set their own goals.
 */
public class SHGoal {

	private String goal;
	private int daysToComplete;

	/**
	 * @param goal				The goal the user should work toward completing
	 * @param daysToComplete	The number of days until the goal is considered unmet
	 */
	public SHGoal(String goal, int daysToComplete) {
		this.goal = goal;
		this.daysToComplete = daysToComplete;
	}

	public String getGoal() {
		return goal;
	}

	public int getDaysToComplete() {
		return daysToComplete;
	}
}

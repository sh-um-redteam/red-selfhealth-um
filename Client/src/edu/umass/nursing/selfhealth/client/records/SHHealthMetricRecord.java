/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.records;

import org.joda.time.LocalDateTime;

import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;

/**
 *	A record that contains health metric completion data.
 */
@SuppressWarnings("serial")
public abstract class SHHealthMetricRecord extends SHRecord {

	private SHHealthMetricID healthMetricID;
	
	public SHHealthMetricRecord(
			SHHealthMetricID healthMetricID,
			LocalDateTime dateTimeCompleted) {
		super(dateTimeCompleted);
		
		this.healthMetricID = healthMetricID;
	}
	
	public boolean isConfirmable() {
		return healthMetricID.isConfirmable();
	}
	
	/**
	 * 	Used by the controller for saving the record to the database.
	 */
	public SHHealthMetricID healthMetricID() {
		return healthMetricID;
	}

	@Override
	public String toString() {
		return healthMetricID.toString();
	}
}

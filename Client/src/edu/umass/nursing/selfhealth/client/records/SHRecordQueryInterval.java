/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.records;

import org.joda.time.LocalDateTime;

/**
 *	Represents an interval of time for which records should be retrieved 
 *	TODO : 	There is currently no reason to retrieve a time interval, but this may be
 *			useful when graphs are present and the patient cares to see a window of time.
 */
public class SHRecordQueryInterval {

	private LocalDateTime startDate;
	private LocalDateTime endDate;
	
	/**
	 * @param startDate	The earliest date for which the records should be retrieved.
	 * 					If null, records are retrieved since the earliest recording.
	 * 
	 * @param endDate	The latest date for which the records should be retrieved.
	 * 					If null, records are retrieved up until today.
	 */
	public SHRecordQueryInterval(LocalDateTime startDate, LocalDateTime endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}
	
	/**
	 * Creates a time interval string for the WHERE clause in a database SELECT statement
	 */
	public static String toSQLiteDateIntervalClause(SHRecordQueryInterval interval, String columnName) {
		
		if(interval == null) {
			return null;
		}
		
		String startClause = "";
		String endClause = "";
		
		LocalDateTime start = interval.getStartDate();
		LocalDateTime end 	= interval.getEndDate();
		
		if(start == null && end == null) {
			return null;
		}
		
		if(start != null) {
			startClause	+= 	
					columnName + 
					" >= " + 
					"datetime('" + 
					interval.getStartDate().toString("YYYY-MM-dd hh:mm:ss") + 
					"')"; 
		}
		
		if(end != null) {
			endClause += 	
					columnName + 
					" <= " + 
					"datetime('" + 
					interval.getStartDate().toString("YYYY-MM-dd hh:mm:ss") + 
					"')"; 
		}
		
		if(startClause != "" && endClause != "") {
			return startClause + " AND " + endClause;
		} else {
			return startClause + endClause;
		}
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.records;

import java.util.HashMap;

import org.joda.time.LocalDateTime;

import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetric;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricComponent;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;

/**
 *	A record that contains measurement data for an associated health metric.
 */
@SuppressWarnings("serial")
public class SHMeasurableHealthMetricRecord extends SHHealthMetricRecord {

	private static final String VALUE_SEPARATOR = ":";
	private static final String COMPONENT_SEPARATOR = ";";
	
	private HashMap<SHHealthMetricComponent, Number> measurements;
	
	public SHMeasurableHealthMetricRecord(
			SHHealthMetricID healthMetricID,
			LocalDateTime dateTimeCompleted, 
			HashMap<SHHealthMetricComponent, Number> measurements) {
		super(healthMetricID, dateTimeCompleted);
		
		if(measurements == null) {
			this.measurements = new HashMap<SHHealthMetricComponent, Number>();
		} else {
			this.measurements = measurements;
		}
	}
	
	/**
	 * 	This should be used prior to getting or setting component values.
	 *	By calling this first, the components can be used as keys for 
	 *	mapping measured values.
	 */
	public SHHealthMetric healthMetric() {
		return SHHealthMetricID.generateHealthMetric(this.healthMetricID());
	}
	
	public void setComponentValue(SHHealthMetricComponent component, Number value) {
		this.measurements.put(component, value);
	}
	
	/**
	 * @param component	Component keys can be accessed from the healthMetric.
	 */
	public Number componentValue(SHHealthMetricComponent component) {
		return this.measurements.get(component);
	}
	
	/**
	 * Generates a string for keeping the measurements together in a single database entry
	 * TODO : 	Perhaps normalize? Difficult to do because you don't know exactly what metrics will be needed.
	 * 			This way is much more flexible, but more attention needs to be paid when modifying metrics
	 */
	public static String toSQLiteMeasurementMapString(SHMeasurableHealthMetricRecord record) {
		
		String map = new String();
		
		SHHealthMetric metric = record.healthMetric();
		for(SHHealthMetricComponent component : metric.components()) {
			map += component.id() + VALUE_SEPARATOR + record.componentValue(component).doubleValue() + COMPONENT_SEPARATOR; 
		}
				
		return map;
	}
	
	/**
	 * Restore the measurement map that was inserted into the database as a single string
	 */
	public static HashMap<SHHealthMetricComponent, Number> measurementMapFromSQLiteString(String mapString) {
		
		HashMap<SHHealthMetricComponent, Number> map = new HashMap<SHHealthMetricComponent, Number>();
		
		for(String component : mapString.split(COMPONENT_SEPARATOR)) {
			String[] pair = component.split(VALUE_SEPARATOR);
			map.put(SHHealthMetricComponent.fromID(Integer.parseInt(pair[0])), Double.parseDouble(pair[1]));
		}
		
		return map;
	}
}

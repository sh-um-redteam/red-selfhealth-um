/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.records;

import java.io.Serializable;

import org.joda.time.LocalDateTime;

/**
 *	The framework for records that contain relevant information 
 *	that the user provides as input.  For example, a user can record
 *	taking medications, completing surveys, measuring health metrics, etc.
 */
@SuppressWarnings("serial")
public abstract class SHRecord implements Comparable<SHRecord>, Serializable {

	private LocalDateTime dateTimeCompleted;
	
	public SHRecord(LocalDateTime dateTimeCompleted) {
		this.dateTimeCompleted = dateTimeCompleted;
	}

	public LocalDateTime getTimeCompleted() {
		return dateTimeCompleted;
	}

	public boolean isComplete() {
		return (dateTimeCompleted != null);
	}
	
	public void setCompleted() {
		if(dateTimeCompleted == null) {
			this.dateTimeCompleted = LocalDateTime.now();
		}
	}
	
	@Override
	public int compareTo(SHRecord otherRecord) {
       return this.dateTimeCompleted.compareTo(otherRecord.getTimeCompleted());
    } 

	@Override
	public abstract String toString();
}

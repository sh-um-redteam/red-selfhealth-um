/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.records;

import org.joda.time.LocalDateTime;

import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;

/**
 *	A record that contains health metric confirmation data.
 */
@SuppressWarnings("serial")
public class SHConfirmableHealthMetricRecord extends SHHealthMetricRecord {

	public SHConfirmableHealthMetricRecord(
			SHHealthMetricID healthMetricID,
			LocalDateTime dateTimeCompleted) {
		super(healthMetricID, dateTimeCompleted);
	}
}
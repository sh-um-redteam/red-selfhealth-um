/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.records;

import org.joda.time.LocalDateTime;

import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.medication.SHMedicationID;

/**
 *	A record with the time that a medication was taken and information about the medication.
 */
@SuppressWarnings("serial")
public class SHMedicationRecord extends SHRecord {

	private SHMedicationID medicationID;
	
	public SHMedicationRecord(SHMedicationID medicationID, LocalDateTime dateTimeCompleted) {
		super(dateTimeCompleted);
		this.medicationID = medicationID;
	}
	
	public SHMedicationID medicationID() {
		return this.medicationID;
	}
	
	public SHMedication medication() {
		return SHMedicationID.medication(medicationID);
	}
	
	@Override
	public String toString() {
		return medication().toString();
	}
}

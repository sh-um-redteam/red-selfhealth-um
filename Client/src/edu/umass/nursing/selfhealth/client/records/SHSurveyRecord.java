/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.records;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.joda.time.LocalDateTime;

import android.annotation.SuppressLint;
import android.util.Pair;

import edu.umass.nursing.selfhealth.client.surveys.SHSurvey;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyID;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyQuestion;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyResponse;

/**
 *	A record that contains responses and scoring for a survey.
 */
@SuppressWarnings("serial")
public class SHSurveyRecord extends SHRecord {
	
	private static final String RESPONSE_SEPARATOR = ":";
	private static final String QUESTION_SEPARATOR = ";";
	
	private SHSurveyID surveyID;
	private HashMap<Integer, Integer> questionResponseMap;
	
	public SHSurveyRecord(
			SHSurveyID surveyID, 
			HashMap<Integer, Integer> questionResponseMap, 
			LocalDateTime dateTimeCompleted) {
		
		super(dateTimeCompleted);
		this.surveyID = surveyID;
		this.questionResponseMap = questionResponseMap;
	}

	public SHSurveyID surveyID() {
		return surveyID;
	}
	
	public SHSurvey survey() {
		return SHSurveyID.generateSurvey(surveyID);
	}
	
	/**
	 * 	Maps question numbers to response indices selected by the Patient
	 */
	public HashMap<Integer, Integer> getQuestionResponseMap() {
		return questionResponseMap;
	}
	
	/**
	 * @return	A list of each question and associated response that the user selected
	 */
	public ArrayList<Pair<SHSurveyQuestion, SHSurveyResponse>> getQuestionResponsePairs() {
		
		ArrayList<Pair<SHSurveyQuestion, SHSurveyResponse>> pairs 
			= new ArrayList<Pair<SHSurveyQuestion, SHSurveyResponse>>();
		
		for(Entry<Integer, Integer> pair : questionResponseMap.entrySet()) {
			pairs.add(survey().getQuestionAndResponse(pair.getKey(), pair.getValue()));
		}
		return pairs;
	}
	
	public int getScore() {
		
		int score = 0;
		for(Entry<Integer, Integer> pair : questionResponseMap.entrySet()) {
			score += survey().getValueForResponse(pair.getKey(), pair.getValue());
		}
		return score;
	}
	
	/**
	 * Generates a string for keeping the responses together in a single database entry
	 */
	public static String toSQLiteResponseMapString(SHSurveyRecord record) {
		
		String map = new String();
		
		for(Entry<Integer, Integer> pair : record.getQuestionResponseMap().entrySet()) {
			map += pair.getKey() + RESPONSE_SEPARATOR + pair.getValue() + QUESTION_SEPARATOR;
		}
				
		return map;
	}
	
	/**
	 * Restore the response map that was inserted into the database as a single string
	 */
	@SuppressLint("UseSparseArrays")
	public static HashMap<Integer, Integer> responseMapFromSQLiteString(String mapString) {
		
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		for(String component : mapString.split(QUESTION_SEPARATOR)) {
			String[] pair = component.split(RESPONSE_SEPARATOR);
			map.put(Integer.parseInt(pair[0]), Integer.parseInt(pair[1]));
		}
		
		return map;
	}

	@Override
	public String toString() {
		return surveyID.toString();
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.records;

import org.joda.time.Days;
import org.joda.time.LocalDateTime;

import edu.umass.nursing.selfhealth.client.goal.SHGoal;

/**
 *	A record containing goal information
 *  TODO : This type of record is not yet implemented. May be useful later on.
 */
@SuppressWarnings("serial")
public class SHGoalRecord extends SHRecord {

	private int goalID;
	private SHGoal goal;
	private LocalDateTime goalStartDate;
	
	public SHGoalRecord(int goalID, LocalDateTime goalStartDate, LocalDateTime dateTimeCompleted, SHGoal goal) {
		super(dateTimeCompleted);
		this.goalStartDate = goalStartDate;
		this.goal = goal;
	}
	
	public int getGoalID() {
		return goalID;
	}
	
	public SHGoal getGoal() {
		return goal;
	}
	
	public LocalDateTime getGoalStartDate() {
		return goalStartDate;
	}
	
	public int daysActive() {
		if(isComplete()) {
			return Days.daysBetween(goalStartDate, this.getTimeCompleted()).getDays();
		} else {
			return Days.daysBetween(goalStartDate, LocalDateTime.now()).getDays();
		}
	}

	@Override
	public String toString() {
		return "GOOOOOOOAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLLL!!!!!! -Stephen Colbert, 2014";
	}
}

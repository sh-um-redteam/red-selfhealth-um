/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.schedule;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;

/**
 * Displays a notification (started from SHReminderBroadcastReceiver)
 */
public class SHReminderService extends IntentService {
		
	private static int notificationID = 0;
	
	private static int id() {
		return notificationID++;
	}
	
	public SHReminderService(){
		super("SHReminderService");
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
	
	@Override
	protected void onHandleIntent(Intent intent){
		
		int reminderId = intent.getIntExtra(SHConstants.REMINDER_ID, -1);
		int notificationId = id();
		
		// Update the reminder so that the notification will be cancelled when the activity is run from the schedule list.
		try {
		    SHSchedule.getInstance(getApplicationContext()).getScheduleItem(reminderId).getReminder().setNotificationID(notificationId);
		} catch (java.lang.NullPointerException e) {
		    return;
		}
		
		String message = intent.getStringExtra(SHConstants.REMINDER_MESSAGE);
		
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		
		Intent notificationIntent = new Intent(this, SHReminderActivity.class);
		notificationIntent.putExtra(SHConstants.REMINDER_ID, reminderId);
		notificationIntent.putExtra(SHConstants.NOTIFICATION_ID, notificationId);
		notificationIntent.putExtra(SHConstants.REMINDER_MESSAGE, message);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		
		PendingIntent pendingIntent = PendingIntent.getActivity(this, reminderId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
		style.setBigContentTitle(message);
		style.bigText("Reminder");
		
		Notification notification = new NotificationCompat.Builder(this)
		
			.setSmallIcon(R.drawable.ic_stat_notification)
			.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.note_big))
			.setAutoCancel(true)
			// At some point, use setFullScreenIntent(PendingIntent intent, boolean highPriority)
	        // to get a true pop up. This currently only launches a single intent, no matter how many nofications there are.
			// needs more thought to turn it into something similar to the schedule view to enable handling all items for a given
			// time
			//.setFullScreenIntent(pi, true)
			.setContentIntent(pendingIntent)
			.setTicker(message)
			.setWhen(0)
			.setContentTitle("Reminder")
			.setContentText(message)
			.addAction(R.drawable.ic_stat_snooze, "Snooze", pendingIntent)
			.setStyle(style)
			.build();
		
		notification.defaults |= Notification.DEFAULT_SOUND;
		
		notificationManager.notify(notificationId, notification);
	}
}

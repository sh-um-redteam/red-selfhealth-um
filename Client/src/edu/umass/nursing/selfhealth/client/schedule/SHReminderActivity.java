/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.schedule;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;

// TODO : Save Data for screen on/off/rotations

public class SHReminderActivity extends Activity {

    private Bundle extras;
    private int reminderID;
    private int notificationID;
    private String reminderMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sh_activity_reminder_popup);
        
        extras = getIntent().getExtras();
        
        reminderID 		= extras.getInt(SHConstants.REMINDER_ID);
        notificationID 	= extras.getInt(SHConstants.NOTIFICATION_ID);
        reminderMessage = extras.getString(SHConstants.REMINDER_MESSAGE);

        TextView tv = (TextView) this.findViewById(R.id.reminderTitle);
        tv.setText(reminderMessage);
    }

    public void onConfirmClick(View v) {
        
    	Intent intent = SHSchedule.getInstance(getBaseContext()).getScheduleItem(reminderID).getIntent(this);
        startActivityForResult(intent, reminderID);
    }

    public void onSnoozeClick(View v){
    	
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationID);		
        Toast.makeText(this, "Snoozed " + reminderMessage, Toast.LENGTH_SHORT).show();
        SHSchedule.getInstance(this).snoozeReminder(reminderID);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
        	SHSchedule.getInstance(this).completeScheduleItem(reminderID, data.getIntExtra((SHConstants.COMPLETION_ID), -1));
        }
        finish();
    }
    
    /* prevents dialog from going away when user touches screen without touching a button */
    @Override
    public boolean onTouchEvent(MotionEvent event){
        return false;
    }
}

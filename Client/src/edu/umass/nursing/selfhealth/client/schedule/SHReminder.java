/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.schedule;

import org.joda.time.LocalTime;

import edu.umass.nursing.selfhealth.client.application.SHConstants;

public class SHReminder {
	
	/**
	 * Key to a scheduleItem in {@link SHSchedule}
	 */
	private int reminderID;
	private int notificationID;
	
	private LocalTime scheduledTime;
	private String message;

	public SHReminder(int scheduleIDKey, LocalTime scheduledTime, String message) {
		
		this.reminderID = scheduleIDKey;
		this.scheduledTime = scheduledTime;
		this.message = message;
		notificationID = SHConstants.DEFAULT_NOTIFICATION_ID;
	}
	
	public void setNotificationID(int notificationID) {
		this.notificationID = notificationID;
	}
	
	public int getNotificationID() 		{ return notificationID; }
	public int getReminderID() 			{ return reminderID; }
	public int getScheduleID() 			{ return reminderID; }
	
	public String getMessage() 			{ return message; }

	public LocalTime getReminderTime() 	{ return scheduledTime; }
}

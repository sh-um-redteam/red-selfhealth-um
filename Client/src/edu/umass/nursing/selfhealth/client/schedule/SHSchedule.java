/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.schedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.joda.time.LocalDateTime;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.controller.SHController;

public class SHSchedule {

    private static final String TAG 	= "SHSchedule";
    
    private SHReminderManager reminderManager;
    private Context context;
    
    private Hashtable<Integer, SHScheduleItem> schedule;

    private static SHSchedule instance = null;
    
    private OnScheduleUpdatedListener scheduleUpdatedListener;
    
    public interface OnScheduleUpdatedListener {
    	public void onScheduleUpdated();
    }
    public void registerOnScheduleUpdatedListener(OnScheduleUpdatedListener listener) {
    	this.scheduleUpdatedListener = listener;
    }

    public static SHSchedule getInstance(Context c){
        if (instance == null)
            instance = new SHSchedule(c);
        return instance;
    }

    private SHSchedule(Context c) {
        
        Log.i(TAG, "Making schedule...");
        
        this.context = c;
        this.reminderManager = SHReminderManager.getInstance(c);
        schedule = new Hashtable<Integer, SHScheduleItem>();
        performDailyUpdate();
    }
    
    public void performDailyUpdate() {
		
    	Log.v(TAG, "Performing daily Schedule update...");
    	
    	// Cancel Reminders
    	for (SHScheduleItem scheduleItem : schedule.values()) {
    		reminderManager.cancelReminder(scheduleItem);
    	}
    	// Clear the Schedule
    	schedule.clear();
    	// Cancel Notifications
    	((NotificationManager) context.getSystemService(IntentService.NOTIFICATION_SERVICE)).cancelAll();

    	ArrayList<SHScheduleItem> scheduleItems = (ArrayList<SHScheduleItem>) SHController.getInstance(context).getTodaysSchedule();

        for (SHScheduleItem scheduleItem : scheduleItems) {
        	
    		if (scheduleItem.isCompleted()) {
    			
    			scheduleItem.setReminder(null);
    			this.schedule.put(scheduleItem.scheduleID(), scheduleItem);
    		} else {
    			if (scheduleItem.timeScheduled().getMillisOfDay() > LocalDateTime.now().getMillisOfDay()) {

    				scheduleItem.setReminder(reminderManager.makeReminder(scheduleItem));
    				this.schedule.put(scheduleItem.scheduleID(), scheduleItem);
    			} else {
    				scheduleItem.setReminder(null);
    			}
    		}
        }
        
        scheduleDailyUpdate();
        if(scheduleUpdatedListener != null) scheduleUpdatedListener.onScheduleUpdated();
	}


    public void scheduleDailyUpdate(){
    	
        Intent intent = new Intent(context, SHScheduleBroadcastReceiver.class);
        intent.setAction(SHConstants.ACTION_SCHEDULE_UPDATE);
        
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        
        // Set for tomorrow at midnight
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.roll(Calendar.DAY_OF_YEAR, true);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        long start = cal.getTimeInMillis();
        
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, start, pendingIntent);
    }
    
    public Hashtable<Integer, SHScheduleItem> getScheduleTable(){
        return schedule;
    }

    public SHScheduleItem getScheduleItem(int id) {
        return schedule.get(id);
    }

    public void snoozeReminder(int id) {
        reminderManager.snoozeReminder(getScheduleItem(id));
    }

    public void completeScheduleItem(int scheduleID, int completionID) {
    	
        SHScheduleItem item = schedule.get(scheduleID);
        if (item != null) {
        	
            item.setCompleted(completionID);
            this.schedule.put(scheduleID, item);
            
            Log.i(TAG, "Completing Item... ScheduleID: " + scheduleID);
            reminderManager.cancelReminder(item);
        }
        
        if(scheduleUpdatedListener != null)
      	   scheduleUpdatedListener.onScheduleUpdated();
    }

    public void cancelScheduleItem(int scheduleID) {
        reminderManager.cancelReminder(schedule.get(scheduleID));
    }

    public ArrayList<SHScheduleItem> getScheduledItems() {
    	
        ArrayList<SHScheduleItem> items = new ArrayList<SHScheduleItem>();
        for (SHScheduleItem item : schedule.values()) {
            if (!item.isCompleted()) items.add(item);
        }
        Collections.sort(items);
        return items;
    }
    
    public ArrayList<SHScheduleItem> getCompletedItems(){
    	
    	ArrayList<SHScheduleItem> items = new ArrayList<SHScheduleItem>();
        for (SHScheduleItem item : schedule.values()) {
            if (item.isCompleted()) items.add(item);
        }
        Collections.sort(items);
        return items;
    }

    public ArrayList<String> getAsArrayList(){
    	
        ArrayList<String> items = new ArrayList<String>();
        for (SHScheduleItem item : schedule.values()) {
            items.add(item.toString());
        }
        Collections.sort(items);  // TODO : Implement sort before pulling from schedule
        return items;
    }
}

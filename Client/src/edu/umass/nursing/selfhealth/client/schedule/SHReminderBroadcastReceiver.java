/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.schedule;

import edu.umass.nursing.selfhealth.client.application.SHConstants;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public class SHReminderBroadcastReceiver extends BroadcastReceiver {
		
	@Override @SuppressWarnings("deprecation") @SuppressLint("Wakelock")
	public void onReceive(Context context, Intent intent){
		
		String action = intent.getAction();
		
		if (action == SHConstants.ACTION_SEND_REMINDER){
			
			int reminderID = intent.getIntExtra(SHConstants.REMINDER_ID, -1);
			String message = intent.getStringExtra(SHConstants.REMINDER_MESSAGE);
			
			Intent notification = new Intent(context, SHReminderService.class);
			notification.putExtra(SHConstants.REMINDER_ID, reminderID);
			notification.putExtra(SHConstants.REMINDER_MESSAGE, message);
			
			// Wake up the screen if it's off
			PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
			
			if (!powerManager.isScreenOn()){	
				
			    WakeLock wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK|PowerManager.ACQUIRE_CAUSES_WAKEUP|PowerManager.ON_AFTER_RELEASE , "MyLock");
			    wakeLock.acquire(1000 * 120); // two minutes
	            WakeLock cpuLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
	            cpuLock.acquire(1000 * 120); // two minutes

			}
			context.startService(notification);
		}
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.schedule;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.joda.time.LocalTime;

import edu.umass.nursing.selfhealth.client.application.SHConstants;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SHReminderManager {
	
	private Context context;
	private AlarmManager alarmManager;
	
	private Hashtable<Integer, PendingIntent> reminders;
	
	private static SHReminderManager instance = null;
	
	public static SHReminderManager getInstance(Context c){
		if (instance == null)
			instance = new SHReminderManager(c);
		return instance;
	}
	
	private SHReminderManager(Context context){
		this.context 		= context;
		this.reminders 		= new Hashtable<Integer, PendingIntent>();
		this.alarmManager 	= (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	}
	
	private void scheduleReminder(SHReminder reminder) {
		
		Intent intent = new Intent(context, SHReminderBroadcastReceiver.class);
		intent.putExtra(SHConstants.REMINDER_ID, reminder.getReminderID());
		intent.putExtra(SHConstants.REMINDER_MESSAGE, reminder.getMessage());
		intent.setAction(SHConstants.ACTION_SEND_REMINDER);
		
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, reminder.getReminderID(), intent, PendingIntent.FLAG_ONE_SHOT);
		
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.HOUR_OF_DAY, reminder.getReminderTime().getHourOfDay());
		cal.set(Calendar.MINUTE, reminder.getReminderTime().getMinuteOfHour());
		
		Log.i("SH_DB", "Reminder Scheduled...");
		alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
		reminders.put(reminder.getReminderID(), pendingIntent);
	}
	
	private void cancelReminder(SHReminder reminder) {
		
		if(reminder == null)
			return;
		
		alarmManager.cancel(reminders.get(reminder.getReminderID()));
		reminders.remove(reminder.getReminderID());
		
		if (reminder.getNotificationID() != SHConstants.DEFAULT_NOTIFICATION_ID) {
		    NotificationManager notificationManager = (NotificationManager) context.getSystemService(IntentService.NOTIFICATION_SERVICE);
		    notificationManager.cancel(reminder.getNotificationID());     
		}
	}
	
	public void cancelReminder(SHScheduleItem scheduleItem) {
		cancelReminder(scheduleItem.getReminder());
		scheduleItem.setReminder(null);
	}
	
	public SHReminder makeReminder(SHScheduleItem scheduleItem) {
		
		if(scheduleItem.getReminder() != null && reminders.containsKey(scheduleItem.scheduleID())) {
			Log.i("SH_DB", "Reminder has already been scheduled. Returning reminder...");
			return scheduleItem.getReminder();
		}
		
		SHReminder reminder = new SHReminder(scheduleItem.scheduleID(), scheduleItem.timeScheduled(), scheduleItem.getMessage());
		scheduleReminder(reminder);
		return reminder;
	}
	
	public void snoozeReminder(SHScheduleItem scheduleItem) {

		cancelReminder(scheduleItem.getReminder());
		
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(System.currentTimeMillis() + SHConstants.SNOOZE_INTERVAL_SHORT);
		
		// TODO : Check if this LocalTime from Cal is working properly
		SHReminder reminder = new SHReminder(scheduleItem.scheduleID(), LocalTime.fromCalendarFields(cal), scheduleItem.getMessage());
		scheduleReminder(reminder);
		scheduleItem.setReminder(reminder);
	}
}

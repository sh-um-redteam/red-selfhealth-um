/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.schedule;

import edu.umass.nursing.selfhealth.client.application.SHConstants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public class SHScheduleBroadcastReceiver extends BroadcastReceiver {
	
    @Override
    public void onReceive(Context context, Intent intent) {
    	
        String action = intent.getAction();
        Log.d("SHScheduleBroadcastReceiver received: ", action);
        
        if (action.equals(SHConstants.ACTION_BOOT_COMPLETED)) {
        	
            // Start the application
            Intent startIntent = new Intent(context, edu.umass.nursing.selfhealth.client.activities.SHHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(startIntent);
        }
        
        if (action.equals(SHConstants.ACTION_SCHEDULE_UPDATE)) {
        	
            // Grab the CPU lock
            PowerManager powerManager 	= (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            WakeLock wakeLock 			= powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
            
            wakeLock.acquire(); 
            SHSchedule schedule = SHSchedule.getInstance(context);            
            schedule.performDailyUpdate();
            wakeLock.release();
        }
    }

}

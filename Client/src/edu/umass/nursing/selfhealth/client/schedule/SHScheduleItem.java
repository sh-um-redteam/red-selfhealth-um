/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.schedule;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import android.content.Context;
import android.content.Intent;

import edu.umass.nursing.selfhealth.client.application.SHApplication;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.records.SHRecord;
import edu.umass.nursing.selfhealth.client.tools.SHSelfManagementTool;

public class SHScheduleItem implements Comparable<SHScheduleItem> {
	
	private int scheduleID;
	private Integer completionID;

	private SHRecord record;
	private SHSelfManagementTool tool;
	
	private LocalTime timeScheduled;
	private LocalDateTime timeCompleted;
	
	private SHReminder reminder;

	public SHScheduleItem(int scheduleID, Integer completionID, LocalTime timeScheduled) {
		
		this.scheduleID = scheduleID;
		this.completionID = completionID;
		this.timeScheduled = timeScheduled;
		
		this.tool = SHController.getInstance(SHApplication.getContext()).getSelfManagementTool(scheduleID);
		if(completionID != null) {
			record = SHController.getInstance(SHApplication.getContext()).getRecord(completionID);
			this.timeCompleted = record.getTimeCompleted();
		}
	}
	
	public int scheduleID()					{ return scheduleID; }
	
	public void setNotificationID(int id) 	{ reminder.setNotificationID(id); }
	
	public boolean isCompleted() 			{ return completionID != null; }
	
	public LocalTime timeScheduled() 		{ return timeScheduled; }
	
	public LocalDateTime timeCompleted() 	{ return timeCompleted; }
	
	public SHSelfManagementTool getTool() 	{ return tool; }
	
	public SHRecord getRecord() {
		if(record == null) {
			if(this.isCompleted()) {
				record = SHController.getInstance(SHApplication.getContext()).getRecord(completionID);
			} else {
				return null;
			}
		}
		return record;
	}
	
	public void setCompleted(Integer completionID) {
		
		record = SHController.getInstance(SHApplication.getContext()).getRecord(completionID);
		
		this.completionID = completionID;
		this.timeCompleted = record.getTimeCompleted();
	}
	
	public Intent getIntent(Context c) {
		Intent intent = getTool().getIntent(c);
		intent.putExtra(SHConstants.AD_HOC, false);
		intent.putExtra(SHConstants.SCHEDULE_ID, scheduleID);
		return intent;
	}
	
	public String getMessage(){
		return tool.toString();
	}
	
	public void setReminder(SHReminder reminder){
		this.reminder = reminder;
	}
	
	public SHReminder getReminder(){
		return reminder;
	}
	
	////////////////////////// Visual Logic //////////////////////////
	
	public LocalTime displayTime(){
		if(isCompleted()) {
			return timeCompleted.toLocalTime();
		} else {
			return timeScheduled;
		}
	}
	
	public int displayIcon(){
		return tool.getIcon();
	}
	
	@Override
	public String toString(){
		if(isCompleted()) {
			return record.toString();
		} else {
			return tool.toString();
		}
	}
	
	@Override
	public int compareTo(SHScheduleItem other) {
		
		if (other == null) {
			return -1;
			
		} else if(this.isCompleted() && other.isCompleted()) {
			
			int thisValue 	= timeCompleted.getMillisOfDay();
			int otherValue 	= other.timeCompleted().getMillisOfDay();
			
			if(thisValue == otherValue)
				return 0;
			if(thisValue > otherValue)
				return -1;
			if(thisValue < otherValue)
				return 1;
			
			return -1;
			
		} else {
			
			return timeScheduled.compareTo(other.timeScheduled());
		}
	}
}

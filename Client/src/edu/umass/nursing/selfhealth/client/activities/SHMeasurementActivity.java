/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import java.util.HashMap;

import org.joda.time.LocalDateTime;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.fragments.SHMeasurementFragment;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricComponent;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;
import edu.umass.nursing.selfhealth.client.records.SHMeasurableHealthMetricRecord;

public class SHMeasurementActivity extends SHActivityWithActionBar {

	private SHHealthMetricID healthMetricID;
	private FragmentManager fragmentManager;
	private SHMeasurementFragment measurementFragment;
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		fragmentManager.putFragment(outState, "MEASUREMENT_FRAGMENT", fragmentManager.findFragmentById(R.id.contentPanel));
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, "Measurement");
        setContentView(R.layout.sh_activity_single_pane);
        
        fragmentManager = getFragmentManager();
        
        if(savedInstanceState == null) {
        	measurementFragment = new SHMeasurementFragment();
        	fragmentManager.beginTransaction().add(R.id.contentPanel, measurementFragment, "MEASUREMENT_FRAGMENT").commit();
        } else {
        	measurementFragment = (SHMeasurementFragment) fragmentManager.getFragment(savedInstanceState, "MEASUREMENT_FRAGMENT");
        }
        this.healthMetricID = (SHHealthMetricID) getIntent().getSerializableExtra(SHConstants.HEALTH_METRIC_ID);
	}
	
	@Override
	public void onStart(){
		super.onStart();
		measurementFragment.initializeMeasurementComponentPanes(healthMetricID);
	}
	
	public void onConfirm(View v) {
		
		HashMap<SHHealthMetricComponent, Number> measurements = measurementFragment.getMeasurements();
		if(measurements != null) {
			Integer scheduleID 	= (Integer) getIntent().getExtras().getSerializable(SHConstants.SCHEDULE_ID);
			int completionID = SHController.getInstance(this).saveHealthMetricRecord(new SHMeasurableHealthMetricRecord(healthMetricID, LocalDateTime.now(), measurements), scheduleID);
			
			if(scheduleID != null) {
	    		setResult(RESULT_OK, new Intent().putExtra(SHConstants.COMPLETION_ID, completionID));
	    	}
			for(Number values: measurements.values() ){
				if (values.intValue() < 100)
				{
					buildPRNDialog("The metric you entered surpassed the threshold. Would you like to take:                            Advil 1.0x 800mg Oral");
					break;
				}
				else
					this.finish();
			}
			//this.finish();
		}
	}


private void buildPRNDialog(String msg) {
	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	final Activity activity = this;
	builder.setTitle("PRN Prompt");
	builder.setMessage(msg);
	builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) 
		{
			showToastWithText("You declined the medication");
			activity.finish();
		}
	});
	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) 
		{
			showToastWithText("The medication has been logged.");
			activity.finish();
		}
	});

	AlertDialog dialog= builder.create();
	dialog.getWindow().setGravity(Gravity.CENTER);
	dialog.show();
}

private void showToastWithText(String msg) {
	Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
	toast.setGravity(Gravity.CENTER,0,0);
	toast.show();
}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import java.util.ArrayList;
import java.util.HashMap;

import org.joda.time.LocalDateTime;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.fragments.SHSurveyQuestionFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHSurveyResponseFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHSurveyResponseFragment.OnResponseSelectedListener;
import edu.umass.nursing.selfhealth.client.records.SHSurveyRecord;
import edu.umass.nursing.selfhealth.client.surveys.SHSurvey;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyID;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyQuestion;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyResponse;

@SuppressLint("UseSparseArrays")
public class SHSurveyActivity extends SHActivityWithActionBar implements OnResponseSelectedListener {

	private HashMap<Integer, Integer> responses;
	private HashMap<Integer, Integer> responseIndices;
	private int questionIndex;
	
	private SHSurvey shSurvey;
	private SHSurveyID surveyID;
	
	private Button nextButton;
	private Button previousButton;
	
	private FragmentManager fragmentManager; 
	private SHSurveyResponseFragment responsesFragment;
	private SHSurveyQuestionFragment questionFragment;
	
	OnResponseSetChangedListener responseListener;
	OnQuestionSelectedListener questionListener;
	
	public interface OnResponseSetChangedListener {
		public void onResponseSetChanged(Integer selectedIndex, ArrayList<SHSurveyResponse> responseSet);
	}
	
	public interface OnQuestionSelectedListener {
		public void onQuestionSelected(SHSurveyQuestion question);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		fragmentManager.putFragment(outState, "RESPONSES_FRAGMENT", responsesFragment);
		fragmentManager.putFragment(outState, "QUESTION_FRAGMENT", questionFragment);
		
		outState.putInt("CURRENT_QUESTION", questionIndex);
		outState.putSerializable("RESPONSES", responses);
		outState.putSerializable("RESPONSE_INDICES", responseIndices);
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        
        fragmentManager = getFragmentManager();
        this.surveyID 	= (SHSurveyID) getIntent().getExtras().get(SHConstants.SURVEY_ID);

        super.onCreate(savedInstanceState, surveyID.toString());
        setContentView(R.layout.sh_activity_survey);
        
        nextButton 		= (Button) findViewById(R.id.nextButton);
        previousButton 	= (Button) findViewById(R.id.previousButton);
        
        if(savedInstanceState == null) {
            this.responses 			= new HashMap<Integer, Integer>();
            this.responseIndices	= new HashMap<Integer, Integer>();
            
            responsesFragment 		= new SHSurveyResponseFragment();
        	questionFragment 		= new SHSurveyQuestionFragment();
        	
        	fragmentManager.beginTransaction()
        		.add(R.id.questionPanel, questionFragment, "QUESTION")
        		.add(R.id.responsePanel, responsesFragment, "RESPONSES")
            	.commit();
        	
        } else {
        	
        	responsesFragment 		= (SHSurveyResponseFragment) fragmentManager.getFragment(savedInstanceState, "RESPONSES_FRAGMENT");
        	questionFragment 		= (SHSurveyQuestionFragment) fragmentManager.getFragment(savedInstanceState, "QUESTION_FRAGMENT");
        	
        	this.questionIndex 		= (Integer) savedInstanceState.get("CURRENT_QUESTION");
        	this.responses 			= (HashMap<Integer, Integer>) savedInstanceState.get("RESPONSES");
        	this.responseIndices	= (HashMap<Integer, Integer>) savedInstanceState.get("RESPONSE_INDICES");
        }
        this.shSurvey = SHSurveyID.generateSurvey(surveyID);
        responseListener = responsesFragment;
        questionListener = questionFragment;
        updateButtons();
	}
	
	@Override
    public void onStart() {
		super.onStart();
		notifyListeners();
	}
	
	@Override
    public void onResponseSelected(boolean selected, int responseIndex, int responseID) {
		if(selected) {
			responseIndices.put(questionIndex, responseIndex);
			responses.put(shSurvey.getQuestionsAndResponseOptions().get(questionIndex).first.getQuestionID(), responseID);
		} else {
			responseIndices.remove(questionIndex);
			responses.remove(questionIndex);
		}
		
	}
	
	public void nextQuestion(View v) {
		if(!responses.containsKey(shSurvey.getQuestionsAndResponseOptions().get(questionIndex).first.getQuestionID())) {
			responses.put(shSurvey.getQuestionsAndResponseOptions().get(questionIndex).first.getQuestionID(), SHSurveyResponse.NO_RESPONSE_ID);
		}
		
		if(questionIndex < shSurvey.size()-1) {
			questionIndex++;
			notifyListeners();
			updateButtons();
        } else {
        	Integer scheduleID = (Integer) getIntent().getExtras().getSerializable(SHConstants.SCHEDULE_ID);
        	Log.i("SH", "Completing SURVEY with SCHEDULE ID: " +scheduleID);
        	SHSurveyRecord record = new SHSurveyRecord(surveyID, responses, LocalDateTime.now());
        	int completionID = SHController.getInstance(this).saveSurveyRecord(record, scheduleID);
        	
        	if(scheduleID != null) {
        		Intent intent = new Intent().putExtra(SHConstants.COMPLETION_ID, completionID);
        		setResult(RESULT_OK, intent);
        	}
        	if(record.getScore() < 1000){
				//AlertDialog alertdialog;
        		showSocialAppChooser();
				//buildPRNDialog("Your score on this survey surpassed the threshold. Would you like to" +
						//" take an advil?");	
			}
        	else{
        		this.finish();
        	}
        }
	}


	private void buildPRNDialog(String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final Activity activity = this;
		builder.setTitle("PRN Prompt");
		builder.setMessage(msg);
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				showToastWithText("You declined the medication");
				activity.finish();
			}
		});
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				showToastWithText("The medication has been logged.");
				activity.finish();
			}
		});

		AlertDialog dialog= builder.create();
		dialog.getWindow().setGravity(Gravity.CENTER);
		dialog.show();
	}
	
	public void previousQuestion(View v) {
		if(!responses.containsKey(shSurvey.getQuestionsAndResponseOptions().get(questionIndex).first.getQuestionID())) {
			responses.put(shSurvey.getQuestionsAndResponseOptions().get(questionIndex).first.getQuestionID(), SHSurveyResponse.NO_RESPONSE_ID);
		}
		questionIndex--;
		notifyListeners();
		updateButtons();
	}

	private void notifyListeners() {
		responseListener.onResponseSetChanged(responseIndices.get(questionIndex), shSurvey.getQuestionsAndResponseOptions().get(questionIndex).second);
		questionListener.onQuestionSelected(shSurvey.getQuestionsAndResponseOptions().get(questionIndex).first);
	}

	private void updateButtons() {
		if(questionIndex == 0) {
			previousButton.setVisibility(View.GONE);
		} else {
			previousButton.setVisibility(View.VISIBLE);
		}
		if(questionIndex == shSurvey.size()-1) {
			nextButton.setText("Finish");
		} else {
			nextButton.setText("Next");
		}
	}


	private void showToastWithText(String msg) {
		Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER,0,0);
		toast.show();
	}
	/*private void showSocialAppChooser(String msg){

		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
		sendIntent.setType("text/plain");
		startActivity(Intent.createChooser(sendIntent, "Testing"));
		}
	 */
	private void showSocialAppChooser(){

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final Activity activity = this;
		LayoutInflater inflater = this.getLayoutInflater();
		View view = inflater.inflate(R.layout.sh_socialmedia_dialog, null);
		builder.setTitle("Social Media Prompt");

		builder.setView(view)
		// Add action buttons

		.setNegativeButton("No Thanks" , new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				activity.finish(); 
			}
		});    
		ImageButton skype = (ImageButton)view.findViewById(R.id.skype);
		ImageButton facebook = (ImageButton)view.findViewById(R.id.facebook);

		facebook.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				launchApplication("com.facebook.katana");
			}
		});

		skype.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				launchApplication("com.skype.raider");
			}

		});

		AlertDialog dialog= builder.create();
		dialog.show();
	}

	private void launchApplication(String packageName){
		Intent i;
		PackageManager manager = getPackageManager();
		try {
			i = manager.getLaunchIntentForPackage(packageName);
			if (i == null)
				throw new PackageManager.NameNotFoundException();
			i.addCategory(Intent.CATEGORY_LAUNCHER);
			startActivity(i);
		} catch (PackageManager.NameNotFoundException e) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Application is not installed.");
			builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) 
				{
					dialog.dismiss();
				}
			});
			AlertDialog dialog= builder.create();
			dialog.show();
			
			
		}
	}

}

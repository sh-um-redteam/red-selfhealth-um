/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;

import edu.umass.nursing.selfhealth.client.R;


public abstract class SHActivityWithActionBar extends Activity {
	
	private ActionBar defaultActionBar;
	private View bar;
	private View barHomeButton; 
	private View barBackButton;
	private TextView barTitle;
	
    protected void onCreate(Bundle savedInstanceState, String contextTitle) {
        super.onCreate(savedInstanceState);
        setupActionBar(contextTitle);
	}
	
	private void setupActionBar(String title){
	    	
		// Access the ActionBar
    	defaultActionBar = getActionBar();
    	
    	// Remove default text and home button
        defaultActionBar.setDisplayShowHomeEnabled(false);
        defaultActionBar.setDisplayShowTitleEnabled(false);
        
        // Inflate the custom components (bar, title, and back + home buttons)
        LayoutInflater inflater = LayoutInflater.from(this);
        
        bar 			= inflater.inflate(R.layout.action_bar, null);
        barHomeButton 	=  bar.findViewById(R.id.HomeButton); 
        barBackButton 	=  bar.findViewById(R.id.BackButton);
        barTitle 		= (TextView) bar.findViewById(R.id.TitleText);
        
        // Populate the title view with the given text
        barTitle.setText(title);

        // Pop all activities back to the Home activity when the 'Home' button is pressed
        barHomeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	Intent intent = new Intent(view.getContext(), SHHomeActivity.class);
            	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            	startActivity(intent);
            }
        });
        
        // Pop the activity from the stack when 'Back' is pressed
        barBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	SHActivityWithActionBar.this.onBackPressed();
            }
        });

        // Make the changes visible to the user
        defaultActionBar.setCustomView(bar);
        defaultActionBar.setDisplayShowCustomEnabled(true);
    }
	
	public void setupHomeActionBar(String title) {
		
        // Hide the 'Home' button component
        barHomeButton.setVisibility(View.INVISIBLE);
        
        // Find and hide the 'Back' button
        barBackButton.setVisibility(View.INVISIBLE);
        MarginLayoutParams params = (MarginLayoutParams) barTitle.getLayoutParams();
        params.leftMargin = 30;
        
        barTitle.setLayoutParams(params);
        barTitle.setText(title);

        // Make the changes visible to the user
        defaultActionBar.setCustomView(bar);
        defaultActionBar.setDisplayShowCustomEnabled(true);
	}
	
	public void updateTitle(String title) {
		barTitle.setText(title);
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;
//test
/**
 *	Okay, this is not even correct--there is a flaw somewhere in the logic, but it currently works.
 *	I HIGHLY recommend that instead of using a single view-frame on portrait activities, that you 
 *	instead use multiple views and toggle visibility.  Fragments can be a nightmare, and I am
 *	still at a loss for making them work let alone use them efficiently.
 */
public enum SHActivityState {

		DUAL_PANE_FROM_HOME,
		DUAL_PANE_FROM_MENU,
		DUAL_PANE_FROM_CONTENT,
		MENU_FROM_HOME,
		MENU_FROM_CONTENT,
		CONTENT_FROM_MENU,
		CONTENT_FROM_DUAL_PANE;
		
		/* 
		 * Generic Layout Naming Convention
		 * 
		Landscape Layout							Portrait		
		_____________________________________		_____________		
		|			|						|		|			|		
		|			|						|		|			|		
		|			|						|		|			|		
		|	MENU	|		CONTENT			|		|  CONTENT	|	
		|			|						|		|			|	
		|			|						|		|			|	
		|___________|_______________________|		|___________|	
		*
		*/
		
		public static SHActivityState getCurrentState(SHActivityState previousState, boolean isDualPane) {
			if((previousState == null || previousState == DUAL_PANE_FROM_HOME) && isDualPane) {
				return DUAL_PANE_FROM_HOME;
				
			} else if((previousState == MENU_FROM_HOME || previousState == MENU_FROM_CONTENT) && isDualPane) {
				return DUAL_PANE_FROM_MENU;
				
			} else if((previousState == CONTENT_FROM_MENU || previousState == CONTENT_FROM_DUAL_PANE) && isDualPane) {
				return DUAL_PANE_FROM_CONTENT;
				
			} else if((previousState == MENU_FROM_HOME || previousState == MENU_FROM_CONTENT) && !isDualPane) {
				return CONTENT_FROM_MENU;
				
			} else if((previousState == DUAL_PANE_FROM_HOME || 
					previousState == DUAL_PANE_FROM_MENU || 
					previousState == DUAL_PANE_FROM_CONTENT) && !isDualPane) {
				return CONTENT_FROM_DUAL_PANE;
				
			} else if((previousState == CONTENT_FROM_MENU || previousState == CONTENT_FROM_DUAL_PANE) && !isDualPane) {
				return MENU_FROM_CONTENT;
				
			} else if((previousState == null || previousState == MENU_FROM_HOME) && !isDualPane) {
				return MENU_FROM_HOME;
				
			} else {
				return null;
			}
		}
}

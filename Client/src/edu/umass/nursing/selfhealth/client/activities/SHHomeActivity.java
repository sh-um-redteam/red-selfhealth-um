/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import java.util.ArrayList;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.fragments.SHHomeMenuFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHHomeMenuFragment.OnScheduleViewToggledListener;
import edu.umass.nursing.selfhealth.client.fragments.SHScheduleFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHScheduleFragment.OnScheduleItemSelectedListener;
import edu.umass.nursing.selfhealth.client.patient.SHPatient;
import edu.umass.nursing.selfhealth.client.schedule.SHSchedule;
import edu.umass.nursing.selfhealth.client.schedule.SHSchedule.OnScheduleUpdatedListener;
import edu.umass.nursing.selfhealth.client.schedule.SHScheduleItem;

public class SHHomeActivity extends SHActivityWithActionBar implements OnScheduleItemSelectedListener, OnScheduleUpdatedListener, OnScheduleViewToggledListener {

	// Datasource
	private static SHPatient patient;
	private static SHSchedule shSchedule;
	private ArrayList<SHScheduleItem> scheduledEvents = new ArrayList<SHScheduleItem>();
	private ArrayList<SHScheduleItem> completedEvents = new ArrayList<SHScheduleItem>();
	
	private boolean viewingScheduledEvents;
	
	private OnScheduleToggledListener scheduleToggledListener;
		
	// Interfaces
	public interface OnScheduleToggledListener {
		public void onViewSchedule(ArrayList<SHScheduleItem> scheduledItems);
		public void onViewCompleted(ArrayList<SHScheduleItem> completedItems);
	}
	
	@Override
	public void onScheduleUpdated() {
		updateSchedule();
	}
	
	@Override
	public void viewSchedule() {
		viewingScheduledEvents = true;
		scheduleToggledListener.onViewSchedule(scheduledEvents);
	}

	@Override
	public void viewCompleted() {
		viewingScheduledEvents = false;
		scheduleToggledListener.onViewCompleted(completedEvents);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.putFragment(outState, "HOME_MENU", fragmentManager.findFragmentById(R.id.sidePanel));
		fragmentManager.putFragment(outState, "SCHEDULE", fragmentManager.findFragmentById(R.id.contentPanel));
		
		outState.putBoolean("VIEWING_SCHEDULED_EVENTS", viewingScheduledEvents);
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, "Home");
        setContentView(R.layout.sh_activity_home);
        
        SHController.getInstance(this).loadConfiguration();
        
        patient = SHController.getInstance(this).getPatient();
        setupHomeActionBar("Hello, " + patient.getFirstName());
        
        shSchedule = SHSchedule.getInstance(this);
        shSchedule.registerOnScheduleUpdatedListener(this);
		
		FragmentManager fragmentManager = getFragmentManager();
        
        SHScheduleFragment scheduleFragment;
        SHHomeMenuFragment menuFragment;
        
        if(savedInstanceState != null) {
        	
        	scheduleFragment = (SHScheduleFragment) fragmentManager.getFragment(savedInstanceState, "SCHEDULE");
        	menuFragment = (SHHomeMenuFragment) fragmentManager.getFragment(savedInstanceState, "HOME_MENU");
        	this.viewingScheduledEvents = (Boolean) savedInstanceState.get("VIEWING_SCHEDULED_EVENTS");
        } else {
        	
        	FragmentTransaction transaction = fragmentManager.beginTransaction();
        	scheduleFragment = new SHScheduleFragment();
            menuFragment = new SHHomeMenuFragment();
            transaction.add(R.id.contentPanel, scheduleFragment, "SCHEDULE");
            transaction.add(R.id.sidePanel, menuFragment, "HOME_MENU");
            transaction.commit();
        	this.viewingScheduledEvents = true;
        }
        scheduleToggledListener = scheduleFragment;
	}
	
	@Override
	protected void onStart() {
    	super.onStart();
    	updateSchedule();
    }
	
	private void updateSchedule() {
		
		scheduledEvents = shSchedule.getScheduledItems();
		completedEvents = shSchedule.getCompletedItems();
		
		if(viewingScheduledEvents) {
			scheduleToggledListener.onViewSchedule(scheduledEvents);
		} else {
			scheduleToggledListener.onViewCompleted(completedEvents);
		}
	}
	
	@Override
	public void onScheduleItemSelected(int scheduleItemIndex) {
    	startActivityForResult(scheduledEvents.get(scheduleItemIndex).getIntent(this), scheduleItemIndex);
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
        	Log.i("SH", "Completing SCHEDULE ITEM with COMPLETEION ID: " + data.getIntExtra((SHConstants.COMPLETION_ID), -1));
        	shSchedule.completeScheduleItem(scheduledEvents.get(requestCode).scheduleID(), data.getIntExtra((SHConstants.COMPLETION_ID), -1));
            updateSchedule();
        }
    }
}

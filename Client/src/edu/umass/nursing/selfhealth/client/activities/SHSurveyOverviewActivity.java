/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.fragments.SHSurveyHistoryFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHSurveyMenuFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHSurveyMenuFragment.OnSurveySelectedListener;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyID;

//TODO : Follow Pattern as implemented in {@link SHMedicationOverviewActivity}

public class SHSurveyOverviewActivity extends SHActivityWithActionBar implements OnSurveySelectedListener {

	// Local Logic
	private boolean isDualPane;
	private SHSurveyID surveyID;
	
	private SHActivityState currentState;
	
	// Listeners
	private OnViewSurveyHistorListener viewSurveyHistoryListener;
	private SurveyHistoryRequestedListener historyRequestListener;
	
	// Fragment References
	private FragmentManager fragmentManager;
	private SHSurveyMenuFragment menuFragment;
	private SHSurveyHistoryFragment historyFragment;
	private final String MENU_TAG = "SURVEY_MENU";
	private final String HISTORY_TAG = "SURVEY_HISTORY";
		
	// Interfaces
	public interface OnViewSurveyHistorListener {
		public void onViewSurveyHistory(SHSurveyID surveyID);
	}
	public interface SurveyHistoryRequestedListener {
		public void onSurveyHistoryRequested();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		menuFragment	= (SHSurveyMenuFragment) fragmentManager.findFragmentByTag(MENU_TAG);
		historyFragment = (SHSurveyHistoryFragment) fragmentManager.findFragmentByTag(HISTORY_TAG);
    	
    	if(isDualPane) {
    		fragmentManager.putFragment(outState, HISTORY_TAG, historyFragment);
    		fragmentManager.putFragment(outState, MENU_TAG, menuFragment);
    	} else if(currentState == SHActivityState.MENU_FROM_HOME || currentState == SHActivityState.MENU_FROM_CONTENT){
    		fragmentManager.putFragment(outState, MENU_TAG, menuFragment);
    	} else {
    		fragmentManager.putFragment(outState, HISTORY_TAG, historyFragment);
    	}
    		
		outState.putSerializable("PREVIOUS_STATE", currentState);
		outState.putSerializable("SH_SURVEY", surveyID);
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, "Surveys");
        setContentView(R.layout.sh_activity_dual_pane);
        
        this.isDualPane 		= isDualPaneOrientation();
        this.fragmentManager	= getFragmentManager();
		
        if(savedInstanceState == null) {
        	
        	this.currentState = getInitialState();
        	
        	menuFragment 	= new SHSurveyMenuFragment();
    		historyFragment = new SHSurveyHistoryFragment();
    		
        	FragmentTransaction transaction = fragmentManager.beginTransaction();
        	
        	if(isDualPane) {
        		transaction.add(R.id.sidePanel, menuFragment, MENU_TAG);
        		transaction.add(R.id.contentPanel, historyFragment, HISTORY_TAG);
        	} else {
        		transaction.add(R.id.contentPanel, menuFragment, MENU_TAG);
        	}
        	
            transaction.commit();
            
        } else {
        	
        	this.surveyID 	= (SHSurveyID) savedInstanceState.get("SH_SURVEY");
        	this.currentState 	= SHActivityState.getCurrentState((SHActivityState) savedInstanceState.get("PREVIOUS_STATE"), isDualPane);
        	
        	historyFragment = (SHSurveyHistoryFragment) fragmentManager.getFragment(savedInstanceState, HISTORY_TAG);
        	menuFragment	= (SHSurveyMenuFragment) fragmentManager.getFragment(savedInstanceState, MENU_TAG);
        	
        	if(historyFragment == null)
        		historyFragment = new SHSurveyHistoryFragment();
        	if(menuFragment == null)
        		menuFragment 	= new SHSurveyMenuFragment();
        	
        	switch(currentState) {
        	
	        	case DUAL_PANE_FROM_MENU:
	        		
	        		fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	        		fragmentManager.beginTransaction().remove(menuFragment).commit();
	        		fragmentManager.executePendingTransactions();
	        		fragmentManager.beginTransaction()
	        		    .replace(R.id.sidePanel, menuFragment, MENU_TAG)
	        		    .add(R.id.contentPanel, historyFragment, HISTORY_TAG)
	        		    .commit();              
	        		break;
	        		
	        	case DUAL_PANE_FROM_CONTENT:
	        		fragmentManager.beginTransaction().replace(R.id.sidePanel, menuFragment, MENU_TAG).commit();
	        		break;
	        		
	        	case MENU_FROM_CONTENT:
	        		fragmentManager.beginTransaction().replace(R.id.contentPanel, menuFragment, MENU_TAG).commit();
	        		break;
	        		
	        	case CONTENT_FROM_MENU:
	        		fragmentManager.beginTransaction().replace(R.id.contentPanel, historyFragment, HISTORY_TAG).commit();
	        		break;
	        		
	        	case CONTENT_FROM_DUAL_PANE:
	        		break;
	        		
	        	default:
	        		break;

        	}
        	updateTitle();
        }
        viewSurveyHistoryListener = historyFragment;
        historyRequestListener = menuFragment;
	}
	
	@Override
	protected void onStart() {
    	super.onStart();
    	
    	if(isDualPane) {
    		historyRequestListener.onSurveyHistoryRequested();
    	} else if(currentState == SHActivityState.CONTENT_FROM_DUAL_PANE || currentState == SHActivityState.CONTENT_FROM_MENU) {
    		viewSurveyHistoryListener.onViewSurveyHistory(surveyID);
    	}
    }
	
	@Override
	// Preserves the BackStack and State transition -- injects fragments into the stack when necessary
	public void onBackPressed() {
		if(currentState == SHActivityState.CONTENT_FROM_DUAL_PANE) {
			
			currentState = SHActivityState.getCurrentState(currentState, isDualPane);
			
			fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    		fragmentManager.beginTransaction().remove(menuFragment).commit();
    		fragmentManager.executePendingTransactions();
    		fragmentManager.beginTransaction()
    		    .replace(R.id.contentPanel, menuFragment, MENU_TAG)
    		    .commit();   
    		fragmentManager.executePendingTransactions();
		} else {
	        super.onBackPressed();
	    }
		updateTitle();
	}
	
	@Override
	public void onSurveySelected(SHSurveyID surveyID1) {
		
		this.surveyID = surveyID1;
		if(currentState == SHActivityState.MENU_FROM_HOME || currentState == SHActivityState.MENU_FROM_CONTENT) {
			
			if(historyFragment == null) {
				historyFragment = new SHSurveyHistoryFragment();
        		viewSurveyHistoryListener = historyFragment;
			}
        		
			fragmentManager.beginTransaction().replace(R.id.contentPanel, historyFragment).addToBackStack(null).commit();
			fragmentManager.executePendingTransactions();
		}
		viewSurveyHistoryListener.onViewSurveyHistory(surveyID1);
		updateTitle();
	}
	
	// OnClick Listener -- Transition Activity to take a survey (implement as onClick in XML)
	public void takeSurvey(View view) {
		Intent intent = new Intent(this, SHSurveyActivity.class);
		intent.putExtra(SHConstants.SURVEY_ID, surveyID);
		startActivity(intent);
	}
	
	// PRIVATE LOGIC /////////////////////////////////////////////////////////////////
	
	public boolean isDualPane() {
		return isDualPane;
	}
	
	private boolean isDualPaneOrientation() {
		return (findViewById(R.id.sidePanel) != null);
	}
	
	private void updateTitle() {
		if(!isDualPane) {
			if(!menuFragment.isVisible()) {
				this.updateTitle(surveyID.toTitleString());
				return;
			}
		} 
		this.updateTitle("Surveys");
	}
	
	private SHActivityState getInitialState() {
		if(findViewById(R.id.sidePanel) != null) {
			return SHActivityState.DUAL_PANE_FROM_HOME;
		} else {
			return SHActivityState.MENU_FROM_HOME;
		}
	}
}

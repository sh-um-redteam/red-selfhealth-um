/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.fragments.SHMedicationHistoryFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHMedicationMenuFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHMedicationMenuFragment.OnMedicationSelectedListener;
import edu.umass.nursing.selfhealth.client.medication.SHMedication;

public class SHMedicationOverviewActivity extends SHActivityWithActionBar implements OnMedicationSelectedListener {

	// Local Logic
	private boolean isDualPane;
	private SHActivityState currentState;
	private FragmentManager fragmentManager;
	private SHMedicationMenuFragment menuFragment;
	private SHMedicationHistoryFragment historyFragment;
	private final String MENU_TAG = "MEDICATION_MENU";
	private final String HISTORY_TAG = "MEDICATION_HISTORY";
	private OnViewMedicationHistorListener viewMedicationHistoryListener;
		
	public interface OnViewMedicationHistorListener {
		public void onViewMedicationHistory(SHMedication medication);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
    	if(isDualPane) {
    		fragmentManager.putFragment(outState, HISTORY_TAG, historyFragment);
    		fragmentManager.putFragment(outState, MENU_TAG, menuFragment);
    	} else if(currentState == SHActivityState.MENU_FROM_HOME || currentState == SHActivityState.MENU_FROM_CONTENT){
    		fragmentManager.putFragment(outState, MENU_TAG, menuFragment);
    	} else {
    		fragmentManager.putFragment(outState, HISTORY_TAG, historyFragment);
    	}
		outState.putSerializable("PREVIOUS_STATE", currentState);
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, "Medications");
        setContentView(R.layout.sh_activity_dual_pane);
        
        this.isDualPane 		= isDualPaneOrientation();
        this.fragmentManager	= getFragmentManager();
		
        if(savedInstanceState == null) {
        	
        	this.currentState = getInitialState();
        	
        	menuFragment 	= new SHMedicationMenuFragment();
    		historyFragment = new SHMedicationHistoryFragment();
    		
        	FragmentTransaction transaction = fragmentManager.beginTransaction();
        	
        	if(isDualPane) {
        		transaction.add(R.id.sidePanel, menuFragment, MENU_TAG);
        		transaction.add(R.id.contentPanel, historyFragment, HISTORY_TAG);
        	} else {
        		transaction.add(R.id.contentPanel, menuFragment, MENU_TAG);
        	}
            transaction.commit();
            
        } else {
        	
        	this.currentState 	= SHActivityState.getCurrentState((SHActivityState) savedInstanceState.get("PREVIOUS_STATE"), isDualPane);
        	
        	historyFragment = (SHMedicationHistoryFragment) fragmentManager.getFragment(savedInstanceState, HISTORY_TAG);
        	menuFragment	= (SHMedicationMenuFragment) fragmentManager.getFragment(savedInstanceState, MENU_TAG);
        	
        	if(historyFragment == null)	historyFragment = new SHMedicationHistoryFragment();
        	if(menuFragment == null) menuFragment = new SHMedicationMenuFragment();
        	
        	switch(currentState) {
	        	case DUAL_PANE_FROM_MENU:
	        		fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	        		fragmentManager.beginTransaction().remove(menuFragment).commit();
	        		fragmentManager.executePendingTransactions();
	        		fragmentManager.beginTransaction()
	        		    .replace(R.id.sidePanel, menuFragment, MENU_TAG)
	        		    .add(R.id.contentPanel, historyFragment, HISTORY_TAG)
	        		    .commit();              
	        		break;
	        	case DUAL_PANE_FROM_CONTENT:
	        		fragmentManager.beginTransaction().replace(R.id.sidePanel, menuFragment, MENU_TAG).commit();
	        		break;
	        	case MENU_FROM_CONTENT:
	        		fragmentManager.beginTransaction().replace(R.id.contentPanel, menuFragment, MENU_TAG).commit();
	        		break;
	        	case CONTENT_FROM_MENU:
	        		fragmentManager.beginTransaction().replace(R.id.contentPanel, historyFragment, HISTORY_TAG).commit();
	        		break;
	        	default:
	        		break;
        	}
        }
        viewMedicationHistoryListener = historyFragment;
	}
	
	@Override
	// Preserves the BackStack and ActivtyState transition -- injects fragments into the stack when necessary
	public void onBackPressed() {
		if(currentState == SHActivityState.CONTENT_FROM_DUAL_PANE) {
			
			currentState = SHActivityState.getCurrentState(currentState, isDualPane);
			
			fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    		fragmentManager.beginTransaction().remove(menuFragment).commit();
    		fragmentManager.executePendingTransactions();
    		fragmentManager.beginTransaction().replace(R.id.contentPanel, menuFragment, MENU_TAG).commit();              
		} else {
	        super.onBackPressed();
	    }
	}
	
	@Override
	public void onMedicationSelected(SHMedication medication) {
		
		if(currentState == SHActivityState.MENU_FROM_HOME || currentState == SHActivityState.MENU_FROM_CONTENT) {
			
			if(historyFragment == null) {
				historyFragment = new SHMedicationHistoryFragment();
        		viewMedicationHistoryListener = historyFragment;
			}
			fragmentManager.beginTransaction().replace(R.id.contentPanel, historyFragment).addToBackStack(null).commit();
			fragmentManager.executePendingTransactions();
		}
		viewMedicationHistoryListener.onViewMedicationHistory(medication);
	}
	
	public boolean isDualPane() {
		return isDualPane;
	}
	
	private boolean isDualPaneOrientation() {
		return (findViewById(R.id.sidePanel) != null);
	}
	
	private SHActivityState getInitialState() {
		if(findViewById(R.id.sidePanel) != null) {
			return SHActivityState.DUAL_PANE_FROM_HOME;
		} else {
			return SHActivityState.MENU_FROM_HOME;
		}
	}
}

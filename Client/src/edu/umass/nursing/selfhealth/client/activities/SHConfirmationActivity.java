/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import org.joda.time.LocalDateTime;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;

import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.fragments.SHConfirmationFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHConfirmationFragment.OnConfirmationListener;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;
import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.records.SHConfirmableHealthMetricRecord;
import edu.umass.nursing.selfhealth.client.records.SHMedicationRecord;

public class SHConfirmationActivity extends SHActivityWithActionBar implements OnConfirmationListener {

	private SHMedication medication;
	private SHHealthMetricID healthMetricID;
	
	private FragmentManager fragmentManager;
	private SHConfirmationFragment confirmationFragment;
	private OnSetupPaneInformationListener listener;
	
	public interface OnSetupPaneInformationListener {
		public void onSetupPaneInformation(String prompt, String title, String subtitle);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		fragmentManager.putFragment(outState, "CONFIRMATION", fragmentManager.findFragmentById(R.id.contentPanel));
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, "Confirmation");
        setContentView(R.layout.sh_activity_single_pane);
        
        fragmentManager = getFragmentManager();
        
        if(savedInstanceState == null) {
			confirmationFragment = new SHConfirmationFragment();
        	fragmentManager.beginTransaction().add(R.id.contentPanel, confirmationFragment, "CONFIRMATION").commit();
        } else {
        	confirmationFragment = (SHConfirmationFragment) fragmentManager.getFragment(savedInstanceState, "CONFIRMATION");
        }
        
        this.medication = (SHMedication) getIntent().getSerializableExtra(SHConstants.MEDICATION);
        this.healthMetricID = (SHHealthMetricID) getIntent().getSerializableExtra(SHConstants.HEALTH_METRIC_ID);
        
        listener = confirmationFragment;
	}
	
	@Override
	public void onStart(){
		super.onStart();
		updatePanel();
	}
	
	private void updatePanel() {
		
		if(healthMetricID != null) {
			listener.onSetupPaneInformation("Confirm Your...", healthMetricID.toString(), "");
		} else {
			listener.onSetupPaneInformation("Confirm Taking...", medication.getName(), medication.getDetails());
		}
	}
	
	@Override
	public void onConfirm() {
		
		Integer scheduleID = (Integer) getIntent().getExtras().getSerializable(SHConstants.SCHEDULE_ID);
			
		int completionID;
		if(healthMetricID != null) {
			completionID = SHController.getInstance(this).saveHealthMetricRecord(new SHConfirmableHealthMetricRecord(healthMetricID, LocalDateTime.now()), scheduleID);
		} else {
			completionID = SHController.getInstance(this).saveMedicationRecord(new SHMedicationRecord(medication.getID(), LocalDateTime.now()), scheduleID);
		}
			
		if(scheduleID != null) {
    		setResult(RESULT_OK, new Intent().putExtra(SHConstants.COMPLETION_ID, completionID));
    	}
		this.finish();
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.fragments.SHHealthHistoryFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHHealthMenuFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHHealthMenuFragment.OnHealthToolSelectedListener;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;

public class SHHealthOverviewActivity extends SHActivityWithActionBar implements OnHealthToolSelectedListener {

	// Local Logic
	private boolean isDualPane;
	private SHActivityState currentState;
	private SHHealthMetricID healthMetricID;
	
	private FragmentManager fragmentManager;
	private SHHealthMenuFragment menuFragment;
	private SHHealthHistoryFragment historyFragment;
	private final String MENU_TAG = "MENU";
	private final String HISTORY_TAG = "HISTORY";
	private OnViewHealthHistoryListener viewHealthHistoryListener;
		
	public interface OnViewHealthHistoryListener {
		public void onViewHealthHistory(SHHealthMetricID healthMetricID);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
    	if(isDualPane) {
    		fragmentManager.putFragment(outState, HISTORY_TAG, historyFragment);
    		fragmentManager.putFragment(outState, MENU_TAG, menuFragment);
    	} else if(currentState == SHActivityState.MENU_FROM_HOME || currentState == SHActivityState.MENU_FROM_CONTENT){
    		fragmentManager.putFragment(outState, MENU_TAG, menuFragment);
    	} else {
    		fragmentManager.putFragment(outState, HISTORY_TAG, historyFragment);
    	}
		outState.putSerializable("PREVIOUS_STATE", currentState);
		outState.putSerializable("HEALTH_METRIC_ID", healthMetricID);
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, "Health");
        setContentView(R.layout.sh_activity_dual_pane);
        
        this.isDualPane 		= isDualPaneOrientation();
        this.fragmentManager	= getFragmentManager();
		
        if(savedInstanceState == null) {
        	
        	this.currentState = getInitialState();
        	
        	menuFragment 	= new SHHealthMenuFragment();
    		historyFragment = new SHHealthHistoryFragment();
    		
        	FragmentTransaction transaction = fragmentManager.beginTransaction();
        	
        	if(isDualPane) {
        		transaction.add(R.id.sidePanel, menuFragment, MENU_TAG);
        		transaction.add(R.id.contentPanel, historyFragment, HISTORY_TAG);
        	} else {
        		transaction.add(R.id.contentPanel, menuFragment, MENU_TAG);
        	}
            transaction.commit();
            
        } else {
        	
        	this.currentState 	= SHActivityState.getCurrentState((SHActivityState) savedInstanceState.get("PREVIOUS_STATE"), isDualPane);
        	this.healthMetricID	= (SHHealthMetricID) savedInstanceState.get("HEALTH_METRIC_ID");
        	
        	historyFragment = (SHHealthHistoryFragment) fragmentManager.getFragment(savedInstanceState, HISTORY_TAG);
        	menuFragment	= (SHHealthMenuFragment) fragmentManager.getFragment(savedInstanceState, MENU_TAG);
        	
        	if(historyFragment == null)	historyFragment = new SHHealthHistoryFragment();
        	if(menuFragment == null) menuFragment = new SHHealthMenuFragment();
        	
        	switch(currentState) {
	        	case DUAL_PANE_FROM_MENU:
	        		fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	        		fragmentManager.beginTransaction().remove(menuFragment).commit();
	        		fragmentManager.executePendingTransactions();
	        		fragmentManager.beginTransaction()
	        		    .replace(R.id.sidePanel, menuFragment, MENU_TAG)
	        		    .add(R.id.contentPanel, historyFragment, HISTORY_TAG)
	        		    .commit();              
	        		break;
	        	case DUAL_PANE_FROM_CONTENT:
	        		fragmentManager.beginTransaction().replace(R.id.sidePanel, menuFragment, MENU_TAG).commit();
	        		break;
	        	case MENU_FROM_CONTENT:
	        		fragmentManager.beginTransaction().replace(R.id.contentPanel, menuFragment, MENU_TAG).commit();
	        		break;
	        	case CONTENT_FROM_MENU:
	        		fragmentManager.beginTransaction().replace(R.id.contentPanel, historyFragment, HISTORY_TAG).commit();
	        		break;
	        	default:
	        		break;
        	}
        	updateTitle();
        }
        viewHealthHistoryListener = historyFragment;
	}
	
	@Override
	// Preserves the BackStack and ActivtyState transition -- injects fragments into the stack when necessary
	public void onBackPressed() {
		if(currentState == SHActivityState.CONTENT_FROM_DUAL_PANE) {
			
			currentState = SHActivityState.getCurrentState(currentState, isDualPane);
			
			fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    		fragmentManager.beginTransaction().remove(menuFragment).commit();
    		fragmentManager.executePendingTransactions();
    		fragmentManager.beginTransaction().replace(R.id.contentPanel, menuFragment, MENU_TAG).commit(); 
    		fragmentManager.executePendingTransactions();
		} else {
	        super.onBackPressed();
	    }
		updateTitle();
	}
	
	@Override
    public void onHealthToolSelected(SHHealthMetricID healthMetricID1) {
		
		if(currentState == SHActivityState.MENU_FROM_HOME || currentState == SHActivityState.MENU_FROM_CONTENT) {
			
			if(historyFragment == null) {
				historyFragment = new SHHealthHistoryFragment();
        		viewHealthHistoryListener = historyFragment;
			}
			fragmentManager.beginTransaction().replace(R.id.contentPanel, historyFragment).addToBackStack(null).commit();
			fragmentManager.executePendingTransactions();
		}
		this.healthMetricID = healthMetricID1;
		viewHealthHistoryListener.onViewHealthHistory(healthMetricID1);
		updateTitle();
	}
	
	// OnClick Listener -- Transition Activity to record health (implement as onClick in XML)
	public void recordHealth(View view) {
		
		// TODO : Use the SelfManagementTool to delegate the intent
		
		Intent intent; 
		if(this.healthMetricID.isConfirmable()) {
			intent = new Intent(this, SHConfirmationActivity.class);
		} else {
			
			intent = new Intent(this, SHMeasurementActivity.class);
//			intent = new Intent(this, TakeMeasurementActivity.class);
		}
		intent.putExtra(SHConstants.HEALTH_METRIC_ID, healthMetricID);
		startActivity(intent);
	}
	
	public boolean isDualPane() {
		return isDualPane;
	}
	
	private void updateTitle() {
		if(!isDualPane) {
			if(!menuFragment.isVisible()) {
				this.updateTitle(healthMetricID.toString());
				return;
			}
		} 
		this.updateTitle("Health");
	}
	
	private boolean isDualPaneOrientation() {
		return (findViewById(R.id.sidePanel) != null);
	}
	
	private SHActivityState getInitialState() {
		if(findViewById(R.id.sidePanel) != null) {
			return SHActivityState.DUAL_PANE_FROM_HOME;
		} else {
			return SHActivityState.MENU_FROM_HOME;
		}
	}
}

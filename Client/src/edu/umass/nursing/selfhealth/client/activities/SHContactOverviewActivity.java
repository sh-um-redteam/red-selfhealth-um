/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.activities;

import java.util.ArrayList;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import edu.umass.nursing.selfhealth.client.R;
import edu.umass.nursing.selfhealth.client.application.SHConstants;
import edu.umass.nursing.selfhealth.client.controller.SHController;
import edu.umass.nursing.selfhealth.client.fragments.SHHomeMenuFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHHomeMenuFragment.OnScheduleViewToggledListener;
import edu.umass.nursing.selfhealth.client.fragments.SHScheduleFragment;
import edu.umass.nursing.selfhealth.client.fragments.SHScheduleFragment.OnScheduleItemSelectedListener;
import edu.umass.nursing.selfhealth.client.patient.SHPatient;
import edu.umass.nursing.selfhealth.client.schedule.SHSchedule;
import edu.umass.nursing.selfhealth.client.schedule.SHSchedule.OnScheduleUpdatedListener;
import edu.umass.nursing.selfhealth.client.schedule.SHScheduleItem;

public class SHContactOverviewActivity extends SHActivityWithActionBar implements OnScheduleItemSelectedListener, OnScheduleUpdatedListener, OnScheduleViewToggledListener {

	// Datasource
	private static SHPatient patient;
	private static SHSchedule shSchedule;
	private ArrayList<SHScheduleItem> scheduledEvents = new ArrayList<SHScheduleItem>();
	private ArrayList<SHScheduleItem> completedEvents = new ArrayList<SHScheduleItem>();

	@Override
	protected void onStart() {
    	super.onStart();
    }

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, "Contacts");
        setContentView(R.layout.sh_activity_home);
        
        SHController.getInstance(this).loadConfiguration();
        
        patient = SHController.getInstance(this).getPatient();
        setupHomeActionBar(patient.getFirstName() + "'s Contacts");
        
        //shSchedule = SHSchedule.getInstance(this);
        //shSchedule.registerOnScheduleUpdatedListener(this);
		
		FragmentManager fragmentManager = getFragmentManager();
        
        SHScheduleFragment scheduleFragment;
        SHHomeMenuFragment menuFragment;
        
	}

	@Override
	public void viewSchedule() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void viewCompleted() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onScheduleUpdated() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onScheduleItemSelected(int scheduleItemIndex) {
		// TODO Auto-generated method stub
		
	}
	
}

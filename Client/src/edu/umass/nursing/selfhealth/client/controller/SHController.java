/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import edu.umass.nursing.selfhealth.client.application.SHDay;
import edu.umass.nursing.selfhealth.client.configuration.SHConfiguration;
import edu.umass.nursing.selfhealth.client.configuration.SHConfigurationLoader;
import edu.umass.nursing.selfhealth.client.configuration.SHMedicationConfiguration;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricComponent;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;
import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.medication.SHMedicationID;
import edu.umass.nursing.selfhealth.client.patient.SHPatient;
import edu.umass.nursing.selfhealth.client.records.SHConfirmableHealthMetricRecord;
import edu.umass.nursing.selfhealth.client.records.SHHealthMetricRecord;
import edu.umass.nursing.selfhealth.client.records.SHMeasurableHealthMetricRecord;
import edu.umass.nursing.selfhealth.client.records.SHMedicationRecord;
import edu.umass.nursing.selfhealth.client.records.SHRecord;
import edu.umass.nursing.selfhealth.client.records.SHRecordQueryInterval;
import edu.umass.nursing.selfhealth.client.records.SHSurveyRecord;
import edu.umass.nursing.selfhealth.client.schedule.SHScheduleItem;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyID;
import edu.umass.nursing.selfhealth.client.tools.SHConfirmableHealthMetricTool;
import edu.umass.nursing.selfhealth.client.tools.SHHealthMetricTool;
import edu.umass.nursing.selfhealth.client.tools.SHMeasurableHealthMetricTool;
import edu.umass.nursing.selfhealth.client.tools.SHMedicationTool;
import edu.umass.nursing.selfhealth.client.tools.SHSelfManagementTool;
import edu.umass.nursing.selfhealth.client.tools.SHSelfManagementToolCategory;
import edu.umass.nursing.selfhealth.client.tools.SHSurveyTool;

/**
 * 	Access point for Database-dependent queries.
 */
public class SHController extends SQLiteAssetHelper implements SHControllerInterface {
	
// @INITIALIZATION
// =============================================================================================
	
	private static SHController instance = null;
	
	public static synchronized SHController getInstance(Context c) {
		if (instance == null)
			instance = new SHController(c.getApplicationContext());
		return instance;
	}
	
	// Database Properties
	
	// The filename of the database that the system looks for in the 'assets' folder
	private static final String DATABASE_NAME = "SHDatabase.sqlite";
	
	private static final int DATABASE_VERSION = 1;
	private static SQLiteDatabase database;

	/**
	 * The SQLiteAssetHelper library is used to avoid the hassle of creating the database in code here.
	 * An SQLite database file created in an external program should be placed in the 'assets' folder
	 * so that SQLiteAssetHelper can read/write to it through query executions.
	 */
	private SHController(Context c) {
		super(c, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public SQLiteDatabase getDatabase() {
		return database;
	}
	
	// Database must be opened for each set of operations 
	private void openDatabase() {
		if(database == null) {
			database = this.getWritableDatabase();
		} 
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }

// @TABLE_DEFINITIONS
// =============================================================================================
			
	@SuppressWarnings("unused")
	private static final String FALSE						= "0";
	private static final String TRUE						= "1";
	
	private static final String VERSION_TABLE 				= "CONFIGURATION_VERSION";	// Integer
	   
	   private static final String CONFIGURATION_VERSION 	= "configuration_version";	// Integer
	   
    private static final String PATIENT_TABLE 				= "PATIENT";
	   
	   private static final String PATIENT_ID 				= "patient_id";				// Integer
	   private static final String FIRST_NAME 				= "first_name";				// String
	   private static final String LAST_NAME 				= "last_name";				// String
	   private static final String DATE_OF_BIRTH 			= "date_of_birth";			// DateTime
	   private static final String GENDER 					= "gender";					// String
	
	private static final String CONFIGURED_TOOLS_TABLE 		= "CONFIGURED_TOOLS";
	
		private static final String CONFIGURATION_ID 		= "configuration_id";		// Integer
		private static final String TOOL_CATEGORY 			= "tool_category";			// String
		private static final String TOOL_TYPE 				= "tool_type";				// String
		private static final String ACTIVE 					= "active";					// Boolean
	
	private static final String SCHEDULE_TABLE 				= "SCHEDULE";
	
		private static final String EVERY_X_WEEKS 			= "every_x_weeks";			// Integer
		private static final String START_WEEK_INDEX 		= "start_week_index";		// Integer
		private static final String MONDAY 					= "monday";					// Boolean
		private static final String TUESDAY 				= "tuesday";				// Boolean
		private static final String WEDNESDAY 				= "wednesday";				// Boolean
		private static final String THURSDAY 				= "thursday";				// Boolean
		private static final String FRIDAY 					= "friday";					// Boolean
		private static final String SATURDAY 				= "saturday";				// Boolean
		private static final String SUNDAY 					= "sunday";					// Boolean
		
	private static final String SCHEDULE_TIMES_TABLE 		= "SCHEDULE_TIMES";
	
		private static final String SCHEDULE_ID 			= "schedule_id";			// Integer
		private static final String HOUR 					= "hour";					// Integer
		private static final String MINUTE 					= "minute";					// Integer
	
	private static final String MEDICATIONS_TABLE 			= "MEDICATIONS";
	
		private static final String NAME 					= "name";					// String
		private static final String FORM 					= "form";					// String
		private static final String DOSAGE 					= "dosage";					// Double
		private static final String UNITS 					= "units";					// String
		private static final String QUANTITY 				= "quantity";				// Double
	
	private static final String COMPLETED_TABLE 			= "COMPLETED";
	
		private static final String COMPLETION_ID 			= "completion_id";			// Integer
		private static final String COMPLETION_DATE_TIME 	= "completion_date_time";	//DateTime
	
	private static final String SURVEY_RESPONSES_TABLE 		= "SURVEY_RESPONSES";
	
		private static final String RESPONSE_MAP 			= "response_map";			// String
	
	private static final String MEASUREMENTS_TABLE	 		= "MEASUREMENTS";

		private static final String MEASUREMENT_MAP 		= "measurement_map";		// String

// @CONFIGURATION
// =============================================================================================
		
	@Override
	public void loadConfiguration() {
		SHConfigurationLoader.loadConfiguration();
	}
	
	@Override
	public boolean updateConfigurationVersion(int newConfigurationVersion) {
		
		String query = "SELECT * FROM " + VERSION_TABLE + " WHERE " + CONFIGURATION_VERSION + "=" + newConfigurationVersion;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
		
		if (c.moveToFirst()) {
			// The existing version is the same as the new version
			c.close();
			return false;
		} else {
			// The existing version is outdated--replace it with the new version
			ContentValues values = new ContentValues();
			values.put(CONFIGURATION_VERSION, newConfigurationVersion);
			database.replace(VERSION_TABLE, null, values);
		}
		c.close();
		return true;
	}
	
	@Override
	public int getConfigurationVersion() {
		
		String query = "SELECT " + CONFIGURATION_VERSION + " FROM " + VERSION_TABLE;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
		
		if (c.moveToFirst()) {
			
			int version = c.getInt(c.getColumnIndex(CONFIGURATION_VERSION));
			c.close();
			return version;
		} else {
			// No version yet exists, return the appropriate constant value
			c.close();
			return SHConfigurationLoader.NO_CONFIGURATION_VERSION;
		}
	}
	
	@Override
	public void destoryAllDemoData() {
		
		String[] tables = {VERSION_TABLE,  PATIENT_TABLE ,CONFIGURED_TOOLS_TABLE, SCHEDULE_TABLE,
				SCHEDULE_TIMES_TABLE, MEDICATIONS_TABLE, COMPLETED_TABLE, SURVEY_RESPONSES_TABLE,
				MEASUREMENTS_TABLE};
		
		openDatabase();
		for(String table : tables) {
			database.delete(table, null, null);
		}
	}

// @PATIENT
// =============================================================================================

	@Override
	public void updatePatient(SHPatient patient) {
		
		ContentValues values = new ContentValues();
		values.put(PATIENT_ID, 			patient.getId());
		values.put(FIRST_NAME, 			patient.getFirstName());
		values.put(LAST_NAME, 			patient.getLastName());
		values.put(DATE_OF_BIRTH, 		toDateForSQLite(patient.getDateOfBirth()));
		values.put(GENDER, 				patient.getGender());
		
		openDatabase();
		database.replace(PATIENT_TABLE, null, values);
	}

	@Override
	public SHPatient getPatient() {
		
		String query = "SELECT * FROM " + PATIENT_TABLE;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
		
		if (c.moveToFirst()) {
			int patientID 			= c.getInt(c.getColumnIndex(PATIENT_ID));
			String firstName 		= c.getString(c.getColumnIndex(FIRST_NAME));
			String lastName 		= c.getString(c.getColumnIndex(FIRST_NAME));
			String gender 			= c.getString(c.getColumnIndex(FIRST_NAME));
			LocalDate dateOfBirth	= getDateFromSQLite(c.getString(c.getColumnIndex(DATE_OF_BIRTH)));
			
			c.close();
			return new SHPatient(patientID, firstName, lastName, gender, dateOfBirth);
		}
		c.close();
		return null;
	}
	
// @EXPORT
// =============================================================================================

	@Override
	public void exportPatientData() {
		
		String[] tables = {VERSION_TABLE,  PATIENT_TABLE ,CONFIGURED_TOOLS_TABLE, SCHEDULE_TABLE,
				SCHEDULE_TIMES_TABLE, MEDICATIONS_TABLE, COMPLETED_TABLE, SURVEY_RESPONSES_TABLE,
				MEASUREMENTS_TABLE};
		
	    File externalStorage = new File(Environment.getExternalStorageDirectory()+"/../extSdCard/lumos/");
	    if (!externalStorage.exists()) {
	    	externalStorage = new File(Environment.getExternalStorageDirectory(), "lumos");
	    }
	    String outDir = externalStorage.getAbsolutePath();
	    for (String table : tables) {
	    	externalStorage = new File(outDir, table + ".csv");
	        try {
	            String csv = toCSV(table);
	            if (csv != null) {
	                PrintWriter p = new PrintWriter(new FileWriter(externalStorage));
	                p.println(csv);
	                p.close();
	            }
	        } catch (IOException e) { }
	    }
	}
		
// @RECORDS
// =============================================================================================
	
	// ######################################## RETRIEVE ########################################
	
	@Override
	public ArrayList<SHHealthMetricRecord> getHealthMetricRecords(
			SHHealthMetricID healthMetricID, SHRecordQueryInterval interval) {
		
		ArrayList<SHHealthMetricRecord> healthMetricRecords = new ArrayList<SHHealthMetricRecord>();
		
		if(healthMetricID.isConfirmable()) {
			healthMetricRecords.addAll(getConfirmableHealthMetricRecords(healthMetricID, interval));
		} else {
			healthMetricRecords.addAll(getMeasurableHealthMetricRecords(healthMetricID, interval));
		}
		
		// Return the records ordered from most recent completion to oldest completion dates
		Collections.reverse(healthMetricRecords);
        return healthMetricRecords;
	}
	
	/**
	 * Searches for and returns all confirmable health metric records matching the specified criteria
	 */
	private ArrayList<SHConfirmableHealthMetricRecord> getConfirmableHealthMetricRecords(
			SHHealthMetricID healthMetricID, SHRecordQueryInterval interval) {
		
		ArrayList<SHConfirmableHealthMetricRecord> confirmableHealthMetricRecords = new ArrayList<SHConfirmableHealthMetricRecord>();
		
		String whereClause 	= 	intervalClause(
				COMPLETED_TABLE + "." + CONFIGURATION_ID + " = " + getConfigurationID(healthMetricID.toString()), 
				COMPLETED_TABLE + "." + COMPLETION_DATE_TIME, interval);
		
		String complimentClause = 	" (SELECT " + COMPLETION_ID + " FROM " + MEASUREMENTS_TABLE + 
									" WHERE " + COMPLETED_TABLE + "." + COMPLETION_ID + " = " + MEASUREMENTS_TABLE + "." + COMPLETION_ID + " )";
		
		// Selects health metric records that are confirmable--eg: they do not contain a measurement map
		String query = "SELECT " + COMPLETION_DATE_TIME + " FROM " + COMPLETED_TABLE + " WHERE " + whereClause + " AND NOT EXISTS " + complimentClause;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
		while(c.moveToNext()) {
			
			// Add confirmable records to the list
			LocalDateTime dateTimeCompleted =  getDateTimeFromSQLite(c.getString(c.getColumnIndex(COMPLETION_DATE_TIME)));
			confirmableHealthMetricRecords.add(new SHConfirmableHealthMetricRecord(healthMetricID, dateTimeCompleted));
		}
		
		c.close();
		return confirmableHealthMetricRecords;
	}
	
	/**
	 * Searches for and returns all measurable health metric records matching the specified criteria
	 */
	private ArrayList<SHMeasurableHealthMetricRecord> getMeasurableHealthMetricRecords(
			SHHealthMetricID healthMetricID, SHRecordQueryInterval interval) {
		
		ArrayList<SHMeasurableHealthMetricRecord> measurableHealthMetricRecords = new ArrayList<SHMeasurableHealthMetricRecord>();
		
		String whereClause 	= 	intervalClause(
				COMPLETED_TABLE + "." + CONFIGURATION_ID + " = " + getConfigurationID(healthMetricID.toString()), 
				COMPLETED_TABLE + "." + COMPLETION_DATE_TIME, interval);
		
		String joinClause 	= 	MEASUREMENTS_TABLE + 
								" ON " + COMPLETED_TABLE + "." + COMPLETION_ID + " = " +  MEASUREMENTS_TABLE + "." + COMPLETION_ID;
		
		// Selects health metric records that are measurable--eg: they contain a measurement map
		String query 		= 	"SELECT " + COMPLETED_TABLE + "." + COMPLETION_DATE_TIME + ", " + MEASUREMENTS_TABLE + "." + MEASUREMENT_MAP + 
								" FROM " + COMPLETED_TABLE + " INNER JOIN " + joinClause + " WHERE " + whereClause;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
        while(c.moveToNext()) {
        	
        	// Add measurable health metric records to the list
            LocalDateTime dateTimeCompleted =  getDateTimeFromSQLite(c.getString(c.getColumnIndex(COMPLETION_DATE_TIME)));
            HashMap<SHHealthMetricComponent, Number> measurementMap = 
            		SHMeasurableHealthMetricRecord.measurementMapFromSQLiteString(c.getString(c.getColumnIndex(MEASUREMENT_MAP)));
            measurableHealthMetricRecords.add(new SHMeasurableHealthMetricRecord(healthMetricID, dateTimeCompleted, measurementMap));
        } 
        
        c.close();
        return measurableHealthMetricRecords;
	}
	
	@Override
	public ArrayList<SHMedicationRecord> getMedicationRecords(
			SHMedicationID medicationID, SHRecordQueryInterval interval) {
		
		ArrayList<SHMedicationRecord> medicationRecords = new ArrayList<SHMedicationRecord>();
		
		String query = 	"SELECT " + COMPLETION_DATE_TIME + " FROM " + COMPLETED_TABLE + " WHERE " +
				intervalClause(CONFIGURATION_ID + "= " + medicationID.value(), COMPLETION_DATE_TIME, interval);
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
        while(c.moveToNext()) {
        	
        	// Add medication records to the list
        	LocalDateTime dateTimeCompleted = getDateTimeFromSQLite(c.getString(c.getColumnIndex(COMPLETION_DATE_TIME)));
        	medicationRecords.add(new SHMedicationRecord(medicationID, dateTimeCompleted));
        } 
        
        c.close();
        Collections.reverse(medicationRecords);
        return medicationRecords;
	}
	
	@Override
	public ArrayList<SHSurveyRecord> getSurveyRecords(SHSurveyID surveyID,
			SHRecordQueryInterval interval) {
		
		ArrayList<SHSurveyRecord> surveyRecords = new ArrayList<SHSurveyRecord>();
		
		String whereClause 	= 	intervalClause(
				COMPLETED_TABLE + "." + CONFIGURATION_ID + " = " + getConfigurationID(surveyID.toString()), 
				COMPLETED_TABLE + "." + COMPLETION_DATE_TIME, interval);
		
		String joinClause 	= 	SURVEY_RESPONSES_TABLE + 
								" ON " + COMPLETED_TABLE + "." + COMPLETION_ID + " = " +  SURVEY_RESPONSES_TABLE + "." + COMPLETION_ID;
		
		String query 		= 	"SELECT " + COMPLETED_TABLE + "." + COMPLETION_DATE_TIME + ", " + SURVEY_RESPONSES_TABLE + "." + RESPONSE_MAP + 
								" FROM " + COMPLETED_TABLE + " INNER JOIN " + joinClause + " WHERE " + whereClause;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
		
        while(c.moveToNext()) {
        	
        	// Add survey records to the list
            LocalDateTime dateTimeCompleted = getDateTimeFromSQLite(c.getString(c.getColumnIndex(COMPLETION_DATE_TIME)));
            HashMap<Integer, Integer> responseMap = 
            		SHSurveyRecord.responseMapFromSQLiteString(c.getString(c.getColumnIndex(RESPONSE_MAP)));
            surveyRecords.add(new SHSurveyRecord(surveyID, responseMap, dateTimeCompleted));
        } 
        
        c.close();
        Collections.reverse(surveyRecords);
        return surveyRecords;
	}
	
	// ########################################## SAVE ##########################################

	@Override
	public int saveHealthMetricRecord(SHHealthMetricRecord record, Integer scheduleID) {
		
		int completionID = getNewCompletionID();
		saveRecordCompletionData(getConfigurationID(record.healthMetricID().toString()), completionID, record.getTimeCompleted(), scheduleID);
		
		if(!record.isConfirmable()) {
		
			// The record is measurable--save the measurement data
			ContentValues values = new ContentValues();
			values = new ContentValues();
			values.put(COMPLETION_ID, 		completionID);
			values.put(MEASUREMENT_MAP, 	SHMeasurableHealthMetricRecord.toSQLiteMeasurementMapString((SHMeasurableHealthMetricRecord) record));
			
			openDatabase();
			database.insert(MEASUREMENTS_TABLE, null, values);
		}
		return completionID;
	}
	
	@Override
	public int saveMedicationRecord(SHMedicationRecord record, Integer scheduleID) {
		
		int completionID = getNewCompletionID();
		saveRecordCompletionData(record.medicationID().value(), completionID, record.getTimeCompleted(), scheduleID);
		return completionID;
	}
	
	@Override
	public int saveSurveyRecord(SHSurveyRecord record, Integer scheduleID) {
		
		int completionID = getNewCompletionID();
		saveRecordCompletionData(getConfigurationID(record.surveyID().toString()), completionID, record.getTimeCompleted(), scheduleID);
		
		ContentValues values = new ContentValues();
		values.put(COMPLETION_ID, 			completionID);
		values.put(RESPONSE_MAP, 			SHSurveyRecord.toSQLiteResponseMapString(record));
		
		openDatabase();
		// Save the survey responses
		database.insert(SURVEY_RESPONSES_TABLE, null, values);
		return completionID;
	}
	
	/**
	 * 	Save standard completion data required in all records
	 */
	private void saveRecordCompletionData(int configurationID, int completionID, LocalDateTime completionsDateTime, Integer scheduleID) {
		
		ContentValues values = new ContentValues();
		values.put(CONFIGURATION_ID, 		configurationID);
		values.put(COMPLETION_ID, 			completionID);
		values.put(COMPLETION_DATE_TIME, 	toDateTimeForSQLite(completionsDateTime));
		values.put(SCHEDULE_ID, 			scheduleID);
		
		openDatabase();
		database.insert(COMPLETED_TABLE, null, values);
	}
	
// @MEDICATION_LOOKUP
// =============================================================================================

	@Override
	public SHMedication getMedication(SHMedicationID id) {
		
		openDatabase();
        Cursor c = database.query(
        		MEDICATIONS_TABLE, 
        		new String[]{NAME, FORM, DOSAGE, UNITS, QUANTITY}, 
        		CONFIGURATION_ID + "= ?",
        		new String[]{Integer.toString(id.value())},
        		null, null, null);
        
        if(c.moveToNext()) {
        	
            String name 	= c.getString(c.getColumnIndex(NAME));
            String form		= c.getString(c.getColumnIndex(FORM));
            String units	= c.getString(c.getColumnIndex(UNITS));
            Double dosage 	= c.getDouble(c.getColumnIndex(DOSAGE));
            Double quantity = c.getDouble(c.getColumnIndex(QUANTITY));
            
            c.close(); 
            return new SHMedication(id, name, form, units, dosage, quantity);
        } 
        c.close(); 
    	return null;
	}
	
	@Override
	public ArrayList<SHMedication> getActiveMedications() {
		
		ArrayList<SHMedication> medications = new ArrayList<SHMedication>();
		
		String query = "SELECT " + 	
				MEDICATIONS_TABLE + "." + CONFIGURATION_ID + ", " +
				MEDICATIONS_TABLE + "." + NAME + ", " +
				MEDICATIONS_TABLE + "." + FORM + ", " +
				MEDICATIONS_TABLE + "." + DOSAGE + ", " +
				MEDICATIONS_TABLE + "." + UNITS + ", " +
				MEDICATIONS_TABLE + "." + QUANTITY + 
				" FROM " + MEDICATIONS_TABLE +
				" INNER JOIN " + CONFIGURED_TOOLS_TABLE + 
				" ON " + CONFIGURED_TOOLS_TABLE + "." + CONFIGURATION_ID + " = " +  MEDICATIONS_TABLE  + "." + CONFIGURATION_ID +
				" WHERE " + CONFIGURED_TOOLS_TABLE + "." + ACTIVE + " = " + TRUE;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
		
        while(c.moveToNext()) {
        	
        	SHMedicationID id	= new SHMedicationID(c.getInt(c.getColumnIndex(CONFIGURATION_ID)));
            String name 	= c.getString(c.getColumnIndex(NAME));
            String form		= c.getString(c.getColumnIndex(FORM));
            String units	= c.getString(c.getColumnIndex(UNITS));
            Double dosage 	= c.getDouble(c.getColumnIndex(DOSAGE));
            Double quantity = c.getDouble(c.getColumnIndex(QUANTITY));
            
            SHMedication medication = new SHMedication(id, name, form, units, dosage, quantity);
            medications.add(medication);
        } 
        
        c.close();
        return medications;
	}
	
// @TOOLS
// =============================================================================================
	
	// ######################################## RETRIEVE ########################################
	
	@Override
	public ArrayList<SHSelfManagementToolCategory> getActiveToolCategories() {
		
		ArrayList<SHSelfManagementToolCategory> categories = new ArrayList<SHSelfManagementToolCategory>();
		String query = "SELECT " + CONFIGURATION_ID + " FROM " + CONFIGURED_TOOLS_TABLE + " WHERE " + TOOL_CATEGORY + " =? AND " + ACTIVE + " =?";
		
		openDatabase();
		
		// SURVEY
		Cursor c = database.rawQuery(query, new String[]{SHSelfManagementToolCategory.SURVEY.toString(), TRUE});
		if(c.getCount() > 0) categories.add(SHSelfManagementToolCategory.SURVEY);
		c.close();
		// MEDICATION
		c = database.rawQuery(query, new String[]{SHSelfManagementToolCategory.MEDICATION.toString(), TRUE});
		if(c.getCount() > 0) categories.add(SHSelfManagementToolCategory.MEDICATION);
		c.close();
		// HEALTH
		c = database.rawQuery(query, new String[]{SHSelfManagementToolCategory.HEALTH_METRIC.toString(), TRUE});
		if(c.getCount() > 0) categories.add(SHSelfManagementToolCategory.HEALTH_METRIC);
		
		c.close();
		return categories;
	}

	@Override
	public ArrayList<SHSurveyTool> getActiveSurveyTools() {
		
		ArrayList<SHSurveyTool> tools = new ArrayList<SHSurveyTool>();
		
		openDatabase();
		Cursor c = database.query(
				CONFIGURED_TOOLS_TABLE, 
        		new String[]{TOOL_TYPE}, 
        		TOOL_CATEGORY + " =? AND " + ACTIVE + " =?", 
        		new String[]{SHSelfManagementToolCategory.SURVEY.toString(), TRUE}, 
        		null, null, null);
        
        while(c.moveToNext()) {
        	// Add the survey tool to the list
        	SHSurveyTool tool = new SHSurveyTool(SHSurveyID.fromString(c.getString(c.getColumnIndex(TOOL_TYPE))));
            tools.add(tool);
        } 
        
        c.close();
		return tools;
	}

	@Override
	public ArrayList<SHHealthMetricTool> getActiveHealthMetricTools() {
		
		ArrayList<SHHealthMetricTool> tools = new ArrayList<SHHealthMetricTool> ();
		
		openDatabase();
		Cursor c = database.query(
				CONFIGURED_TOOLS_TABLE, 
        		new String[]{TOOL_TYPE}, 
        		TOOL_CATEGORY + " =? AND " + ACTIVE + " =?", 
        		new String[]{SHSelfManagementToolCategory.HEALTH_METRIC.toString(), TRUE}, 
        		null, null, null);
		
        while(c.moveToNext()) {
        	// Add the appropriate type of health metric tool to the list
        	SHHealthMetricID id = SHHealthMetricID.fromString(c.getString(c.getColumnIndex(TOOL_TYPE)));
        	if(id.isConfirmable()) {
        		tools.add(new SHConfirmableHealthMetricTool(id));
        	} else {
        		tools.add(new SHMeasurableHealthMetricTool(id));
        	}
        } 
        
        c.close();
		return tools;
	}
	
	// ########################################## SAVE ##########################################
	
	@Override
	public void updateToolConfigurations(ArrayList<SHConfiguration> configurations) {
		
		/*
		 * Collect the schedule IDs that should not be removed from the database--remember, some reminders already set
		 * in the system rely on existing IDs. We cannot simply remove them all and add new ones.
		 */
		ArrayList<Integer> scheduleIDsToKeep = new ArrayList<Integer>();
					
		openDatabase();
		for(SHConfiguration configuration : configurations) {
		
			// Update the Configured Tools table ////////////////////////////////////////
			
			ContentValues values = new ContentValues();
			values.put(CONFIGURATION_ID, 	configuration.getConfigurationID());
			values.put(TOOL_CATEGORY, 		configuration.getToolCategory());
			values.put(TOOL_TYPE, 			configuration.getToolType());
			values.put(ACTIVE, 				configuration.isActive());
			
			// Insert the configuration for the specified ID, or replace the values if the ID already exists
			database.insertWithOnConflict(CONFIGURED_TOOLS_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
			
			if(configuration.getToolCategory().equals(SHSelfManagementToolCategory.MEDICATION.toString())) {
				
				saveMedication(configuration.getConfigurationID(), ((SHMedicationConfiguration) configuration).getMedication());
			}
			
			// Update the SHSchedule table ////////////////////////////////////////
			
			values = new ContentValues();
			values.put(CONFIGURATION_ID, 	configuration.getConfigurationID());
			values.put(EVERY_X_WEEKS, 		configuration.getEveryXWeeks());
			values.put(START_WEEK_INDEX, 	configuration.getStartWeekIndex());
			
			ArrayList<SHDay> scheduledDays = configuration.getScheduleDays();
			values.put(MONDAY, 				scheduledDays.contains(SHDay.MONDAY));
			values.put(TUESDAY, 			scheduledDays.contains(SHDay.TUESDAY));
			values.put(WEDNESDAY, 			scheduledDays.contains(SHDay.WEDNESDAY));
			values.put(THURSDAY, 			scheduledDays.contains(SHDay.THURSDAY));
			values.put(FRIDAY, 				scheduledDays.contains(SHDay.FRIDAY));
			values.put(SATURDAY, 			scheduledDays.contains(SHDay.SATURDAY));
			values.put(SUNDAY, 				scheduledDays.contains(SHDay.SUNDAY));
			
			// Insert the schedule data for the specified ID, or replace the values if the ID already exists
			database.insertWithOnConflict(SCHEDULE_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
	
			// Update the SHSchedule Times table ////////////////////////////////////////
			
			for(LocalTime time : configuration.getScheduleTimes()) {
				
				// NOTE: The query is setup in 2 parts as the ConflictIgnore algorithm is bugged on Android's end.
				// See: http://stackoverflow.com/questions/13391915/why-does-insertwithonconflict-conflict-ignore-return-1-error
				
				Cursor c = database.query(
						SCHEDULE_TIMES_TABLE, 
		        		new String[]{SCHEDULE_ID}, 
		        		CONFIGURATION_ID + "=? AND " + HOUR + "=? AND " + MINUTE + "=?",
		        		new String[]{
								String.valueOf(configuration.getConfigurationID()), 
								String.valueOf(time.getHourOfDay()), 
								String.valueOf(time.getMinuteOfHour()) },
		        		null, null, null);
		        
		        if(c.moveToNext()) {
		        	
		        	// The time already exists--we must keep the schedule ID so it is not deleted later on
		        	scheduleIDsToKeep.add(c.getInt(c.getColumnIndex(SCHEDULE_ID)));
		        } else {
		        	
		        	// The time does not yet exist--add it and keep the new schedule ID
		        	values = new ContentValues();
		        	values.put(CONFIGURATION_ID, 	configuration.getConfigurationID());
					values.put(HOUR, 	time.getHourOfDay());
					values.put(MINUTE, 	time.getMinuteOfHour());
					scheduleIDsToKeep.add((int) database.insert(SCHEDULE_TIMES_TABLE, null, values)); // TODO : Remove the container method if not working
					
					// TODO: simply add the insert return value from above and remove this second query below.
					// The DELETE was fucking up so this was just to be sure...
					
//						Cursor d = database.query(
//								SCHEDULE_TIMES_TABLE, 
//				        		new String[]{SCHEDULE_ID}, 
//				        		CONFIGURATION_ID + "=? AND " + HOUR + "=? AND " + MINUTE + "=?",
//				        		new String[]{
//										String.valueOf(configuration.getConfigurationID()), 
//										String.valueOf(time.getHourOfDay()), 
//										String.valueOf(time.getMinuteOfHour())
//								},
//				        		null, null, null);
//						if(d.moveToNext()) {
//				        	scheduleIDsToKeep.add(d.getInt(d.getColumnIndex(SCHEDULE_ID)));
//				        }
		        }
		        c.close();
			}
		}
		
		// Prepare the query that will remove schedule times from the database that should no longer exist
		// NOTE : scheduleIDs of times that are the same as in a new configuration must be retained and that
		// is why the times cannot simply be all destroyed and re-added.
		
		StringBuilder keptIDs = new StringBuilder();
		keptIDs.append("(");
		boolean first = true;
		for(int id : scheduleIDsToKeep) {
			if (first) {
		        first = false;
		        keptIDs.append(id);
		    } else {
		    	keptIDs.append(",").append(id);
		    }
		}
		keptIDs.append(")");
		
		String delete = "DELETE FROM " + SCHEDULE_TIMES_TABLE + " WHERE " + SCHEDULE_ID + " NOT IN " + keptIDs.toString();
		database.execSQL(delete);
	}
	
	/**
	 * Updates the medication in the table or inserts it if the ID does not exist
	 */
	private void saveMedication(int configurationID, SHMedication medication) {
		
		ContentValues values = new ContentValues();
		values.put(CONFIGURATION_ID, 	configurationID);
		values.put(NAME, 				medication.getName());
		values.put(FORM, 				medication.getForm());
		values.put(DOSAGE, 				medication.getDosage());
		values.put(UNITS, 				medication.getUnits());
		values.put(QUANTITY,			medication.getQuantity());
		
		// Insert the medication or update the medication information if it already exists
		openDatabase();
		database.insertWithOnConflict(MEDICATIONS_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
	}
	
// @SCHEDULE
// =============================================================================================
	
	@Override
	public ArrayList<SHScheduleItem> getTodaysSchedule() {
		
		ArrayList<SHScheduleItem> scheduleItems = new ArrayList<SHScheduleItem>();
		
		/*
		 * x = everyXWeeks;
		 * y = startWeekIndex;
		 * z = thisWeekIndex;
		 * if x DIVIDES |y-z|  -> add the item to the cursor; it is scheduled for the current date
		 * |y-z| % x == 0
		 */
		int currentWeekIndex 	= LocalDate.now().getWeekOfWeekyear();
		String DAY 				= ((LocalDate.Property) LocalDate.now().dayOfWeek()).getAsText().toLowerCase(Locale.US);
		
		// Get the configurations that are scheduled for today
		String query = "SELECT " + CONFIGURATION_ID + " FROM " + SCHEDULE_TABLE + " WHERE " +
		DAY + " = " + TRUE + " AND ((" + START_WEEK_INDEX + " - " + currentWeekIndex + ") % " + EVERY_X_WEEKS + ") = 0";
		
		openDatabase();
		Cursor c = database.rawQuery(query,	null);
		while(c.moveToNext()) {
			
			// Select all of the times for the configurations that are scheduled for today
			query = "SELECT " + 	
					SCHEDULE_TIMES_TABLE + "." + SCHEDULE_ID + ", " +
					SCHEDULE_TIMES_TABLE + "." + HOUR + ", " +
					SCHEDULE_TIMES_TABLE + "." + MINUTE + 
					" FROM " + SCHEDULE_TIMES_TABLE +
					" INNER JOIN " + CONFIGURED_TOOLS_TABLE + 
					" ON " + CONFIGURED_TOOLS_TABLE + "." + CONFIGURATION_ID + " = " +  SCHEDULE_TIMES_TABLE  + "." + CONFIGURATION_ID +
					" WHERE " + CONFIGURED_TOOLS_TABLE + "." + CONFIGURATION_ID + " = " + c.getInt(c.getColumnIndex(CONFIGURATION_ID)) + " AND " + CONFIGURED_TOOLS_TABLE + "." + ACTIVE + " = " + TRUE;
			
			Cursor schedule = database.rawQuery(query, null);
			while(schedule.moveToNext()) {
				
				LocalTime scheduleTime = new LocalTime(schedule.getInt(schedule.getColumnIndex(HOUR)), schedule.getInt(schedule.getColumnIndex(MINUTE)));
				int scheduleID = schedule.getInt(schedule.getColumnIndex(SCHEDULE_ID));
				
				// Check to see if the scheduled items have already been completed
				String CURRENT_DATE = this.toDateForSQLite(new LocalDate());
				query = "SELECT " + COMPLETION_ID +"," + "date(" + COMPLETION_DATE_TIME + ")" +  " FROM " + COMPLETED_TABLE + " WHERE " + SCHEDULE_ID + " = " + scheduleID;
				
				Cursor completed = database.rawQuery(query, null);
				boolean complete = false;
				
				// TODO: This seems like a foolish way to do this... Could be done in the query, right?  Wrong. The equality check
				// on the dates was simply not functioning properly.  So here we are...
				
				// TODO: This is still an extremely inefficient way to query, try to clean this up some.
				
				while(completed.moveToNext()) {
					
					// The scheduled item has already been completed--add it as a completed item.
					if(CURRENT_DATE.equals(completed.getString(1))) {
						complete = true;
						scheduleItems.add(new SHScheduleItem(scheduleID, completed.getInt(completed.getColumnIndex(COMPLETION_ID)), scheduleTime));
						break;
					}
				}
				if(!complete) {
					// The scheduled item has not been completed--add it to the schedule.
					scheduleItems.add(new SHScheduleItem(scheduleID, null, scheduleTime));
				}
				completed.close();
			}
			schedule.close();
		}
		
		c.close();
		return scheduleItems;
	}
	
	@Override
	public SHRecord getRecord(int completionID) {
		
		// Get the configuration ID that maps to the completion ID
		String query = "SELECT " + CONFIGURATION_ID + ", " + COMPLETION_DATE_TIME + " FROM " + COMPLETED_TABLE + " WHERE " + COMPLETION_ID + "=" + completionID;
		SHRecord record = null;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
		if(c.moveToNext()) {
			
			LocalDateTime dateTimeCompleted = getDateTimeFromSQLite(c.getString(c.getColumnIndex(COMPLETION_DATE_TIME)));
			
        	int configurationID = c.getInt(c.getColumnIndex(CONFIGURATION_ID));
        	String toolCategory = getToolCategory(configurationID);
        	
        	// Find out the tool type based on the configuration ID
        	query = "SELECT " + TOOL_TYPE + " FROM " + CONFIGURED_TOOLS_TABLE + " WHERE " + CONFIGURATION_ID + "=" + configurationID;
    		Cursor type = database.rawQuery(query, null);
    		
    		if(type.moveToNext()) {
    			
	    		if(toolCategory.equals(SHSelfManagementToolCategory.HEALTH_METRIC.toString())) {
	    			
	    			SHHealthMetricID id = SHHealthMetricID.fromString(type.getString(type.getColumnIndex(TOOL_TYPE)));
	    			
	    			if(id.isConfirmable()) {
	    				// Construct a confirmable health metric record
	    				record = new SHConfirmableHealthMetricRecord(id, dateTimeCompleted);
	    			} else {
	    				// The measurements must be retrieved for the record
	    				query = "SELECT " + MEASUREMENT_MAP + " FROM " + MEASUREMENTS_TABLE + " WHERE " + COMPLETION_ID + "=" + completionID;
		    			Cursor map = database.rawQuery(query, null);
		    			
		    			if(map.moveToNext()) {
			    			HashMap<SHHealthMetricComponent, Number> measurementMap =
			    					SHMeasurableHealthMetricRecord.measurementMapFromSQLiteString(map.getString(map.getColumnIndex(MEASUREMENT_MAP)));
			    			
			    			// Construct a measurable health metric record
			    			record = new SHMeasurableHealthMetricRecord(id, dateTimeCompleted, measurementMap);
		    			}
		    			map.close();
	    			}
	        	} else if(toolCategory.equals(SHSelfManagementToolCategory.SURVEY.toString())) {
	        		
	        		// The responses must be retrieved for the survey record
	        		query = "SELECT " + RESPONSE_MAP + " FROM " + SURVEY_RESPONSES_TABLE + " WHERE " + COMPLETION_ID + "=" + completionID;
	        		Cursor map = database.rawQuery(query, null);
	        		
	        		if(map.moveToNext()) {
		        		HashMap<Integer, Integer> responseMap = 
		                		SHSurveyRecord.responseMapFromSQLiteString(map.getString(map.getColumnIndex(RESPONSE_MAP)));
		        		
		        		// Construct a survey record
		        		record = new SHSurveyRecord(SHSurveyID.fromString(type.getString(type.getColumnIndex(TOOL_TYPE))), responseMap, dateTimeCompleted);
	        		}
	        		map.close();
	        	} else if(toolCategory.equals(SHSelfManagementToolCategory.MEDICATION.toString())) {
	        		
	        		// Construct a medication record
	        		record = new SHMedicationRecord(new SHMedicationID(configurationID), dateTimeCompleted);
	        	}
    		}
    		type.close();
		}
		
		c.close();
		return record;
	}
	
	@Override
	public SHSelfManagementTool getSelfManagementTool(int scheduleID) {
		
		// Get the configuration ID that maps to the schedule ID
		String query = "SELECT " + CONFIGURATION_ID + " FROM " + SCHEDULE_TIMES_TABLE + " WHERE " + SCHEDULE_ID + "=" + scheduleID;
		SHSelfManagementTool tool = null;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
        if(c.moveToNext()) {
        	
        	int configurationID = c.getInt(c.getColumnIndex(CONFIGURATION_ID));
        	
        	// Get the category from the configuration ID
        	SHSelfManagementToolCategory toolCategory = SHSelfManagementToolCategory.fromString(getToolCategory(configurationID));
        	
        	// Get the tool type that maps to the configuration ID
        	query = "SELECT " + TOOL_TYPE + " FROM " + CONFIGURED_TOOLS_TABLE + " WHERE " + CONFIGURATION_ID + "=" + configurationID;
    		Cursor type = database.rawQuery(query, null);
    		if(type.moveToNext()) {
    			
    			String toolType = type.getString((type.getColumnIndex(TOOL_TYPE)));
    			
	        	if(toolCategory == SHSelfManagementToolCategory.HEALTH_METRIC) {
	        		// Generate a health metric tool from the configuration ID
	        		SHHealthMetricID id = SHHealthMetricID.fromString(toolType);
	        		if(id.isConfirmable()) {
	        			tool = new SHConfirmableHealthMetricTool(id);
	        		} else {
	        			tool = new SHMeasurableHealthMetricTool(id);
	        		}
	        	} else if(toolCategory == SHSelfManagementToolCategory.SURVEY) {
	        		// Generate a survey tool from the configuration ID
	        		tool = new SHSurveyTool(SHSurveyID.fromString(toolType));
	        	} else if(toolCategory == SHSelfManagementToolCategory.MEDICATION) {
	        		// Generate a medication tool from the configuration ID
	        		tool = new SHMedicationTool(getMedication(new SHMedicationID(configurationID)));
	        	}
    		}
    		type.close();
        }
        
        c.close();
        return tool;
	}
	
// @PRIVATE_LOGIC
// =============================================================================================
	
	/**
	 *	Maps from ToolType to ConfigurationID for a reverse lookup.
	 *	For use on Survey, HealthMetric, Log, and Goal Records, NOT Medication Records.
	 */
	private int getConfigurationID(String toolType) {
		
		if(!toolType.equals(SHSelfManagementToolCategory.MEDICATION.toString())) {
			
			String query = "SELECT " + CONFIGURATION_ID + " FROM " + CONFIGURED_TOOLS_TABLE + " WHERE " + TOOL_TYPE + " = '" + toolType + "'";
			
			openDatabase();
			Cursor c = database.rawQuery(query, null);
	        if(c.moveToNext()) {
	        	int id = c.getInt(c.getColumnIndex(CONFIGURATION_ID));
	        	c.close();
	        	return id;
	        }
	        c.close();
		}
		return -1;
	}
	
	/**
	 *	Maps from ConfigurationID to ToolCategory.
	 */
	private String getToolCategory(int configurationID) {
			
		String query = "SELECT " + TOOL_CATEGORY + " FROM " + CONFIGURED_TOOLS_TABLE + " WHERE " + CONFIGURATION_ID + "=" + configurationID;
		
		openDatabase();
		Cursor c = database.rawQuery(query, null);
        if(c.moveToNext()) {
        	String category = c.getString(c.getColumnIndex(TOOL_CATEGORY));
        	c.close();
        	return category;
        }
    	c.close();
    	return null;
	}
	
	/**
	 * @return A new completion ID to be mapped to a completed record and potentially a schedule ID
	 */
	private int getNewCompletionID() {
		
        String query = "SELECT MAX(" + COMPLETION_ID + ") FROM " + COMPLETED_TABLE;
        
        openDatabase();
        Cursor c = database.rawQuery(query, null);
        if(c.moveToFirst()) {
        	int id = c.getInt(0) + 1;
        	c.close();
        	return id;
        } 
        c.close();
    	return 0;
	}
	
	/**
	 * 	Converts a LocalDateTime to a qualified DateTime SQLite String
	 */
	private String toDateTimeForSQLite(LocalDateTime dateTime) {
		return dateTime.toString("YYYY-MM-dd HH:mm:ss"); 
	}
	
	/**
	 * 	Converts a DateTime from the database into a LocalDateTime
	 */
	private LocalDateTime getDateTimeFromSQLite(String dateTime) {
		return DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss").parseLocalDateTime(dateTime);
	}
	
	/**
	 * 	Converts a LocalDate to a qualified DateTime SQLite String
	 */
	private String toDateForSQLite(LocalDate date) {
		return date.toString("YYYY-MM-dd"); 
	}
	
	/**
	 * 	Converts a DateTime from the database into a LocalDate
	 */
	private LocalDate getDateFromSQLite(String date) {
		return DateTimeFormat.forPattern("YYYY-MM-dd").parseLocalDate(date);
	}
	
	/**
	 * 	Converts a given clause into an interval clause based on the query interval and column name.
	 * 	The initial clause is prepended to the new interval clause generated.
	 */
	private String intervalClause(String initialClause, String columnName, SHRecordQueryInterval interval) {
		
		String intervalClause = SHRecordQueryInterval.toSQLiteDateIntervalClause(interval, columnName);
		
		if(intervalClause != null) {
			if(initialClause != null) {
				return initialClause + " AND " + intervalClause;
			} else {
				return intervalClause;
			}
		} else {
			return initialClause;
		}
	}
	
	
	/**
	 * Generate a CSV string of all values in the given table
	 */
	private String toCSV(String table) {
		
        StringBuilder csv = new StringBuilder();
        String query = "SELECT * FROM " + table;

        openDatabase();
        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        
        String[] columns = c.getColumnNames();
        int count = 0;
        
        for (String column: columns) {
        	
        	csv.append(column);
            if (count++ < columns.length - 1) { csv.append(','); }
        }
        
        c.moveToPosition(-1);
        boolean added = false;
        
        while (c.moveToNext()) {
        	
            added = true;
            csv.append('\n');
            count = 0;
            
            for (int i = 0; i < columns.length; i++) {
                String value;
                try {
                    value = c.getString(i);
                } catch (SQLiteException e) {
                    // assume that if the getString threw this exception then the column is not
                    // representable by a string, e.g. it is a BLOB.
                    value = "<unprintable>";
                }
                csv.append(value);
                if (count++ < columns.length - 1) {
                	csv.append(',');
                }
            }
            csv.append('\n');
        }  
        c.close();
        if (!added) return null;
        return csv.toString();
    }
}

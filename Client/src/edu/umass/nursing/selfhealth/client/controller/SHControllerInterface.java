/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.controller;

import java.util.ArrayList;

import edu.umass.nursing.selfhealth.client.configuration.SHConfiguration;
import edu.umass.nursing.selfhealth.client.healthMetrics.SHHealthMetricID;
import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.medication.SHMedicationID;
import edu.umass.nursing.selfhealth.client.patient.SHPatient;
import edu.umass.nursing.selfhealth.client.records.SHHealthMetricRecord;
import edu.umass.nursing.selfhealth.client.records.SHMedicationRecord;
import edu.umass.nursing.selfhealth.client.records.SHRecord;
import edu.umass.nursing.selfhealth.client.records.SHRecordQueryInterval;
import edu.umass.nursing.selfhealth.client.records.SHSurveyRecord;
import edu.umass.nursing.selfhealth.client.schedule.SHScheduleItem;
import edu.umass.nursing.selfhealth.client.surveys.SHSurveyID;
import edu.umass.nursing.selfhealth.client.tools.SHHealthMetricTool;
import edu.umass.nursing.selfhealth.client.tools.SHSelfManagementTool;
import edu.umass.nursing.selfhealth.client.tools.SHSelfManagementToolCategory;
import edu.umass.nursing.selfhealth.client.tools.SHSurveyTool;

/**
 * 	Access point for all database-dependent queries.
 */
public interface SHControllerInterface {

// @CONFIGURATION
// =============================================================================================
	
	/**
	 * Begin the JSON parsing transaction
	 */
	public void loadConfiguration();
	
	/**
	 * @return Whether or not the configuration Version was updated.
	 * This is used when loading the configuration to see if the database should be updated.
	 * Returns false if the configurationVersionof the file is the same as in the database.
	 */
	public boolean updateConfigurationVersion(int configurationVersion);
	
	/**
	 * @return	Configuration Version ID.  This is used to determine if the database
	 * 			should be emptied of records when switching from a Demo configuration
	 * 			to a Provider generated configuration.
	 */
	public int getConfigurationVersion();
	
	/**
	 * Clear out the entire database when moving to a non-demo configuration version.
	 */
	public void destoryAllDemoData();
	
// @PATIENT
// =============================================================================================
	
	/**
	 * @param patient The patient to overwrite or insert into the database
	 */
	public void updatePatient(SHPatient patient);
	
	/**
	 * @return The patient currently in the database
	 */
	public SHPatient getPatient();
	
// @EXPORT
// =============================================================================================
	
	/**
	 * Writes all collected data to external storage
	 */
	public void exportPatientData();
		
// @RECORDS
// =============================================================================================
	
	/**
	 * @param healthMetricID	The ID that determines which health metric records to return.
	 * @param interval			The time interval for which records should be retrieved
	 * @return					The list of appropriate records matching the criteria
	 */
	public ArrayList<SHHealthMetricRecord> getHealthMetricRecords(
			SHHealthMetricID healthMetricID, 
			SHRecordQueryInterval interval);
	
	/**
	 * Saves a HealthMetricRecord to the database
	 * @param scheduleID The scheduleID for which the record was completed. Null if adHac.
	 * @return completionID;
	 */
	public int saveHealthMetricRecord(SHHealthMetricRecord record, Integer scheduleID);
	
	/**
	 * @param healthMetricID	The ID that determines which medication records are returned
	 * 							Filter on ID as the primary key in the MedicationRecord Table
	 * @param interval			The time interval for which records should be retrieved
	 * @return					The list of appropriate records matching the criteria
	 */
	public ArrayList<SHMedicationRecord> getMedicationRecords(
			SHMedicationID medicationID, 
			SHRecordQueryInterval interval);
	
	/**
	 * Saves a MedicationRecord to the database
	 * @param scheduleID The scheduleID for which the record was completed. Null if adHac.
	 * @return completionID
	 */
	public int saveMedicationRecord(SHMedicationRecord record, Integer scheduleID);
	
	/**
	 * @param surveyID		The ID that determines which table to pull survey records from.
	 * @param interval		The time interval for which records should be retrieved
	 * @return				The list of appropriate records matching the criteria
	 */
	public ArrayList<SHSurveyRecord> getSurveyRecords(
			SHSurveyID surveyID, 
			SHRecordQueryInterval interval);
	
	/**
	 * Saves a SurveyRecord to the database
	 * @param scheduleID The scheduleID for which the record was completed. Null if adHac.
	 * @return the completionID for the saved record
	 */
	public int saveSurveyRecord(SHSurveyRecord record, Integer scheduleID);

	
// @MEDICATION_LOOKUP
// =============================================================================================

	/**
	 * 	Construct a medication from the Medication Table based on the ID key
	 */
	public SHMedication getMedication(SHMedicationID id);
	
	/**
	 * 	Retrieve all Medications available to the user
	 */
	public ArrayList<SHMedication> getActiveMedications();
	
	
// @TOOLS
// =============================================================================================
	
	/**
	 * 	Provides the categories available to the user (Health Metric, Medication, Surveys...)
	 */
	public ArrayList<SHSelfManagementToolCategory> getActiveToolCategories();
	
	/**
	 * 	Provides the Survey Tools that have been configured for use
	 */
	public ArrayList<SHSurveyTool> getActiveSurveyTools();
	
	/**
	 * 	Provides the Health Metric Tools that have been configured for use
	 */
	public ArrayList<SHHealthMetricTool> getActiveHealthMetricTools();
	
	// TODO : Change from using getActiveMedications (@MEDICATION_LOOKUP) to getActiveMedicationTools using {@link: SHMedicationTool}
	
	/**
	 * @param configurations The configuration to be added or updated in the database.
	 */
	public void updateToolConfigurations(ArrayList<SHConfiguration> configurations);
	
// @SCHEDULE
// =============================================================================================
	
	/**
	 * Retrieve all items that have been scheduled for the day--both completed and pending
	 */
	public ArrayList<SHScheduleItem> getTodaysSchedule();
	
	/**
	 * Retrieve the record matching the completionID
	 */
	public SHRecord getRecord(int completionID);
	
	/**
	 * Retrieve the tool for the scheduled item
	 */
	public SHSelfManagementTool getSelfManagementTool(int scheduleID);
	
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.configuration;

import java.util.ArrayList;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import edu.umass.nursing.selfhealth.client.application.SHDay;

/**
 *	An intermediary for reading configurations from JSON and updating the Database with the configuration information.
 */

public class SHConfiguration {

	/**
	 * 	A unique identifier given to this configuration from the Provider application
	 */
	private int 				configurationID;
	/**
	 * The general category of self-management tool this configuration is describing
	 */
	private String 				toolCategory;
	/**
	 * The specific self-management tool this configuration is describing
	 */
	private String 				toolType;
	/**
	 * Whether this tool should be available to the Patient
	 * If false, the records should not be deleted--the tool should simply be hidden from use.
	 */
	private boolean 			active;
	/**
	 * How many weeks between each reminder
	 * For example, if n=1, the reminder will trigger weekly.  If n=2, it will trigger every 2 weeks, etc...
	 */
	private int 				everyXWeeks;
	/**
	 * The index (0-51) of the week that the configuration was updated
	 * This index is used to determine whether a reminder should be triggered using the formula:
	 * (START_WEEK_INDEX - TODAYS_WEEK_INDEX) % EVERY_X_WEEKS
	 */
	private int 				startWeekIndex;
	/**
	 * The days for which the self-management tool is scheduled
	 */
	private ArrayList<SHDay> 	scheduleDays;
	/**
	 * The times for which the self-management tool is scheduled
	 */
	private ArrayList<LocalTime>scheduleTimes;
	
	
	/**
	 * An intermediary for reading configurations from JSON and updating the Database with the configuration information.
	 * 
	 * @param configurationID a unique identifier given to this configuration from the Provider application
	 * @param toolCategory the general category of self-management tool this configuration is describing
	 * @param toolType the specific self-management tool this configuration is describing
	 * @param active whether this tool should be available to the Patient. If false, the records should not be deleted--the tool should simply be hidden from use.
	 * @param everyXWeeks how many weeks between each reminder.
	 * For example, if n=1, the reminder will trigger weekly.  If n=2, it will trigger every 2 weeks, etc...
	 * @param startWeekIndex the index (0-51) of the week that the configuration was updated.
	 * This index is used to determine whether a reminder should be triggered using the formula:
	 * (START_WEEK_INDEX - TODAYS_WEEK_INDEX) % EVERY_X_WEEKS
	 * @param scheduleDays the days for which the self-management tool is scheduled
	 * @param scheduleTimes the times for which the self-management tool is scheduled
	 */
	public SHConfiguration(int configurationID, String toolCategory,
			String toolType, boolean active, int everyXWeeks,
			int startWeekIndex, ArrayList<SHDay> scheduleDays,
			ArrayList<LocalTime> scheduleTimes) {
		
		this.configurationID = configurationID;
		this.toolCategory = toolCategory;
		this.toolType = toolType;
		this.active = active;
		this.everyXWeeks = everyXWeeks;
		this.startWeekIndex = startWeekIndex;
		this.scheduleDays = scheduleDays;
		this.scheduleTimes = scheduleTimes;
	}
	
	public int getConfigurationID() 				{ return configurationID; }
	public String getToolCategory() 				{ return toolCategory; }
	public String getToolType() 					{ return toolType; }
	public boolean isActive() 						{ return active; }
	public int getEveryXWeeks() 					{ return everyXWeeks; }
	public int getStartWeekIndex() 					{ return startWeekIndex; }
	public ArrayList<SHDay> getScheduleDays() 		{ return scheduleDays; }
	public ArrayList<LocalTime> getScheduleTimes() 	{ return scheduleTimes; }

	/**
	 * 	Constructor for parsing a JSON object in the configuration file.
	 */
	public static SHConfiguration fromJSON(JSONObject json)
	{
		try{
			
			int 		configurationID = json.getInt("configurationID");
			String 		toolCategory	= json.getString("toolCategory");
			String 		toolType		= json.getString("toolType");
			boolean 	active			= json.getBoolean("active");
			int 		everyXWeeks		= json.getInt("everyXWeeks");
			int 		startWeekIndex	= LocalDate.now().getWeekOfWeekyear();
			
			ArrayList<SHDay> 	scheduleDays = new ArrayList<SHDay>();	
			ArrayList<LocalTime>scheduleTimes = new ArrayList<LocalTime>();
			
			// If reminders are not enabled, the scheduled days/times should remain empty
			if(json.getBoolean("reminders")) {
				
				JSONArray days 				= json.getJSONArray("days");
				JSONArray times 			= json.getJSONArray("times");
				for(int i = 0; i < days.length(); i++) {
					scheduleDays.add(SHDay.fromString(days.getString(i)));
				}
				for(int i = 0; i < times.length(); i++) {
					scheduleTimes.add(DateTimeFormat.forPattern("H:mm").parseLocalTime(times.getString(i)));
				}
			}
			
			return new SHConfiguration(configurationID, toolCategory, toolType, 
					active, everyXWeeks, startWeekIndex, scheduleDays, scheduleTimes); 
		}
		catch(JSONException jse)
		{
			Log.e("CONFIGURATION", "Error creating a configuration from JSON:/n" + jse.toString());
			return null;
		}
	}
}

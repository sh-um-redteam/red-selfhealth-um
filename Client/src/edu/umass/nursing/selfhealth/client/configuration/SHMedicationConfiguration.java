/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.client.configuration;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import edu.umass.nursing.selfhealth.client.medication.SHMedication;
import edu.umass.nursing.selfhealth.client.medication.SHMedicationID;

/**
 *	An intermediary for reading configurations from JSON and updating the Database with the configuration information.
 */
public class SHMedicationConfiguration extends SHConfiguration {

	/**
	 * The medication that was configured by the Provider
	 */
	private SHMedication medication;
	
	/**
	 * @param configuration the base configuration including all scheduling information
	 * @param medication the medication that was configured by the Provider
	 */
	public SHMedicationConfiguration(SHConfiguration configuration, SHMedication medication) {
		
		super(configuration.getConfigurationID(), 
				configuration.getToolCategory(), 
				configuration.getToolType(), 
				configuration.isActive(), 
				configuration.getEveryXWeeks(),
				configuration.getStartWeekIndex(), 
				configuration.getScheduleDays(), 
				configuration.getScheduleTimes());
		
		this.medication = medication;
	}
	
	public SHMedication getMedication() { return medication; }

	/**
	 * 	Constructor for parsing a JSON object in the configuration file.
	 */
	public static SHMedicationConfiguration fromJSON(JSONObject json)
	{
		try{
			SHConfiguration configuration = SHConfiguration.fromJSON(json);
			
			JSONObject medicationJSON 	= json.getJSONObject("medication");
			
			SHMedicationID id 	= new SHMedicationID(configuration.getConfigurationID());
			String name			= medicationJSON.getString("name");
			String form			= medicationJSON.getString("form");
			String units 		= medicationJSON.getString("units");
			double dosage		= medicationJSON.getDouble("dosage");
			double quantity		= medicationJSON.getDouble("quantity");
			
			SHMedication medication = new SHMedication(id, name, form, units, dosage, quantity);
			
			return new SHMedicationConfiguration(configuration, medication); 
		}
		catch(JSONException jse)
		{
			Log.e("CONFIGURATION", "Error creating a medication configuration from JSON:/n" + jse.toString());
			return null;
		}
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 *	Specifies the configuration of the given Physical Report (physicalReportEnumID).
 *	A wireless device for taking measurements may be associated to this configuration.
 */
@SuppressWarnings("serial")
public class PhysicalReportConfiguration extends Configuration {
	
	// Device for taking Physical Report measurements associated with this configuration.
	private Device device;

	public PhysicalReportConfiguration(int PatientID, int physicalReportEnumID, boolean enabled, boolean remindersEnabled, Schedule schedule) {
		super(PatientID, physicalReportEnumID, enabled, remindersEnabled, TypeEnum.PHYSICAL_REPORT, schedule);
	}
	
	public Device getDevice() {
		return device;
	}
	public void setDevice(Device device) {
		this.device = device;
	}
	
	@Override
	public int getJSONConfigurationId() {
		return Integer.valueOf(20 + "" + this.getId());
	}
	
	@Override
	public JSONObject toConfigurationJSON() {
		
		JSONObject configuration = super.toConfigurationJSON();
		
		try {
		
			configuration.put("toolCategory", 	this.getType().toJSONString());
			configuration.put("toolType", 		SensorEnum.getEnum(this.getId()).toJSONString());
			
			JSONObject returnJSON = new JSONObject();
			
			returnJSON.put("toolCategory", this.getType().toJSONString());
			returnJSON.put("configuration", configuration);
			
			return returnJSON;
			
		} catch(JSONException jse) {
			Log.e("MODEL",jse.toString());
			return null;
		}
	}
}

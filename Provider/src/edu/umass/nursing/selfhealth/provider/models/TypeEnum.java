/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

public enum TypeEnum {
	SURVEY (0),
	PHYSICAL_REPORT (1), 
	MEDICATION (2),
	PRN (3);
	
	private int intValue;
	
	private TypeEnum(int i){
		intValue = i;
	}
	
	public int getValue(){
		return intValue;
	}
	
	public static TypeEnum fromValue(int i){
		switch(i){
			case(0): return SURVEY;
			case(1): return PHYSICAL_REPORT;
			case(2): return MEDICATION;
			case(3): return PRN;
			default: return null;
		}
	}
	
	public String toJSONString() {
		switch(this) {
		case SURVEY :
			return "SURVEY";
		case PHYSICAL_REPORT :
			return "HEALTH_METRIC";
		case MEDICATION :
			return "MEDICATION";
		case PRN :
			return "PRN";
		default :
			return null;
		}
	}
	
}

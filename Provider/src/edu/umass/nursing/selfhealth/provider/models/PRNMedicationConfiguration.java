package edu.umass.nursing.selfhealth.provider.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import edu.umass.nursing.selfhealth.provider.views.ConfigureMedicationFragment;

public class PRNMedicationConfiguration extends MedicationConfiguration {


	private PRNMedication prnMedication;

	public PRNMedicationConfiguration(int PatientID, int medicationConfigurationID, boolean enabled, Schedule schedule, PRNMedication prnMedication) {
		super(PatientID);
		
		this.setPRNMedication(prnMedication);
	}
	
	// Convenience for setting up a new PRNMedication for the first time
	public PRNMedicationConfiguration(int PatientID) {
		super(PatientID);
		this.prnMedication = new PRNMedication();
	}

	public PRNMedication getMedication() {
		return prnMedication;
	}
	public void setPRNMedication(PRNMedication prnMedication) {
		this.prnMedication = prnMedication;
	}
	
	@Override
	public int getJSONConfigurationId() {
		return Integer.valueOf(40 + "" + this.getId());
	}
	
	@Override
	public JSONObject toConfigurationJSON() {
		
		JSONObject configuration = super.toConfigurationJSON();
		
		try {
		
			configuration.put("toolCategory", 	this.getType().toJSONString());
			configuration.put("toolType", 		"MEDICATION");
			
			JSONObject medication1 = new JSONObject();
			
			medication1.put("name",		    this.getMedication().getName());
			medication1.put("form", 		this.getMedication().getForm());
			medication1.put("units", 	    this.getMedication().getUnits());
			medication1.put("dosage", 	    this.getMedication().getDosage());
			medication1.put("quantity", 	this.getMedication().getQuantity());
			medication1.put("minThreshold", this.getMedication().getMinThreshold());
			medication1.put("maxThreshold", this.getMedication().getMaxThreshold());
			
			
			configuration.put("prnMedication", medication1);
			
			JSONObject returnJSON = new JSONObject();
			
			returnJSON.put("toolCategory", this.getType().toJSONString());
			returnJSON.put("configuration", configuration);
			
			return returnJSON;
			
		} catch(JSONException jse) {
			Log.e("MODEL",jse.toString());
			return null;
		}
	}
}
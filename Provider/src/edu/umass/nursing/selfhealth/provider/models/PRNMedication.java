package edu.umass.nursing.selfhealth.provider.models;

@SuppressWarnings("serial")
public class PRNMedication extends Medication{
	
	private String form;
	private String units;
	private double dosage;
	private double quantity;
	private double minThreshold;
	private double maxThreshold;
	
	public PRNMedication(String name, String form, double dosage, String units, double quantity, double minThreshold, double maxThreshold) {
		super(name, form, dosage, units, quantity);
		this.minThreshold = minThreshold;
		this.maxThreshold = maxThreshold;
	}
	
	public PRNMedication() {
		
	}
	
	@Override
	public String getDetailString() {
		return (quantity + "x " + dosage + units + " " + form + " " + "min:" +minThreshold + " " + "max:" + maxThreshold);
	}
	
	
	public double getMinThreshold() {
		return minThreshold; 
	}
	
	public void setMinThreshold(double minThreshold) {
		this.minThreshold = minThreshold;
	}
	
	public double getMaxThreshold() {
		return maxThreshold;
	}
	
	public void setMaxThreshold(double maxThreshold) {
		this.maxThreshold = maxThreshold;
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 *	Specifies the configuration of the given Survey (surveyEnumID)
 */
@SuppressWarnings("serial")
public class SurveyConfiguration extends Configuration {

	public SurveyConfiguration(int PatientID, int surveyEnumID, boolean enabled, boolean remindersEnabled, Schedule schedule) {
		super(PatientID, surveyEnumID, enabled, remindersEnabled, TypeEnum.SURVEY, schedule);
	}
	
	@Override
	public int getJSONConfigurationId() {
		return Integer.valueOf(30 + "" + this.getId());
	}
	
	@Override
	public JSONObject toConfigurationJSON() {
		
		JSONObject configuration = super.toConfigurationJSON();
		
		try {
		
			configuration.put("toolCategory", 	this.getType().toJSONString());
			configuration.put("toolType", 		SurveyEnum.getEnum(this.getId()).toJSONString());
			
			JSONObject returnJSON = new JSONObject();
			
			returnJSON.put("toolCategory", this.getType().toJSONString());
			returnJSON.put("configuration", configuration);
			
			return returnJSON;
			
		} catch(JSONException jse) {
			Log.e("MODEL",jse.toString());
			return null;
		}
	}
}

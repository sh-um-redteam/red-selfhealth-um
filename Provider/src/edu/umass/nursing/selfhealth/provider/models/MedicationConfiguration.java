/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 *	Specifies the configuration of the given Medication.
 *	The medication contains provider-options such as the dosage and quantity.
 */
@SuppressWarnings("serial")
public class MedicationConfiguration extends Configuration {

	private Medication medication;

	public MedicationConfiguration(int PatientID, int medicationConfigurationID, boolean enabled, Schedule schedule, Medication medication) {
		super(PatientID, medicationConfigurationID, enabled, true, TypeEnum.MEDICATION, schedule);
		
		this.setMedication(medication);
	}
	
	// Convenience for setting up a new Medication for the first time
	public MedicationConfiguration(int PatientID) {
		super(PatientID, -1, true, true, TypeEnum.MEDICATION, null);
		this.medication = new Medication();
	}

	public Medication getMedication() {
		return medication;
	}
	public void setMedication(Medication medication) {
		this.medication = medication;
	}
	
	@Override
	public int getJSONConfigurationId() {
		return Integer.valueOf(10 + "" + this.getId());
	}
	
	@Override
	public JSONObject toConfigurationJSON() {
		
		JSONObject configuration = super.toConfigurationJSON();
		
		try {
		
			configuration.put("toolCategory", 	this.getType().toJSONString());
			configuration.put("toolType", 		"MEDICATION");
			
			JSONObject medication1 = new JSONObject();
			
			medication1.put("name",		this.getMedication().getName());
			medication1.put("form", 		this.getMedication().getForm());
			medication1.put("units", 	this.getMedication().getUnits());
			medication1.put("dosage", 	this.getMedication().getDosage());
			medication1.put("quantity", 	this.getMedication().getQuantity());
			
			configuration.put("medication", medication1);
			
			JSONObject returnJSON = new JSONObject();
			
			returnJSON.put("toolCategory", this.getType().toJSONString());
			returnJSON.put("configuration", configuration);
			
			return returnJSON;
			
		} catch(JSONException jse) {
			Log.e("MODEL",jse.toString());
			return null;
		}
	}
}

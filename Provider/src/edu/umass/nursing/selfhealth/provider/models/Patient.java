/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

import java.io.Serializable;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/** JodaTime library simplifies age calculation **/

/**
 * Represents the Patient for whom the application is configured. Name, gender,
 * and date of birth are used by the provider to identify the patient.
 * 
 */
@SuppressWarnings("serial")
public class Patient implements Serializable {

	private int id;
	private String firstName;
	private String lastName;
	private String gender;
	private LocalDate dateOfBirth;

	public Patient(int id, String firstName, String lastName, String gender, LocalDate dateOfBirth) {
		this.setId(id);
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getGender() {
		return gender;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @return The age of the Patient--convenience method that calculates the
	 *         patient age based on current date and their date of birth.
	 */
	public String getAge() {
		LocalDate now = new LocalDate();
		Years age = Years.yearsBetween(dateOfBirth, now);
		return String.valueOf(age.getYears());
	}
	
	public JSONObject toPatientApplicationJSON() {
		try{
			JSONObject patient = new JSONObject();
			
			patient.put("patientID", 		this.getId());
			patient.put("firstName",		this.firstName);
			patient.put("lastName", 		this.lastName);
			patient.put("gender", 			this.getGender());
			patient.put("dateOfBirth", 		this.getDateOfBirth().toString("YYYY-M-d"));
			
			return patient;
		}
		catch(JSONException jse) {
			Log.e("JSON",jse.toString());
			return null;
		}
	}
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Medication implements Serializable {

	private String name;
	private String form;
	private String units;
	private double dosage;
	private double quantity;
	
	public Medication(String name, String form, double dosage, String units, double quantity) {
		this.name = name;
		this.form = form;
		this.dosage = dosage;
		this.units = units;
		this.quantity = quantity;
	}
	
	public Medication() {
		
	}
	
	public String getDetailString() {
		return (quantity + "x " + dosage + units + " " + form);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public double getDosage() {
		return dosage;
	}
	public void setDosage(double dosage) {
		this.dosage = dosage;
	}
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
}

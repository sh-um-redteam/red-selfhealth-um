package edu.umass.nursing.selfhealth.provider.models;

public class PRNSocialMedia {
	private boolean isEnabled;
	private double minThreshold;
	private double maxThreshold;
	
	public PRNSocialMedia(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
	
	public boolean isEnabled() {
		return isEnabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.isEnabled = enabled;
	}
	
	public double getMinThreshold() {
		return minThreshold; 
	}
	
	public void setMinThreshold(double minThreshold) {
		this.minThreshold = minThreshold;
	}
	
	public double getMaxThreshold() {
		return maxThreshold;
	}
	
	public void setMaxThreshold(double maxThreshold) {
		this.maxThreshold = maxThreshold;
	}
}

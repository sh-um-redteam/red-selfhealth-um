/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

import java.io.Serializable;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.util.Log;

/**
 *	Configurations allow the provider to specify whether tools are available for a patient,
 *	whether they are reminded to use those tools, and the schedule for those reminders to go off.
 *	Some tools are customized with further configuration information, such as the medication to
 *	be taken, or even a peripheral device to use for a measurement. 
 */
@SuppressLint("DefaultLocale")
@SuppressWarnings("serial")
public abstract class Configuration implements Serializable, Comparable<Configuration> {
	
	// Maps the configuration to the specified patient
	private int patientID;
	
	// Associative Identifier -- 	Key for Survey, Physical Report, or Medication IDs
	//								May not be unique for medications; dosages and other fields must vary
	private int id;	
	
	// Specifies which Type the configuration maps to (SURVEY, PHYSICAL_REPORT, MEDICATION)
	private TypeEnum type;
	
	// Whether the patient will have access to this tool
	private boolean enabled;
	
	// Whether the user will be reminded about using this tool
	private boolean remindersEnabled;
	
	// Provides a reminder schedule if reminders are enabled
	private Schedule schedule;

	/** Robust constructor **/
	public Configuration(int patientID, int id, boolean enabled, boolean remindersEnabled, TypeEnum type, Schedule schedule) {
		
		this.patientID = patientID;
		this.id = id;
		this.type = type;
		this.enabled = enabled;
		this.remindersEnabled = remindersEnabled;
		
		if(schedule == null) {
			this.schedule = new Schedule();
		} else {
			this.schedule = schedule;
		}
	}
	
	/** Unknown ID construction **/
	public Configuration(int patientID, String name, boolean enabled, boolean remindersEnabled, TypeEnum type, Schedule schedule) {
		
		this.patientID = patientID;
		this.id = -1;
		this.enabled = enabled;
		this.remindersEnabled = remindersEnabled;
		this.type = type;
		
		if(schedule == null) {
			this.schedule = new Schedule();
		} else {
			this.schedule = schedule;
		}
	}
	
	public void setId(int i){
		id = i;
	}
	public int getId() {
		return id;
	}
	public abstract int getJSONConfigurationId();
	
	public TypeEnum getType() {
		return this.type;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isRemindersEnabled() {
		return remindersEnabled;
	}
	public void setRemindersEnabled(boolean remindersEnabled) {
		this.remindersEnabled = remindersEnabled;
	}
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	public int getPatientID() {
		return patientID;
	}
	public void setPatientID(int patientID) {
		this.patientID = patientID;
	}
	
	// Comparator for sorting lists of Configurations -- provides consistency in the layouts
	@Override
	public int compareTo(Configuration compareConfiguration) {
		return (this.id) - compareConfiguration.id;
	}
	
	public JSONObject toConfigurationJSON() {
		try{
			
			JSONObject configuration = new JSONObject();
			
			configuration.put("configurationID",this.getJSONConfigurationId());
			configuration.put("toolCategory", 	this.getType().toJSONString());
			configuration.put("active", 		this.isEnabled());
			configuration.put("reminders", 		this.isRemindersEnabled());
			configuration.put("everyXWeeks", 	this.getSchedule().getWeekRecurrence());
			
			JSONArray days = new JSONArray();
			for(DayEnum day : this.getSchedule().getDays()) {
				days.put(day.toString());
			}
			
			JSONArray times = new JSONArray();
			for(LocalTime time : this.getSchedule().getTimes()) {
				times.put(time.toString("H:mm"));
			}
			
			configuration.put("days", days);
			configuration.put("times", times);
			
			return configuration;
		}
		catch(JSONException jse)
		{
			Log.e("MODEL",jse.toString());
			return null;
		}
	}

}



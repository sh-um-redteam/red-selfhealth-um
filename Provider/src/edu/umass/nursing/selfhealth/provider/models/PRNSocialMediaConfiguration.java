package edu.umass.nursing.selfhealth.provider.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

@SuppressWarnings("serial")
public class PRNSocialMediaConfiguration extends Configuration {

	boolean prnSMEnabled;
	
	public PRNSocialMediaConfiguration(int patientID, String name,
			boolean enabled, boolean remindersEnabled, TypeEnum type,
			Schedule schedule) {
		super(patientID, name, enabled, remindersEnabled, type, schedule);
	}

	private PRNSocialMedia  prnSocialMedia;

public boolean isEnabled() {
	return prnSMEnabled;
}
	public void setPRNMedication(boolean prnSMEnabled) {
		this.prnSMEnabled = prnSMEnabled;
	}
	
	@Override
	public int getJSONConfigurationId() {
		return Integer.valueOf(50 + "" + this.getId());
	}
	
	@Override
	public JSONObject toConfigurationJSON() {
		
		JSONObject configuration = super.toConfigurationJSON();
		
		try {
		
			configuration.put("toolCategory", 	this.getType().toJSONString());
			configuration.put("toolType", 		"MEDICATION");
			
			JSONObject socialMedia = new JSONObject();
			
			socialMedia.put("enabled",		    this.isEnabled());
		
			JSONObject returnJSON = new JSONObject();
			
			returnJSON.put("toolCategory", this.getType().toJSONString());
			returnJSON.put("configuration", configuration);
			
			return returnJSON;
			
		} catch(JSONException jse) {
			Log.e("MODEL",jse.toString());
			return null;
		}
	}
	public PRNSocialMedia getPrnSocialMedia() {
		return prnSocialMedia;
	}
	public void setPrnSocialMedia(PRNSocialMedia prnSocialMedia) {
		this.prnSocialMedia = prnSocialMedia;
	}
}
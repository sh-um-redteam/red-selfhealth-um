/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

public enum SurveyEnum {

	ADL 						(0),
	ATTITUDE 					(1),
	JADS						(2),
	IADL 						(3),
	LSNS						(4),
	SENSE_OF_CONTROL 			(5);
	
	private int surveyID;
	
	SurveyEnum(int surveyID) {
    	this.surveyID = surveyID;
    }
    
    public int getsurveyID() {
    	return this.surveyID;
    }
    
    public static SurveyEnum getEnum(int surveyID) {
    	switch(surveyID) {
	    	case(0): return ADL;
			case(1): return ATTITUDE;
			case(2): return JADS;
			case(3): return IADL;
			case(4): return LSNS;
			case(5): return SENSE_OF_CONTROL;
			default: return null;
    	}
    }
    
    @Override
    public String toString() {
    	
		switch(this) {
			case ADL :
				return "Daily Activities";
			case ATTITUDE :
				return "Attitude";
			case JADS :
				return "Dignity";
			case IADL :
				return "Daily Living";
			case LSNS:
				return "Social Network";
			case SENSE_OF_CONTROL :
				return "Sense of Control";
			default :
				return null;
		}
    }
    
    public String getEventTypeID() {
    	
    	switch(this) {
		case ADL :
			return "SURVEY_MBI";
		case ATTITUDE :
			return "SURVEY_ARS";
		case JADS :
			return "SURVEY_JADS";
		case IADL :
			return "SURVEY_IADL";
		case LSNS:
			return "SURVEY_LSNS";
		case SENSE_OF_CONTROL :
			return "SURVEY_SOC";
		default :
			return null;
    	}
    }
    
    public String toJSONString() {
		switch(this) {
			case ADL :
				return "Activities of Daily Living";
			case ATTITUDE :
				return "Attitude Assessment";
			case JADS :
				return "Dignity Scale";
			case IADL :
				return "Instrumental Activities";
			case LSNS:
				return "Social Network";
			case SENSE_OF_CONTROL :
				return "Sense Of Control";
			default :
				return null;
		}
    }
}

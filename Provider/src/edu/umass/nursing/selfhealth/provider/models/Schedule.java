/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalTime;

/**
 *	Schedules contain days of the week and times of day
 *	which can be used to prepare system alarms.
 */
@SuppressWarnings("serial")
public class Schedule implements Serializable {
	
	private static final Integer DEFAULT_HOUR = 12;
	private static final Integer DEFAULT_MINUTE = 0;

	// Which days the reminder should occur
	private List<DayEnum> days;
	
	// Which times of day the reminders should occur
	private List<LocalTime> times;
	
	// The reminders should occur on the scheduled days and times every X weeks
	private int weekRecurrence = 1;

	/** CONSTRUCTOR **/
	public Schedule(int weekRecurrence, List<DayEnum> days, List<LocalTime> times) {
		this.weekRecurrence = weekRecurrence;
		if(days.size() == 0) {
		    this.days = new ArrayList<DayEnum>();
			setDefaultDays();
		} else {
			this.days = days;
		}
		if(times.size() == 0) {
		    this.times = new ArrayList<LocalTime>();
			setDefaultTime();
		} else {
			this.times = times;
		}
	}
	
	/** LAZY CONSTRUCTOR **/
	public Schedule() {
		
		this.weekRecurrence = 1;
		this.days = new ArrayList<DayEnum>();
		this.times = new ArrayList<LocalTime>();
		
		setDefaultDays();
		setDefaultTime();
	}
	
	private void setDefaultTime() {
		times.add(new LocalTime(DEFAULT_HOUR, DEFAULT_MINUTE));
	}
	private void setDefaultDays() {
		for (int i = 0; i < 7; i++) {
			DayEnum d = DayEnum.getEnum(i);
			days.add(d);
		}
	}
	
	public List<DayEnum> getDays() {
		return days;
	}
	public void setDays(List<DayEnum> days) {
		this.days = days;
	}
	public boolean addDay(DayEnum day) {
		if (!days.contains(day)) {
			return days.add(day);
		}
		return false;
	}
	public boolean removeDay(DayEnum day) {
		return days.remove(day);
	}

	public List<LocalTime> getTimes() {
		return times;
	}
	public void setTimes(List<LocalTime> times) {
		this.times = times;
	}
	public boolean addTime(LocalTime time) {
		if (!times.contains(time)) {
			return times.add(time);
		}
		return false;
	}
	public boolean removeTime(LocalTime time) {
		return times.remove(time);
	}

	public int getWeekRecurrence() {
		return weekRecurrence;
	}

	public void setWeekRecurrence(int weekRecurrence) {
		this.weekRecurrence = weekRecurrence;
	}
	
	
}
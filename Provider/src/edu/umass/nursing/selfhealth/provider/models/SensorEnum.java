/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

public enum SensorEnum {

	BLOOD_GLUCOSE	(0),
	BLOOD_PRESSURE	(1),
	WEIGHT			(2),
	BLOOD_OXYGEN	(3),
	HEART_RATE		(4),
	ACTIVITY		(5),
	MEAL			(6),
	WATER			(7);
	
	private int sensorID;
	
	SensorEnum(int sensorID) {
    	this.sensorID = sensorID;
    }
    
    public int getsensorID() {
    	return this.sensorID;
    }
    
    public static SensorEnum getEnum(int sensorID) {
    	switch(sensorID) {
	    	case(0): return BLOOD_GLUCOSE;
			case(1): return BLOOD_PRESSURE;
			case(2): return WEIGHT;
			case(3): return BLOOD_OXYGEN;
			case(4): return HEART_RATE;
			case(5): return ACTIVITY;
			case(6): return MEAL;
			case(7): return WATER;
			default: return null;
    	}
    }
    
    @Override
    public String toString() {
    	
    	switch(this) {
			case BLOOD_GLUCOSE :
				return "Blood Glucose";
			case BLOOD_PRESSURE :
				return "Blood Pressure";
			case WEIGHT :
				return "Weight";
			case BLOOD_OXYGEN :
				return "Oxygen Saturation";
			case HEART_RATE:
				return "Heart Rate";
			case ACTIVITY :
				return "Activity";
			case MEAL :
				return "Meal";
			case WATER :
				return "Water";
			default :
				return null;
    	}
    }
    
    public String toJSONString() {
    	
    	switch(this) {
			case BLOOD_GLUCOSE :
				return "Blood Glucose";
			case BLOOD_PRESSURE :
				return "Blood Pressure";
			case WEIGHT :
				return "Weight";
			case BLOOD_OXYGEN :
				return "Blood Oxygen";
			case HEART_RATE:
				return "Heart Rate";
			case ACTIVITY :
				return "Activity";
			case MEAL :
				return "Meal";
			case WATER :
				return "Water";
			default :
				return null;
    	}
    }
    
    public String getEventTypeID() {
    	
    	switch(this) {
			case BLOOD_GLUCOSE :
				return "GLUCOSE";
			case BLOOD_PRESSURE :
				return "BP";
			case WEIGHT :
				return "WEIGHT";
			case BLOOD_OXYGEN :
				return "O2";
			case HEART_RATE:
				return "HR";
			case ACTIVITY :
				return "ACTIVITY";
			case MEAL :
				return "MEAL";
			case WATER :
				return "WATER";
			default :
				return null;
    	}
    }
    
    public String getEventRecordClassEquivalent() {
    	
    	switch(this) {
			case BLOOD_GLUCOSE :
				return "BloodGlucoseMeasurementRecord";
			case BLOOD_PRESSURE :
				return "BloodPressureRecord";
			case WEIGHT :
				return "WeightRecord";
			case BLOOD_OXYGEN :
				return "BloodOxygenRecord";
			case HEART_RATE:
				return "HeartRateRecord";
			case ACTIVITY :
				return "ActivityRecord";
			case MEAL :
				return "MealRecord";
			case WATER :
				return "WaterRecord";
			default :
				return null;
    	}
    }
}

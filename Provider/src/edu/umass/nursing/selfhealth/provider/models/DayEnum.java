/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.models;

/** Choices for Frequency **/
public enum DayEnum {

	SUNDAY 		(0),
	MONDAY 		(1),
	TUESDAY 	(2),
	WEDNESDAY 	(3),
	THURSDAY 	(4),
	FRIDAY 		(5),
	SATURDAY 	(6);
	
	private int weekPostion;
	
    DayEnum(int weekPosition) {
    	this.weekPostion = weekPosition;
    }
    
    public int getWeekPosition() {
    	return this.weekPostion;
    }
    
    public static DayEnum getEnum(int weekPosition) {
    	switch(weekPosition) {
		case 0 :
			return SUNDAY;
		case 1 :
			return MONDAY;
		case 2 :
			return TUESDAY;
		case 3 :
			return WEDNESDAY;
		case 4 :
			return THURSDAY;
		case 5 :
			return FRIDAY;
		case 6 :
			return SATURDAY;
		default :
			return null;
    	}
    }
    
    @Override
    public String toString() {
    	
		switch(this) {
			case SUNDAY :
				return "Sunday";
			case MONDAY :
				return "Monday";
			case TUESDAY :
				return "Tuesday";
			case WEDNESDAY :
				return "Wednesday";
			case THURSDAY :
				return "Thursday";
			case FRIDAY :
				return "Friday";
			case SATURDAY :
				return "Saturday";
			default :
				return null;
		}
    }
    
    public String toShorthandString() {
    	
		switch(this) {
			case SUNDAY :
				return "Sun";
			case MONDAY :
				return "Mon";
			case TUESDAY :
				return "Tue";
			case WEDNESDAY :
				return "Wed";
			case THURSDAY :
				return "Thu";
			case FRIDAY :
				return "Fri";
			case SATURDAY :
				return "Sat";
			default :
				return null;
		}
    }
    
}

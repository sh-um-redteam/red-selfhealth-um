/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;

import edu.umass.nursing.selfhealth.provider.database.DatabaseController;
import edu.umass.nursing.selfhealth.provider.models.Patient;
import edu.umass.nursing.selfhealth.provider.models.TypeEnum;
import edu.umass.nursing.selfhealth.provider.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class PatientProfileActivity extends ActivityWithActionBar {

	private Patient patient;
	
	 @Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("PATIENT", patient);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, "Patient Overview");
		setContentView(R.layout.activity_patient_profile);
		
		if(savedInstanceState == null) {
			patient = DatabaseController.getInstance(this).getPatient(patientID);
		} else {
			patient = (Patient) savedInstanceState.getSerializable("PATIENT");
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		patient = DatabaseController.getInstance(this).getPatient(patientID);
		this.setupTextFieldsByPatient(patient);
	}

	/**
	 * Fills in the appropriate text fields with Patient Information
	 */
	private void setupTextFieldsByPatient(Patient patient) {

		TextView name = (TextView) findViewById(R.id.patientNameTextView);
		TextView age = (TextView) findViewById(R.id.patientAgeTextView);

		name.setText(patient.getFirstName() + " " + patient.getLastName());
		age.setText(patient.getAge()); 
	}

	/** BUTTON:
	 * Opens the view to edit patient information. Give that view the current
	 * patient so that it may populate the text fields.
	 */
	public void editPatientInformation(View v) {

		 // Transfer to PatientProfile Screen
		Intent intent = new Intent(this, PatientInformationActivity.class);
		intent.putExtra("PATIENT", patient);
		startActivity(intent);
	}

	/** BUTTON:
	 * Opens the Exporting Data View
	 */
	public void viewDataExport(View v) {
		Intent myIntent = new Intent(this, ExportPatientDataActivity.class);
		myIntent.putExtra("PATIENT_ID", patient.getId());
		this.startActivity(myIntent);
	}
	
	/** BUTTON:
	 * Transition to ToggleTools populated with Surveys
	 */
	public void viewSurveys(View v) {
		Intent myIntent = new Intent(this, ToggleToolsActivity.class);
		myIntent.putExtra("PATIENT_ID", patient.getId());
		myIntent.putExtra("TOOL", "Surveys");
		myIntent.putExtra("TYPE", TypeEnum.SURVEY);
		this.startActivity(myIntent);
	}

	/** BUTTON:
	 * Transition to ToggleTools populated with Physical Reports
	 */
	public void viewPhysicalReports(View v) {
		Intent myIntent = new Intent(this, ToggleToolsActivity.class);
		myIntent.putExtra("PATIENT_ID", patient.getId());
		myIntent.putExtra("TOOL", "Physical Reports");
		myIntent.putExtra("TYPE", TypeEnum.PHYSICAL_REPORT);
		this.startActivity(myIntent);
	}

	/** BUTTON:
	 * Transition to Configured Medications
	 */
	public void viewMedications(View v) {
		Intent myIntent = new Intent(this, ToggleToolsActivity.class);
		myIntent.putExtra("PATIENT_ID", patient.getId());
		myIntent.putExtra("TOOL", "Medications");
		myIntent.putExtra("TYPE", TypeEnum.MEDICATION);
		this.startActivity(myIntent);
	}
}

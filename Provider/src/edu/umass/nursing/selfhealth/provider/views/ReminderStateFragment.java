/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;


import edu.umass.nursing.selfhealth.provider.R;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

public class ReminderStateFragment extends Fragment {
	
	TextView reminderText;
	ToggleButton reminderToggle;
	
	private ReminderListener listener;
	private boolean remindersEnabled;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reminder_state, container, false);
        
        reminderText = (TextView) view.findViewById(R.id.reminderText);
    	reminderToggle = (ToggleButton) view.findViewById(R.id.reminderToggleButton);
    	
    	reminderToggle.setOnClickListener(new View.OnClickListener() {
    	      @Override
    	      public void onClick(View v) {
    	    	  update(!remindersEnabled);
    	      }
    	    });
        return view;
    }
	
	public interface ReminderListener {
		public void onReminderToggled(boolean reminder);
	}
	  
  	@Override
    public void onAttach(Activity activity) {
      super.onAttach(activity);
      if (activity instanceof ReminderListener) {
        listener = (ReminderListener) activity;
      } else {
        throw new ClassCastException(activity.toString()
            + " must implemenet ReminderStateFragment.ReminderListener");
      }
    }
	  
	  // May also be triggered from the Activity
	  public void update(boolean remindersEnabled1) {
		  
		  if(remindersEnabled1) {
			  reminderToggle.setChecked(true);
			  reminderText.setTextColor(getResources().getColor(R.color.dark_text));
			  reminderText.setText("Patient Reminders On");
		  } else {
			  reminderToggle.setChecked(false);
			  reminderText.setTextColor(getResources().getColor(R.color.accent_light_gray));
			  reminderText.setText("Patient Reminders Off");
		  }
		  
		  this.remindersEnabled = remindersEnabled1;
		  
		  listener.onReminderToggled(remindersEnabled1);
	  }
}

/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;

import java.util.ArrayList;
import java.util.List;

import edu.umass.nursing.selfhealth.provider.database.DatabaseController;
import edu.umass.nursing.selfhealth.provider.models.Patient;
import edu.umass.nursing.selfhealth.provider.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PatientListActivity extends ActivityWithActionBar {

	// DB Access
	private DatabaseController controller;
	
	// DataSource
	private ArrayList<Patient> patients;
	
	// UI Components
	private ListView listPatients;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, "Patients");
		this.hideButton("Patients");
		setContentView(R.layout.activity_patient_list);
		
		// Retrieve the controller
		controller = DatabaseController.getInstance(this);
		
		// Find the UI Components
		listPatients = (ListView) findViewById(R.id.patientList);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		// Connect to DBController
		patients = (ArrayList<Patient>) controller.getAllPatients();
		
		// Adapt the list view using the dataSource
		ListAdapter adapter = new ListAdapter(this, R.id.patientList, patients);
		listPatients.setAdapter(adapter);
	}
	
	// Click the "New" button
	public void setupNewPatient(View v) {
		
		Intent intent = new Intent(this, PatientInformationActivity.class);
		startActivity(intent);
	}

	private class ListAdapter extends ArrayAdapter<Patient> {

		public ListAdapter(Context context, int adaptedViewResourceID, List<Patient> patients) {
			super(context, adaptedViewResourceID);
		}
		
		@Override
		public Patient getItem(int position) {
			return patients.get(position);
		}
		
		@Override
		public int getCount() {
			return patients.size();
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = getLayoutInflater();
			convertView = inflater.inflate(R.layout.row_delete, parent, false);
			
			ImageButton deleteIcon = (ImageButton) convertView.findViewById(R.id.delete_button);
			deleteIcon.setVisibility(View.GONE);

			/** Prepare the cell layout **/
			final RelativeLayout row = (RelativeLayout) convertView.findViewById(R.id.row_background);
			if (position == 0) {
				row.setBackgroundResource(R.drawable.border);
			} else {
				row.setBackgroundResource(R.drawable.border_no_top);
			}
			
			// Change the text of the row to show the Patient name
			TextView patientName = (TextView) row.findViewById(R.id.row_text);
			final Patient p = getItem(position);
			patientName.setText(p.getFirstName() + " " + p.getLastName());

			row.setOnClickListener(new OnClickListener() {
				 
				 @Override
				 public void onClick(View v) {
					 
					 // Transfer to PatientProfile Screen
					Intent intent = new Intent(getContext(), PatientProfileActivity.class);
					intent.putExtra("PATIENT_ID", p.getId());
					startActivity(intent);
				 }
			 });

			return convertView;
		}
	}
}

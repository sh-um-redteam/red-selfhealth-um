/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;

import java.util.List;

import edu.umass.nursing.selfhealth.provider.database.DatabaseController;
import edu.umass.nursing.selfhealth.provider.models.Configuration;
import edu.umass.nursing.selfhealth.provider.models.DayEnum;
import edu.umass.nursing.selfhealth.provider.models.MedicationConfiguration;
import edu.umass.nursing.selfhealth.provider.models.PhysicalReportConfiguration;
import edu.umass.nursing.selfhealth.provider.views.ReminderConfigurationActivity;
import edu.umass.nursing.selfhealth.provider.models.SensorEnum;
import edu.umass.nursing.selfhealth.provider.models.SurveyConfiguration;
import edu.umass.nursing.selfhealth.provider.models.SurveyEnum;
import edu.umass.nursing.selfhealth.provider.models.TypeEnum;
import edu.umass.nursing.selfhealth.provider.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;


/**
 * View and toggle Self Management Tools to enabled / disabled states.  Only
 * enabled tools should be usable by the Patient.
 */
public class ToggleToolsActivity extends ActivityWithActionBar {

	List<? extends Configuration> configurations;
	private TypeEnum type;
	private ListView listView;
	
	// Controller Access
	private DatabaseController controller;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, (String) getIntent().getExtras().get("TOOL"));
		setContentView(R.layout.activity_toggle_tools);
		
		controller = DatabaseController.getInstance(this);

		this.type = (TypeEnum) getIntent().getExtras().get("TYPE");
		
		
		Button addButton = (Button) this.findViewById(R.id.add);
		if(type == TypeEnum.MEDICATION) {
			addButton.setVisibility(View.VISIBLE);
			addButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					MedicationConfiguration configuration = new MedicationConfiguration(patientID);
					
					Intent intent = new Intent(getBaseContext(), ReminderConfigurationActivity.class);
					intent.putExtra("PATIENT_ID", patientID);
					intent.putExtra("CONFIGURATION", configuration);
					intent.putExtra("TYPE", type);
					intent.putExtra("SCHEDULE_INDEX", 0);
					startActivity(intent);
				}
				
			});
		} else {
			addButton.setVisibility(View.GONE);
		}
		
		listView = (ListView) findViewById(R.id.buttons);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void onStart() {
		super.onStart();
		
		// Retrieve the list of configurations to use as the Datasource
		configurations = controller.getConfigurations(patientID, type);

		// Bind the ListView with an Adapter
		listView.setAdapter(new ToggleAdapter(this, R.id.buttons, configurations));
	}
	
	/** ADAPTER:
	 * Connects the ListView with Rows that include the specific management tool
	 * name, the frequency of reminders, and whether the tool is enabled
	 **/
	private class ToggleAdapter<T extends Configuration> extends ArrayAdapter<T> {

		private List<T> data;

		public ToggleAdapter(Context context, int adaptedViewResourceID,
				List<T> data) {
			super(context, adaptedViewResourceID, data);
			this.data = data;
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public T getItem(int position) {
			return data.get(position);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			/** Inflate the row with the appropriate Layout **/
			if (convertView == null) {
				if(type == TypeEnum.MEDICATION) {
					convertView = getLayoutInflater().inflate(
							R.layout.row_with_toggle_button_medication, parent, false);
				} else {
					convertView = getLayoutInflater().inflate(
							R.layout.row_with_toggle_button, parent, false);
				}
			}

			// Retrieve the data to prepare the row 
			final T rowData = getItem(position);
			
			// Extract components to prepare the sub views
			
			// Medication Specific
			final String medicationDetails;
			TextView medicationDetailView = null; 
			
			final String name;
			if(rowData.getClass() == (SurveyConfiguration.class)) {
				name = SurveyEnum.getEnum((((SurveyConfiguration) rowData).getId())).toString();
			} else if(type == TypeEnum.MEDICATION){
				name = ((MedicationConfiguration)rowData).getMedication().getName();
				medicationDetails = ((MedicationConfiguration)rowData).getMedication().getDetailString();
				medicationDetailView = (TextView) convertView
						.findViewById(R.id.medication_description_text);
				medicationDetailView.setText(medicationDetails);
			} else {
				name = SensorEnum.getEnum((((PhysicalReportConfiguration) rowData).getId())).toString();
			}
			
			final boolean enabled = rowData.isEnabled();
			final boolean reminders = rowData.isRemindersEnabled();
			final List<DayEnum> frequency = rowData.getSchedule().getDays();
			
			// Retrieve the sub views to manage
			TextView nameTextView = (TextView) convertView
					.findViewById(R.id.reminderText);
			TextView frequencyTextView = (TextView) convertView
					.findViewById(R.id.button_subtitle);
			ToggleButton toggleButton = (ToggleButton) convertView
					.findViewById(R.id.reminderToggleButton);
			LinearLayout mainButton = (LinearLayout) convertView
					.findViewById(R.id.main_button);
			ImageView arrowIcon = (ImageView) convertView
					.findViewById(R.id.right_arrow);
			RelativeLayout rowView = (RelativeLayout) convertView
					.findViewById(R.id.row);

			// Prepare the sub views
			if (position == 0) {
				rowView.setBackgroundResource(R.drawable.border);
			} else {
				rowView.setBackgroundResource(R.drawable.border_no_top);
			}

			nameTextView.setText(name);
			
			if (enabled) {
				
				toggleButton.setChecked(true);
				mainButton.setEnabled(true);
				arrowIcon.setVisibility(View.VISIBLE);
				nameTextView.setTextColor(getContext().getResources().getColor(R.color.dark_text));
				
				if(type == TypeEnum.MEDICATION) {
					medicationDetailView.setTextColor(getContext().getResources().getColor(R.color.accent_dark_gray));
				}
				frequencyTextView.setTextColor(getContext().getResources().getColor(R.color.accent_red));
				
				if (!reminders) {
					frequencyTextView.setText("Reminders Disabled");
				} else {
					if (frequency.size() == 0) {
						frequencyTextView.setText("No Reminders Scheduled");
					} else if (frequency.size() == 7) {
						frequencyTextView.setText("Daily");
					} else {
						
						// TODO : Create DayEnum comparator for sorting days
						
						String frq = "";
						for (DayEnum d : frequency) {
							frq += (d.toShorthandString() + " ");
						}
						frq.trim();
						frequencyTextView.setText(frq);
					}
				}
			} else {
				
				toggleButton.setChecked(false);
				mainButton.setEnabled(false);
				arrowIcon.setVisibility(View.INVISIBLE);
				nameTextView.setTextColor(getContext().getResources().getColor(R.color.accent_light_gray));
				if(type == TypeEnum.MEDICATION) {
					medicationDetailView.setTextColor(getContext().getResources().getColor(R.color.accent_light_gray));
				}
				frequencyTextView.setTextColor(getContext().getResources().getColor(R.color.accent_light_gray));
				frequencyTextView.setText("Disabled");
			}

			// BUTTON:
			// Change enabled state on click of the ToggleButton
			toggleButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					rowData.setEnabled(!enabled);
					data.set(position, rowData);
					
					// Update the configuration in the database
					if(rowData.getClass() == (SurveyConfiguration.class)) {
						System.out.println("TOGGLING SURVEY");;
						controller.setConfiguration(patientID, (SurveyConfiguration) rowData);
					} else if(rowData.getClass() == (MedicationConfiguration.class)){
						System.out.println("TOGGLING MEDICATION");
						controller.setConfiguration(patientID, (MedicationConfiguration) rowData);
					} else {
						System.out.println("TOGGLING SENSOR");
						controller.setConfiguration(patientID, (PhysicalReportConfiguration) rowData);
					}
					
					//Reflect the change on screen
					notifyDataSetChanged();
				}
			});

			// BUTTON:
			// Transition to the ReminderConfiguration screen for the selected tool
			mainButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getContext(), ReminderConfigurationActivity.class);
					intent.putExtra("PATIENT_ID", patientID);
					intent.putExtra("CONFIGURATION", rowData);
					intent.putExtra("TYPE", type);
					intent.putExtra("SCHEDULE_INDEX", 0);
					getContext().startActivity(intent);
				}
			});

			// Finish setting up the row
			return convertView;
		}
	}
}

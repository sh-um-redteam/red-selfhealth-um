/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;

import org.joda.time.LocalDate;

import edu.umass.nursing.selfhealth.provider.database.DatabaseController;
import edu.umass.nursing.selfhealth.provider.models.Patient;
import edu.umass.nursing.selfhealth.provider.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PatientInformationActivity extends ActivityWithActionBar {

	// DB Access
	private DatabaseController controller;
	
	private EditText firstName, lastName, day, month, year;
	private Patient patient;

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("PATIENT", patient);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, "Patient Information");
		 this.hideButton("Patient Information");
		setContentView(R.layout.activity_patient_information);

		controller = DatabaseController.getInstance(this);
		
		if(savedInstanceState == null) {
			
			/**
			 * Retrieve the Patient object that was passed from the Patient Profile
			 * Activity. It may potentially be null if this is the first time
			 * setting up a patient.
			 **/
			Intent intent = getIntent();
			this.patient = (Patient) intent.getSerializableExtra("PATIENT");
		} else {
			this.patient = (Patient) savedInstanceState.getSerializable("PATIENT");
		}

		this.setupTextFieldsByPatient(patient);
	}

	private void setupTextFieldsByPatient(Patient patient) {
		
		// TODO : Gender Field

		firstName = (EditText) findViewById(R.id.editPatientFirstName);
		lastName = (EditText) findViewById(R.id.editPatientLastName);
		day = (EditText) findViewById(R.id.editDOBDay);
		month = (EditText) findViewById(R.id.editDOBMonth);
		year = (EditText) findViewById(R.id.editDOBYear);

		if (patient != null) {
			firstName.setText(patient.getFirstName());
			lastName.setText(patient.getLastName());
			day.setText(String.valueOf(patient.getDateOfBirth().getDayOfMonth()));
			month.setText(String.valueOf(patient.getDateOfBirth().getMonthOfYear()));
			year.setText(String.valueOf(patient.getDateOfBirth().getYear()));
		}
	}

	/** BUTTON:
	 * Saves updated Patient Information when 'Save' is tapped.
	 */
	public void updatePatientInformation(View v) {

		boolean isValid = true;
		
		String newFirstName = firstName.getText().toString().trim();
		String newLastName = lastName.getText().toString().trim();
		String dayString = day.getText().toString().trim();
		String monthString = month.getText().toString().trim();
		String yearString = year.getText().toString().trim();
		LocalDate newDOB = null;
		
		if(newFirstName.length()==0 || newFirstName.matches(".*\\d.*")) {
			firstName.setText("");
			isValid = false;
		}
		if(newLastName.length()==0 || newLastName.matches(".*\\d.*")) {
			lastName.setText("");
			isValid = false;
		}
		if(dayString.length()==0 || 
				monthString.length()==0 ||
				yearString.length()==0) {
			day.setText("");
			month.setText("");
			year.setText("");
				isValid = false;
		} else if (!dayString.matches("[0-9]+")||
				!monthString.matches("[0-9]+")||
				!yearString.matches("[0-9]+")) {
			day.setText("");
			month.setText("");
			year.setText("");
			isValid = false;
		} else {
			
			int newDay = Integer.parseInt(day.getText().toString());
			int newMonth = Integer.parseInt(month.getText().toString());
			int newYear = Integer.parseInt(year.getText().toString());
			
			if(newYear < 1800 || newYear > (new LocalDate()).getYear()) {
				month.setText("");
				year.setText("");
				day.setText("");
				isValid = false;
			} else {
				try {
					
					newDOB = new LocalDate(newYear, newMonth, newDay);
					
				} catch (Exception e) {
					
					month.setText("");
					year.setText("");
					day.setText("");
					isValid = false;
				}
			}
		}
		
		if(isValid) {
			
			Patient updatedPatient;
			
			if(patient == null) {
				updatedPatient = new Patient(-1, newFirstName, newLastName, "GENDERLESS", newDOB);
			} else {
				updatedPatient = new Patient(patient.getId(), newFirstName, newLastName, "GENDERLESS", newDOB);
			}
			// Update the Patient in the database
			controller.updatePatient(updatedPatient);
			
			// Finish the activity and return to the previous view
			this.finish();
			
		} else {
			
			// Notify the Provider that the information is invalid
//			Toast.makeText(getApplicationContext(), "Please enter valid information.",
//					   Toast.LENGTH_SHORT).show();
			
			// Notify the User that it has been saved
			Toast toast = Toast.makeText(this, "Please enter valid information.", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			LinearLayout toastLayout = (LinearLayout) toast.getView();
			TextView toastTV = (TextView) toastLayout.getChildAt(0);
			toastTV.setTextSize(30);
			toast.show();
			
		}
	}
}

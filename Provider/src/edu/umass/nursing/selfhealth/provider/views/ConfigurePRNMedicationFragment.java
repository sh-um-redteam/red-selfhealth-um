/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;

import java.util.ArrayList;
import java.util.Arrays;

import org.joda.time.Minutes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import edu.umass.nursing.selfhealth.provider.R;

public class ConfigurePRNMedicationFragment extends ConfigureMedicationFragment  {
	
	// TODO : BUG*** When the screen turns off for configuring a new medication, the values in the 
	// text fields are reset to nothing. Fragment Life-cycle.
	
	// TODO : The prepopulated lists of units + forms etc should be stored in the DB and new additions should be added to the DB.
	//			Then they should be retrieved with a controller call instead of hard-coded in here.

/****************** UI COMPONENTS ******************/
	
	// Text Views
	private TextView medicationTextView;
	private TextView formTextView;
	private TextView dosageTextView;
	private TextView unitTextView;
	private TextView quantityTextView;
	private TextView minThresholdTextView;
	private TextView maxThresholdTextView;
	
	// Buttons
	private ImageButton editButton;
	private LinearLayout formButton;
	private LinearLayout dosageButton;
	private LinearLayout unitButton;
	private LinearLayout quantityButton;
	private LinearLayout minThresholdButton;
	private LinearLayout maxThresholdButton;
	
/****************** DATA FIELDS ******************/
	
	private final String DEFAULT_FIELD = "Set";
	
	public String name;
	public String form;
	public String dosage;
	public String unit;
	public String quantity;
	public String minThreshold;
	public String maxThreshold;
	
	final String[] medications = new String[] { 
			"Furosemide", 
			"Hydrochlorothiazide",
			"Lisinopril", 
			"Amlodipine", 
			"Digoxin", 
			"Propranolol", 
			"Levemir",
			"Lantus",
			"Glucophage",
			"Atorvastatin",
			"Warfarin",
			"Citalopram",
			"Sertraline",
			"Hydrocodone",
			"Tramadol",
			"Acetaminophen",
			"Advair",
			"Albuterol",
			"Timolol",
			"Travoprost",
			"Ciprofloxacin",
			"Levofloxacin",
			"Cephalexin",
	};
	
	final String[] forms = new String[] { 
			"Pill", 
			"Tablet",
			"Capsule", 
			"Oral", 
			"Inhalant", 
			"Topical", 
	};
	final String[] units = new String[] { 
			"mg", 
			"mL",
	};
	

/****************** LISTENERS ******************/
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
			Bundle savedInstanceState) {

		Arrays.sort(medications);
		Arrays.sort(forms);
		Arrays.sort(units);
		
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_prn_medication, container, false);
		
		medicationTextView = (TextView) view.findViewById(R.id.MedicationNameLabel);
		formTextView = (TextView) view.findViewById(R.id.MedicationFormLabel);
		dosageTextView = (TextView) view.findViewById(R.id.MedicationDosageLabel);
		unitTextView = (TextView) view.findViewById(R.id.MedicationUnitLabel);
		quantityTextView = (TextView) view.findViewById(R.id.MedicationQuantityLabel);
		minThresholdTextView = (TextView) view.findViewById(R.id.TextView07);
		maxThresholdTextView = (TextView)  view.findViewById(R.id.MedicationMinThresholdLabel);
		
		editButton = (ImageButton) view.findViewById(R.id.MedicationNameEditButton);
		formButton = (LinearLayout) view.findViewById(R.id.MedicationFormButton);
		dosageButton = (LinearLayout) view.findViewById(R.id.MedicationDosageButton);
		unitButton = (LinearLayout) view.findViewById(R.id.MedicationUnitButton);
		quantityButton = (LinearLayout) view.findViewById(R.id.MedicationQuantityButton);
		minThresholdButton = (LinearLayout) view.findViewById(R.id.MedicationMinThresholdButton);
		maxThresholdButton = (LinearLayout) view.findViewById(R.id.MedicationMaxThresholdButton);
		
		editButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				AlertDialog alertdialog;
            	
            	final AlertDialog.Builder builder = new AlertDialog.Builder(container.getContext());
                builder.setTitle("Medication");
                
                // Datasource for populating the list of medication choices
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                		container.getContext(),
                        android.R.layout.simple_list_item_1, medications);
                
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                
                builder.setPositiveButton("New",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            	
                            	final EditText input = new EditText(getActivity());
                            	builder.setMessage("Enter a medication name").setView(input);
                			    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                			        @Override
                                    public void onClick(DialogInterface dialog1, int whichButton) {
                			            
                			        	Editable value = input.getText(); 
                			            
                			        	if (value.toString().trim()=="" || value.toString().trim()==null || value.toString().isEmpty()) {
                			            	Toast toast = Toast.makeText(getActivity(), "Please enter valid information.", Toast.LENGTH_SHORT);
                			    			toast.setGravity(Gravity.CENTER, 0, 0);
                			    			LinearLayout toastLayout = (LinearLayout) toast.getView();
                			    			TextView toastTV = (TextView) toastLayout.getChildAt(0);
                			    			toastTV.setTextSize(30);
                			    			toast.show();
                			            } else {
                			            	updateName(value.toString());
                			            }}}).show();}});
                
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateName(arrayAdapter.getItem(which));
                        dialog.dismiss();
                    }
                });
                
                alertdialog = builder.create();
                alertdialog.show();
                alertdialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, 600);
			}
		});
		
		// Uses a Number Picker for selecting the dosage value (3 significant digits)
		dosageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

        		int hundreds, tens, ones, tenths, hundredths;
	        	double dosageValue = Double.parseDouble(dosage);
	        	Log.i("PROVIDER", dosage);
	        	System.out.println(dosageValue);
	        	
	        	if(dosageValue > 0) {
	        		hundreds 	= (int) (dosageValue*.01 % 10);
	        		tens 		= (int) (dosageValue*.1 % 10);
	        		ones 		= (int) (dosageValue % 10);
	        		tenths 		= (int) (dosageValue*10 % 10);
    				hundredths 	= (int) (dosageValue*100 % 10);
        		} else {
        			hundreds = 1; tens = 0; ones = 0; tenths = 0; hundredths = 0;
        		}
        		
                View pickerFrame = inflater.inflate(R.layout.number_picker_dosage, null);
                final NumberPicker pickerHundreds 	= (NumberPicker) pickerFrame.findViewById(R.id.hundreds);
                final NumberPicker pickerTens 		= (NumberPicker) pickerFrame.findViewById(R.id.tens);
                final NumberPicker pickerOnes 		= (NumberPicker) pickerFrame.findViewById(R.id.ones);
                final NumberPicker pickerTenths 	= (NumberPicker) pickerFrame.findViewById(R.id.tenths);
                final NumberPicker pickerHundredths	= (NumberPicker) pickerFrame.findViewById(R.id.hundredths);

                ArrayList<NumberPicker> pickers = new ArrayList<NumberPicker>();
                pickers.add(pickerHundreds); pickers.add(pickerTens); pickers.add(pickerOnes); pickers.add(pickerTenths); pickers.add(pickerHundredths);
                
                for(NumberPicker picker : pickers) {
                	picker.setMaxValue(9);
                	picker.setMinValue(0);
                	picker.setWrapSelectorWheel(false);
                	picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                }
                pickerHundreds.setValue(hundreds);
                pickerTens.setValue(tens);
                pickerOnes.setValue(ones);
                pickerTenths.setValue(tenths);
                pickerHundredths.setValue(hundredths);

                AlertDialog.Builder builder = new AlertDialog.Builder(container.getContext());
                builder.setView(pickerFrame)
            	.setTitle("Dosage")
            	.setPositiveButton(DEFAULT_FIELD, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	
                        int hundreds1 	= pickerHundreds.getValue();
                        int tens1 		= pickerTens.getValue();
                        int ones1 		= pickerOnes.getValue();
                        int tenths1 		= pickerTenths.getValue();
                        int hundredths1 	= pickerHundredths.getValue();
                        
                        updateDosage(String.valueOf(Double.valueOf(hundreds1 + "" + tens1 + "" + ones1 + "." + tenths1 + "" + hundredths1)));
                    }
            	})
            	.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            		
                    @Override
                    public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
            	})
            	.create().show();
            }
        });
		
		// Uses a Number Picker for selecting the quantity (single digit)
		quantityButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
            public void onClick(View v) {

        		int ones, tenths, hundredths;
	        	double quantityValue = Double.parseDouble(quantity);
	        	
	        	if(quantityValue > 0) {
	        		ones 		= (int) (quantityValue % 10);
	        		tenths 		= (int) (quantityValue*10 % 10);
    				hundredths 	= (int) (quantityValue*100 % 10);
        		} else {
        			ones = 0; tenths = 0; hundredths = 0;
        		}
        		
                View pickerFrame = inflater.inflate(R.layout.number_picker_quantity, null);
                final NumberPicker pickerOnes 		= (NumberPicker) pickerFrame.findViewById(R.id.ones);
                final NumberPicker pickerTenths 	= (NumberPicker) pickerFrame.findViewById(R.id.tenths);
                final NumberPicker pickerHundredths	= (NumberPicker) pickerFrame.findViewById(R.id.hundredths);

                ArrayList<NumberPicker> pickers = new ArrayList<NumberPicker>();
                pickers.add(pickerOnes); pickers.add(pickerTenths); pickers.add(pickerHundredths);
                
                for(NumberPicker picker : pickers) {
                	picker.setMaxValue(9);
                	picker.setMinValue(0);
                	picker.setWrapSelectorWheel(false);
                	picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
                }
                pickerOnes.setValue(ones);
                pickerTenths.setValue(tenths);
                pickerHundredths.setValue(hundredths);

                AlertDialog.Builder builder = new AlertDialog.Builder(container.getContext());
                builder.setView(pickerFrame)
            	.setTitle("Dosage")
            	.setPositiveButton(DEFAULT_FIELD, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	
                        int ones1 		= pickerOnes.getValue();
                        int tenths1 		= pickerTenths.getValue();
                        int hundredths1 	= pickerHundredths.getValue();
                        
                        updateQuantity(String.valueOf(Double.valueOf(ones1 + "." + tenths1 + "" + hundredths1)));
                    }
            	})
            	.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            		
                    @Override
                    public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
            	})
            	.create().show();
            }
        });
		
        formButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	AlertDialog alertdialog;
            	
            	final AlertDialog.Builder builder = new AlertDialog.Builder(container.getContext());
                builder.setTitle("Form");
                
                // Datasource for populating the list of form choices
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                		container.getContext(),
                        android.R.layout.simple_list_item_1, forms);
                
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                
                builder.setPositiveButton("New",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            	
                            	final EditText input = new EditText(getActivity());
                            	builder.setMessage("Enter a medication form").setView(input);
                			    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                			        @Override
                                    public void onClick(DialogInterface dialog1, int whichButton) {
                			            
                			        	Editable value = input.getText(); 
                			            
                			            if (value.toString().trim()=="" || value.toString().trim()==null || value.toString().isEmpty()) {
                			            	Toast toast = Toast.makeText(getActivity(), "Please enter valid information.", Toast.LENGTH_SHORT);
                			    			toast.setGravity(Gravity.CENTER, 0, 0);
                			    			LinearLayout toastLayout = (LinearLayout) toast.getView();
                			    			TextView toastTV = (TextView) toastLayout.getChildAt(0);
                			    			toastTV.setTextSize(30);
                			    			toast.show();
                			            } else {
                			            	updateForm(value.toString());
                			            }}}).show();}});
                
                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateForm(arrayAdapter.getItem(which));
                        dialog.dismiss();
                    }
                });
                
                alertdialog = builder.create();
                alertdialog.show();
                alertdialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, 600);
            }
        });
        
        unitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
            	final AlertDialog.Builder builder = new AlertDialog.Builder(
                        container.getContext());
                builder.setTitle("Unit");
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                		container.getContext(),
                        android.R.layout.simple_list_item_1, units);
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builder.setPositiveButton("New",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            	final EditText input = new EditText(getActivity());
                            	builder.setMessage("Enter a measurement unit").setView(input)
                			    .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                			        @Override
                                    public void onClick(DialogInterface dialog1, int whichButton) {
                			            Editable value = input.getText(); 
                			            
                			            if (value.toString().trim()=="" || value.toString().trim()==null || value.toString().isEmpty()) {
                			            	Toast toast = Toast.makeText(getActivity(), "Please enter valid information.", Toast.LENGTH_SHORT);
                			    			toast.setGravity(Gravity.CENTER, 0, 0);
                			    			LinearLayout toastLayout = (LinearLayout) toast.getView();
                			    			TextView toastTV = (TextView) toastLayout.getChildAt(0);
                			    			toastTV.setTextSize(30);
                			    			toast.show();
                			            } else {
                			            	updateUnit(value.toString());
                			            }}}).show();}});
                builder.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                updateUnit(arrayAdapter.getItem(which));
                                dialog.dismiss();
                            }
                        });
                builder.show();
            }
        });

        // Uses a Number Picker for selecting the minimun threshold value (3 significant digits)
        minThresholdButton.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {

        		int hundreds, tens, ones;
        		double minThresholdVal = Double.parseDouble(minThreshold);
        		Log.i("PROVIDER", minThreshold);
        		System.out.println(minThresholdVal);

        		if(minThresholdVal > 0) {
        			hundreds 	= (int) (minThresholdVal*.01 % 10);
        			tens 		= (int) (minThresholdVal*.1 % 10);
        			ones 		= (int) (minThresholdVal % 10);

        		} else {
        			hundreds = 1; tens = 0; ones = 0;
        		}

        		View pickerFrame = inflater.inflate(R.layout.number_picker_prn, null);
        		final NumberPicker pickerHundreds 	= (NumberPicker) pickerFrame.findViewById(R.id.hundreds);
        		final NumberPicker pickerTens 		= (NumberPicker) pickerFrame.findViewById(R.id.tens);
        		final NumberPicker pickerOnes 		= (NumberPicker) pickerFrame.findViewById(R.id.ones);

        		ArrayList<NumberPicker> pickers = new ArrayList<NumberPicker>();
        		pickers.add(pickerHundreds); pickers.add(pickerTens); pickers.add(pickerOnes);

        		for(NumberPicker picker : pickers) {
        			picker.setMaxValue(9);
        			picker.setMinValue(0);
        			picker.setWrapSelectorWheel(false);
        			picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        		}
        		pickerHundreds.setValue(hundreds);
        		pickerTens.setValue(tens);
        		pickerOnes.setValue(ones);

        		AlertDialog.Builder builder = new AlertDialog.Builder(container.getContext());
        		builder.setView(pickerFrame)
        		.setTitle("Minimun Threshold")
        		.setPositiveButton(DEFAULT_FIELD, new DialogInterface.OnClickListener() {
        			@Override
        			public void onClick(DialogInterface dialog, int which) {

        				int hundreds1 	= pickerHundreds.getValue();
        				int tens1 		= pickerTens.getValue();
        				int ones1 		= pickerOnes.getValue();

        				updateMinThreshold(String.valueOf(Double.valueOf(hundreds1 + "" + tens1 + "" + ones1)));
        			}
        		})
        		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

        			@Override
        			public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
        		})
        		.create().show();
        	}
        });
        
        // Uses a Number Picker for selecting the minimun threshold value (3 significant digits)
        maxThresholdButton.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {

        		int hundreds, tens, ones;
        		double maxThresholdVal = Double.parseDouble(maxThreshold);
        		Log.i("PROVIDER", maxThreshold);
        		System.out.println(maxThresholdVal);

        		if(maxThresholdVal > 0) {
        			hundreds 	= (int) (maxThresholdVal*.01 % 10);
        			tens 		= (int) (maxThresholdVal*.1 % 10);
        			ones 		= (int) (maxThresholdVal % 10);

        		} else {
        			hundreds = 1; tens = 0; ones = 0;
        		}

        		View pickerFrame = inflater.inflate(R.layout.number_picker_prn, null);
        		final NumberPicker pickerHundreds 	= (NumberPicker) pickerFrame.findViewById(R.id.hundreds);
        		final NumberPicker pickerTens 		= (NumberPicker) pickerFrame.findViewById(R.id.tens);
        		final NumberPicker pickerOnes 		= (NumberPicker) pickerFrame.findViewById(R.id.ones);

        		ArrayList<NumberPicker> pickers = new ArrayList<NumberPicker>();
        		pickers.add(pickerHundreds); pickers.add(pickerTens); pickers.add(pickerOnes);

        		for(NumberPicker picker : pickers) {
        			picker.setMaxValue(9);
        			picker.setMinValue(0);
        			picker.setWrapSelectorWheel(false);
        			picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        		}
        		pickerHundreds.setValue(hundreds);
        		pickerTens.setValue(tens);
        		pickerOnes.setValue(ones);

        		AlertDialog.Builder builder = new AlertDialog.Builder(container.getContext());
        		builder.setView(pickerFrame)
        		.setTitle("Maximum Threshold")
        		.setPositiveButton(DEFAULT_FIELD, new DialogInterface.OnClickListener() {
        			@Override
        			public void onClick(DialogInterface dialog, int which) {

        				int hundreds1 	= pickerHundreds.getValue();
        				int tens1 		= pickerTens.getValue();
        				int ones1 		= pickerOnes.getValue();

        				updateMaxThreshold(String.valueOf(Double.valueOf(hundreds1 + "" + tens1 + "" + ones1)));
        			}
        		})
        		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

        			@Override
        			public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
        		})
        		.create().show();
        	}
        });
        
		return view;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
	
	public void updateMinThreshold(String minThresholdString) {
		this.minThreshold = minThresholdString;
		if(Double.parseDouble(minThresholdString) <= 0) {
			this.minThresholdTextView.setTextColor(getResources().getColor(R.color.accent_light_gray));
			this.minThresholdTextView.setText(DEFAULT_FIELD);
		} else {
			this.minThresholdTextView.setText(minThresholdString);
			this.minThresholdTextView.setTextColor(getResources().getColor(R.color.dark_text));
		}
	}
	public void updateMaxThreshold(String maxThresholdString) {
		this.maxThreshold = maxThresholdString;
		if(Double.parseDouble(maxThresholdString) <= 0) {
			this.maxThresholdTextView.setTextColor(getResources().getColor(R.color.accent_light_gray));
			this.maxThresholdTextView.setText(DEFAULT_FIELD);
		} else {
			this.maxThresholdTextView.setText(maxThresholdString);
			this.maxThresholdTextView.setTextColor(getResources().getColor(R.color.dark_text));
		}
	}
}

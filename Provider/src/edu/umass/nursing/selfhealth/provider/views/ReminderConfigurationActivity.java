/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import org.joda.time.LocalTime;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;
import edu.umass.nursing.selfhealth.provider.database.DatabaseController;
import edu.umass.nursing.selfhealth.provider.models.Configuration;
import edu.umass.nursing.selfhealth.provider.models.DayEnum;
import edu.umass.nursing.selfhealth.provider.models.MedicationConfiguration;
import edu.umass.nursing.selfhealth.provider.models.Schedule;
import edu.umass.nursing.selfhealth.provider.models.SensorEnum;
import edu.umass.nursing.selfhealth.provider.models.SurveyConfiguration;
import edu.umass.nursing.selfhealth.provider.models.SurveyEnum;
import edu.umass.nursing.selfhealth.provider.models.TypeEnum;
import edu.umass.nursing.selfhealth.provider.views.ReminderStateFragment.ReminderListener;
import edu.umass.nursing.selfhealth.provider.R;

// TODO: Finish implementing interface logic for checkbox

public class ReminderConfigurationActivity<T extends Configuration, E extends Schedule>
extends ActivityWithActionBar implements  ReminderListener {
    
    DatabaseController controller;
    
    //Datasource
    private T configuration;
    //	private int patientID;
    
    // Extracted Datasource Components -- Combine back into Configuration when saving
    private TypeEnum type;
    ArrayList<DayEnum> days;
    
    // Private conditionals
    private boolean daily;
    private boolean isEditTimeMode;
    private boolean isPRNEnabled;
    
    // VIEW COMPONENTS:
    public Button editTimesButton;
    public Button chooseFrequencyButton;
    public Button setTimesButton;
    public Button setEndDateButton; // STEPHEN added 12/5 used for setting the end date of a medication
    public RelativeLayout prnConfigurationButton;
    
    // STEPEHEN ADDED 12/7
    // Datepicker component and variables
    private static final int DATE_DIALOG_ID = 999; // arbitrary, used for the date picker
    private int day;
    private int month;
    private int year;
    private TextView endDateText;
    
    // A toggle between daily and weekly
    public RelativeLayout frequencyButton;
    public TextView frequencyText;
    public ImageView frequencyArrow;
    
    // Main view below upper fragment -- where a schedule is configured
    public LinearLayout scheduleContainer;
    
    // Contains the day-toggle buttons
    public LinearLayout dayContainer;
    
    // Contains the times
    public ListView timeListView;
    
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, getTitle((T) getIntent().getExtras().get("CONFIGURATION")));
        setContentView(R.layout.activity_configuration);
        
        // Prevents awkward screen readjustment when the keyboard appears.  Note, this will cover elements at the bottom.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        
        controller = DatabaseController.getInstance(this);
        
        /** Prepare the datasource **/
        this.configuration = (T) getIntent().getExtras().get("CONFIGURATION");
        
        days = (ArrayList<DayEnum>) configuration.getSchedule().getDays();
        
        /** Create the fragments needed by the Activity **/
        makeFragments();
        
        /** Prepare the Schedule Portion of the view -- we can set it visible or invisible **/
        scheduleContainer = (LinearLayout) this.findViewById(R.id.scheduleWrapper);
        editTimesButton = (Button) this.findViewById(R.id.edit_button);
        prnConfigurationButton = (RelativeLayout) this.findViewById(R.id.prn_config_button);
        
        editTimesButton.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                toggleEditMode(v);
            }
        });
        prnConfigurationButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpPrn(v);
            }
        });
        
        /** Setup Activity for Use **/
        adjustScheduleView();
        setupFrequencyButton();
        setupDaySelector();
        updateEditTimeMode();
        setupTimeSelector();
        setupEndDateButton(); // STEPHEN ADDED 12/6
        
    }
    
    private String getTitle(T configuration1) {
        this.type = (TypeEnum) getIntent().getExtras().get("TYPE");
        if(type == TypeEnum.MEDICATION) {
            return "Configure Medication";
        } else if(type == TypeEnum.PHYSICAL_REPORT) {
            return SensorEnum.getEnum(configuration1.getId()).toString();
        } else {
            return SurveyEnum.getEnum(configuration1.getId()).toString();
        }
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        updateFragments();
    }
    
    /** FRAGMENT LOGIC ****************************************************************************/
    /** FRAGMENT LOGIC ****************************************************************************/
    /** FRAGMENT LOGIC ****************************************************************************/
    
    private void makeFragments() {
        
        /** Apply the correct Fragment based on the Type of the configuration **/
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
        .beginTransaction();
        
        if ((type == TypeEnum.SURVEY) || (type == TypeEnum.PHYSICAL_REPORT)) {
            ReminderStateFragment fragment = new ReminderStateFragment();
            fragmentTransaction.add(R.id.fragmentFrame, fragment);
            fragmentTransaction.commit();
        } else if ((type == TypeEnum.MEDICATION)) {
            ConfigureMedicationFragment fragment = new ConfigureMedicationFragment();
            fragmentTransaction.add(R.id.fragmentFrame, fragment);
            fragmentTransaction.commit();
        }
    }
    
    /** Update our Fragment with the correct information by calling their public setup methods **/
    private void updateFragments() {
        if ((type == TypeEnum.SURVEY) || (type == TypeEnum.PHYSICAL_REPORT)) {
            ReminderStateFragment fragment = (ReminderStateFragment) getFragmentManager()
            .findFragmentById(R.id.fragmentFrame);
            fragment.update(configuration.isRemindersEnabled());
        } else if ((type == TypeEnum.MEDICATION)) {
            ConfigureMedicationFragment fragment = (ConfigureMedicationFragment) getFragmentManager()
            .findFragmentById(R.id.fragmentFrame);
            fragment.updateName(((MedicationConfiguration)configuration).getMedication().getName());
            fragment.updateForm(((MedicationConfiguration)configuration).getMedication().getForm());
            fragment.updateDosage(Double.toString(((MedicationConfiguration)configuration).getMedication().getDosage()));
            fragment.updateUnit(((MedicationConfiguration)configuration).getMedication().getUnits());
            fragment.updateQuantity(Double.toString(((MedicationConfiguration)configuration).getMedication().getQuantity()));
        }
    }
    
    /**********************************************************************************************/
    
    /** INTERFACE LOGIC ****************************************************************************/
    /** INTERFACE LOGIC ****************************************************************************/
    /** INTERFACE LOGIC ****************************************************************************/
    
    /** Show the schedule views if reminders are enabled, otherwise hide them. **/
    private void adjustScheduleView() {
        if (configuration.isRemindersEnabled()) {
            scheduleContainer.setVisibility(View.VISIBLE);
        } else {
            scheduleContainer.setVisibility(View.GONE);
        }
    }
    
    /** Interface Methods **/
    @Override
    public void onReminderToggled(boolean reminder) {
        this.configuration.setRemindersEnabled(reminder);
        adjustScheduleView();
    }
    
    /** Prepares the Frequency buttons functionality **/
    private void setupFrequencyButton() {
        
        frequencyText = (TextView) this.findViewById(R.id.frequencyText);
        frequencyArrow = (ImageView) this.findViewById(R.id.frequencyArrow);
        frequencyButton = (RelativeLayout) this.findViewById(R.id.frequencyButton);
        chooseFrequencyButton = (Button) this.findViewById(R.id.frequencyDetailButton);
        dayContainer = (LinearLayout) this.findViewById(R.id.DaySelectorContainer);
        
        if (configuration.getSchedule().getDays().size() == 7) {
            daily = true;
        }
        
        adjustFrequencyButton();
        adjustDayContainerView();
        
        // BUTTON:
        frequencyButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                daily = !daily;
                updateDaysForFrequency();
                adjustFrequencyButton();
                adjustDayContainerView();
            }
        });
    }
    
    /** Update the toggle switches and frequency. **/
    private void updateDaysForFrequency() {
        if(daily) {
            days.clear();
            for (int i = 0; i < 7; i++) {
                DayEnum d = DayEnum.getEnum(i);
                days.add(d);
            }
        } else {
            days.clear();
            days.add(DayEnum.MONDAY);
        }
        for (int i = 0; i < 7; i++) {
            final ToggleButton dayButton = (ToggleButton) dayContainer.getChildAt(i);
            toggleDay(i, dayButton);
        }
    }
    
    /** Update the frequency button to show Daily or Weekly **/
    private void adjustFrequencyButton() {
        if (daily) {
            chooseFrequencyButton.setVisibility(View.INVISIBLE);
            frequencyText.setText("Daily");
            frequencyArrow.setImageResource(R.drawable.red_down_arrow);
        } else {
            chooseFrequencyButton.setVisibility(View.VISIBLE);
            frequencyText.setText(getWeeklyString());
            frequencyArrow.setImageResource(R.drawable.red_up_arrow);
        }
    }
    
    /** Sets the visibility of the Day Selector based on the selected Frequency **/
    private void adjustDayContainerView() {
        if (daily) {
            dayContainer.setVisibility(View.GONE);
        } else {
            dayContainer.setVisibility(View.VISIBLE);
        }
    }
    
    /** When clicked, the provider may choose 'Every X weeks' when weekly is displayed **/
    public void chooseFrequency(View v) {
        
        View pickerFrame = LayoutInflater.from(this).inflate(R.layout.number_picker_frequency, null);
        final NumberPicker picker = (NumberPicker) pickerFrame.findViewById(R.id.selector);
        
        picker.setMaxValue(4);
        picker.setMinValue(1);
        picker.setWrapSelectorWheel(false);
        picker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        picker.setValue(configuration.getSchedule().getWeekRecurrence()); // TODO : Get value from configuration
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(pickerFrame)
        .setTitle("Frequency")
        .setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                
                int everyXWeeks = picker.getValue();
                configuration.getSchedule().setWeekRecurrence(everyXWeeks);
                adjustFrequencyButton();
            }
        })
        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            
            @Override
            public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
        })
        .create().show();
    }
    
    private String getWeeklyString() {
        if(configuration.getSchedule().getWeekRecurrence() == 1) {
            return "Weekly";
        } else {
            return "Every " + configuration.getSchedule().getWeekRecurrence() + " weeks";
        }
    }
    
    private void setupDaySelector() {
        
        for (int i = 0; i < 7; i++) {
            final int j = i;
            final ToggleButton dayButton = (ToggleButton) dayContainer.getChildAt(i);
            dayButton.setOnClickListener(new OnClickListener() {
                @Override
                // BUTTON:
                public void onClick(View v) {
                    DayEnum d = DayEnum.getEnum(j);
                    if (days.contains(d)) {
                        days.remove(d);
                    } else {
                        days.add(d);
                    }
                    toggleDay(j, dayButton);
                }
            });
            toggleDay(j, dayButton);
        }
    }
    
    /** Updates the view and local model for the Day Toggled **/
    private void toggleDay(int day, ToggleButton dayButton) {
        
        if (days.contains(DayEnum.getEnum(day))) {
            dayButton.setBackgroundResource(R.color.accent_red);
            dayButton.setTextColor(getResources().getColor(R.color.foreground));
        } else {
            dayButton.setBackgroundResource(R.drawable.border);
            dayButton.setTextColor(getResources().getColor(R.color.dark_text));
        }
    }
    
    // BUTTON:
    // Save any components back to the configuration and pass it to the controller.
    public void saveConfiguration(View v) {
        
        if(configuration.isRemindersEnabled() && days.size()==0) {
            
            Toast toast = Toast.makeText(this, "You must choose at least one day for a weekly reminder.", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            LinearLayout toastLayout = (LinearLayout) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(30);
            toast.show();
        } else {
            // Add the schedule back into the configuration
            if(daily) {
                // If it happens every day, it happens every week, that's the way the cookie crumbles--at least in this case.
                configuration.getSchedule().setWeekRecurrence(1);
            }
            configuration.getSchedule().setDays(days);
            // TODO: have a setTimes() method here?
            
            if(configuration.getClass() == SurveyConfiguration.class) {
                //				controller.setConfiguration((SurveyConfiguration) configuration);
                controller.setConfiguration(patientID, (SurveyConfiguration) configuration);
                finish();
            } else if(configuration.getClass() == MedicationConfiguration.class) {
                
                ConfigureMedicationFragment fragment = (ConfigureMedicationFragment) getFragmentManager()
                .findFragmentById(R.id.fragmentFrame);
                
                if(fragment.name == null ||
                   fragment.form == null ||
                   fragment.dosage == null ||
                   fragment.unit == null ||
                   fragment.quantity == null) {
                    
                    Toast toast = Toast.makeText(this, "Please enter valid information.", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    LinearLayout toastLayout = (LinearLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTextSize(30);
                    toast.show();
                    
                } else {
                    ((MedicationConfiguration) configuration).getMedication().setName(fragment.name);
                    ((MedicationConfiguration) configuration).getMedication().setForm(fragment.form);
                    ((MedicationConfiguration) configuration).getMedication().setDosage(Double.parseDouble(fragment.dosage));
                    ((MedicationConfiguration) configuration).getMedication().setUnits(fragment.unit);
                    ((MedicationConfiguration) configuration).getMedication().setQuantity(Double.parseDouble(fragment.quantity));
                    
                    //					controller.setConfiguration((MedicationConfiguration) configuration);
                    controller.setConfiguration(patientID, (MedicationConfiguration) configuration);
                    finish();
                }
                
            } else {
                controller.setConfiguration(patientID, configuration);
                finish();
            }
            
        }
    }
    
    /** Prepares the TimeList and TimePicker functionality **/
    private void setupTimeSelector() {
        
        setTimesButton = (Button) findViewById(R.id.setTimeButton);
        ArrayList<LocalTime> scheduleTimes = (ArrayList<LocalTime>) configuration.getSchedule().getTimes();
        
        timeListView = (ListView) findViewById(R.id.timeList);
        timeListView.setAdapter(new timeListAdapter(this, R.id.timeList,scheduleTimes));
    }
    
    /**
     * Connect the Time ListView with the DataSource for scheduled reminder
     * times
     **/
    private class timeListAdapter extends ArrayAdapter<LocalTime> {
        
        public timeListAdapter(Context context, int adaptedViewResourceID,
                               ArrayList<LocalTime> times) {
            super(context, adaptedViewResourceID, times);
            timeSchedule = times;
        }
        
        private ArrayList<LocalTime> timeSchedule;
        
        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            
            LayoutInflater inflater = getLayoutInflater();
            convertView = inflater.inflate(R.layout.row_delete, parent, false);
            
            /** Prepare the cell layout **/
            final RelativeLayout row = (RelativeLayout) convertView
            .findViewById(R.id.row_background);
            if (position == 0) {
                row.setBackgroundResource(R.drawable.border);
            } else {
                row.setBackgroundResource(R.drawable.border_no_top);
            }
            
            final LocalTime time = timeSchedule.get(position);
            
            /** Show the time in a simple format **/
            TextView time_text = (TextView) convertView
            .findViewById(R.id.row_text);
            time_text.setText(time.toString("h:mm a", Locale.US));
            
            /**
             * Prepare the delete and context buttons based on the current state
             * of EditMode
             **/
            final ImageButton deleteButton = (ImageButton) convertView
            .findViewById(R.id.delete_button);
            ImageView disclosureArrow = (ImageView) convertView
            .findViewById(R.id.down_arrow);
            
            final TimePickerDialog.OnTimeSetListener timeListener = new TimePickerDialog.OnTimeSetListener() {
                
                @Override
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {
                    
                    if (timeSchedule.contains(
                                              new LocalTime(selectedHour, selectedMinute))) {
                        // TODO : Display an error--cannot add the
                    } else {
                        timeSchedule.set(position,(new LocalTime(selectedHour, selectedMinute)));
                        Collections.sort(timeSchedule);
                        ((BaseAdapter) timeListView.getAdapter())
                        .notifyDataSetChanged();
                        configuration.getSchedule().addTime(timeSchedule.get(position));
                    }
                }
            };
            
            row.setOnClickListener(new OnClickListener() {
                @Override
                // BUTTON:
                public void onClick(View v) {
                    new TimePickerDialog(getContext(), timeListener, time.getHourOfDay(),
                                         time.getMinuteOfHour(), false).show();
                    
                }
            });
            
            if (isEditTimeMode) {
                disclosureArrow.setVisibility(View.GONE);
                deleteButton.setVisibility(View.VISIBLE);
                row.setEnabled(false);
                deleteButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteButton.setVisibility(View.GONE);
                        row.setVisibility(View.GONE);
                        timeSchedule.remove(position);
                        if(timeSchedule.size()==1){
                            toggleEditMode(v);
                        } else {
                            updateEditTimeMode();
                        }
                        notifyDataSetChanged();
                    }
                });
            } else {
                row.setEnabled(true);
                disclosureArrow.setVisibility(View.VISIBLE);
                deleteButton.setVisibility(View.GONE);
            }
            return convertView;
        }
        
        @Override
        public int getCount() {
            return timeSchedule.size();
        }
    }
    
    /** Changes whether Times can be deleted or modified **/
    public void toggleEditMode(View v) {
        isEditTimeMode = !isEditTimeMode;
        updateEditTimeMode();
        ((BaseAdapter) timeListView.getAdapter()).notifyDataSetChanged();
        System.out.println("REMOVE PRESSED!");
    }
    
    public void setUpPrn(View v) {
        isPRNEnabled = !isPRNEnabled;
        System.out.println("REMOVE PRESSED!");
    }
    
    private void updateEditTimeMode() {
        
        if(configuration.getSchedule().getTimes().size()==1) {
            this.editTimesButton.setVisibility(View.GONE);
        } else {
            this.editTimesButton.setVisibility(View.VISIBLE);
            if(isEditTimeMode) {
                editTimesButton.setText("Done");
            } else {
                editTimesButton.setText("Remove");
            }
        }
    }
    
    /******************* TIME *******************/
    
    private final String BREAKFAST 	= "Breakfast";
    private final String LUNCH 		= "Lunch";
    private final String DINNER 	= "Dinner";
    
    private LocalTime fromTimeChoice(String choice) {
        if(choice == BREAKFAST) return new LocalTime(7, 0);
        if(choice == LUNCH) 	return new LocalTime(12, 0);
        if(choice == DINNER) 	return new LocalTime(18, 0);
        return null;
    }
    
    private String[] timeChoices = {BREAKFAST, LUNCH, DINNER};
    
    public void setTimes(View v) {
        
        final ArrayList<String> selectedTimes = new ArrayList<String>();
        
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Time")
        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
        })
        .setNeutralButton("Add...", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { 
                new TimePickerDialog(ReminderConfigurationActivity.this, timePickerListener, 12, 0, false).show();
                dialog.dismiss(); 
            }
        })
        .setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) { 
                
                configuration.getSchedule().getTimes().clear();
                
                if(selectedTimes.size() == 0) {
                    configuration.getSchedule().addTime(new LocalTime(12, 0));
                } else {
                    for(String choice : selectedTimes) {
                        
                        configuration.getSchedule().addTime(fromTimeChoice(choice));
                        Collections.sort(configuration.getSchedule().getTimes());
                    }
                }
                updateEditTimeMode();
                ((BaseAdapter) timeListView.getAdapter())
                .notifyDataSetChanged();
                dialog.dismiss(); 
            }
        })
        .setMultiChoiceItems(timeChoices, null, new OnMultiChoiceClickListener() {
            
            @Override
            public void onClick(DialogInterface dialog, int which,
                                boolean isChecked) {
                
                if(isChecked) {
                    if(!selectedTimes.contains(timeChoices[which])) {
                        selectedTimes.add(timeChoices[which]);
                    }
                } else {
                    selectedTimes.remove(timeChoices[which]);
                }
            }
        })
        .create().show();
    }
    
    /** Opens the Dialog to select a new time to add to the schedule **/
    public void addTime(View v) {
        new TimePickerDialog(this, timePickerListener, 12, 0, false).show();
    }
    
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
    
    @Override
    public void onTimeSet(TimePicker view, int selectedHour,
                          int selectedMinute) {
    
    if (!configuration.getSchedule().getTimes().contains( new LocalTime(selectedHour, selectedMinute))) {
				configuration.getSchedule().addTime(new LocalTime(selectedHour, selectedMinute));
				Collections.sort(configuration.getSchedule().getTimes());
				updateEditTimeMode();
				((BaseAdapter) timeListView.getAdapter())
    .notifyDataSetChanged();
}
}
};

/************* STEPHEN - MEDICATION END DATE *************/
// TODO: Finish implementing this section 

public void setEndDate(View v) {
}

private void setupEndDateButton() {
setEndDateButton = (Button) findViewById(R.id.setEndDateButton);
endDateText = (TextView) findViewById(R.id.endDateText);
endDateText.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

final Calendar c = Calendar.getInstance();
year = c.get(Calendar.YEAR);
month = c.get(Calendar.MONTH);
day = c.get(Calendar.DAY_OF_MONTH);

endDateText.setText(new StringBuilder().append(month+1).append("-").append(day).append("-").append(year));


setEndDateButton.setOnClickListener(new OnClickListener() {
@SuppressWarnings("deprecation")
@Override
public void onClick(View v) {
showDialog(DATE_DIALOG_ID);
}

});
}


private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
@Override
public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
year = selectedYear;
month = selectedMonth;
day = selectedDay;

endDateText.setText(new StringBuilder().append(month+1).append("-").append(day).append("-").append(year));
}
};


@Override
protected Dialog onCreateDialog(int id) {
switch (id) {
case DATE_DIALOG_ID:
// set the date picker as current date
return new DatePickerDialog(this, datePickerListener, year, month, day);
}
return null;
}

}
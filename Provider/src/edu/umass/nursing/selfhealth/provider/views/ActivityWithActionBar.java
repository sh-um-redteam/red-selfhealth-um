/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;

import edu.umass.nursing.selfhealth.provider.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class ActivityWithActionBar extends Activity {
	
	int patientID;
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("PATIENT_ID", patientID);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState == null) {
			patientID = getIntent().getIntExtra("PATIENT_ID", -1);
		} else {
			patientID = savedInstanceState.getInt("PATIENT_ID");
		}
	}
	
    protected void onCreate(Bundle savedInstanceState, String contextTitle) {
        super.onCreate(savedInstanceState);
        setupActionBar(contextTitle);
        
        if(savedInstanceState == null) {
			patientID = getIntent().getIntExtra("PATIENT_ID", -1);
		} else {
			patientID = savedInstanceState.getInt("PATIENT_ID");
		}
	}
	
	private void setupActionBar(String title){
	    	
    	ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
 
        View mCustomView = mInflater.inflate(R.layout.action_bar_healthos, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.TitleText);
        mTitleTextView.setText(title);

        // Pop the activities back to the current Patient Profile activity
        View barButton =  mCustomView.findViewById(R.id.BarButton); 
        barButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	Intent intent = new Intent(view.getContext(), PatientProfileActivity.class);
            	intent.putExtra("PATIENT_ID", patientID);
            	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            	startActivity(intent);
            }
        });
        
        // Pop the activity from the stack when 'Back' is pressed
        View backButton =  mCustomView.findViewById(R.id.BackButton); 
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	finish();
            }
        });

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }
	
	public void hideButton(String title) {
		ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
 
        View mCustomView = mInflater.inflate(R.layout.action_bar_healthos, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.TitleText);
        mTitleTextView.setText(title);

        RelativeLayout barButton =  (RelativeLayout) mCustomView.findViewById(R.id.BarButton); 
        barButton.findViewById(R.id.ButtonText).setVisibility(View.GONE);
        barButton.findViewById(R.id.icon).setVisibility(View.GONE);
        
        View backButton =  mCustomView.findViewById(R.id.BackButton); 
        backButton.setVisibility(View.GONE);
        MarginLayoutParams params = (MarginLayoutParams) mTitleTextView.getLayoutParams();
        params.leftMargin = 30;
        mTitleTextView.setLayoutParams(params);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
	}
}

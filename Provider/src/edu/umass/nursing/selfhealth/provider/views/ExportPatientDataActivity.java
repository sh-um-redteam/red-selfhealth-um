/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.views;


import edu.umass.nursing.selfhealth.provider.database.DatabaseController
;

import edu.umass.nursing.selfhealth.provider.R;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;


import android.R.bool;
import android.app.Activity;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ExportPatientDataActivity extends ActivityWithActionBar {
    
	/*private BluetoothAdapter bluetoothAdapter;

	  ListView listViewPaired;
	    ListView listViewDetected;
	    ArrayList<String> arrayListpaired;
	    Button buttonSearch,buttonOn,buttonDesc,buttonOff;
	    ArrayAdapter<String> adapter,detectedAdapter;
	    static HandleSeacrh handleSeacrh;
	    BluetoothDevice bdDevice;
	    BluetoothClass bdClass;
	    ArrayList<BluetoothDevice> arrayListPairedBluetoothDevices;
	    ArrayList<BluetoothDevice> arrayListBluetoothDevices = null;*/
	


	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("PATIENT_ID", patientID);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, "Export Patient Data");
		
		if(savedInstanceState == null) {
			patientID = getIntent().getIntExtra("PATIENT_ID", -1);
		} else {
			patientID = savedInstanceState.getInt("PATIENT_ID");
		}
		setContentView(R.layout.activity_export_patient_data);
		
		
       // bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

      

	}
	
	
	
	// Export the configuration file for the current patient
	public void exportViaSD(View v) {

		// Write the configuration file
//		DatabaseController.getInstance(this).saveConfigurationToExternal();
		DatabaseController.getInstance(this).saveConfigurationToExternal(patientID);
		
		// Notify the User that it has been saved
		Toast toast = Toast.makeText(this, "Saved Configuration to SD Card.", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		LinearLayout toastLayout = (LinearLayout) toast.getView();
		TextView toastTV = (TextView) toastLayout.getChildAt(0);
		toastTV.setTextSize(30);
		toast.show();
	}
	
	public void exportViaBluetooth(View v){
		// Notify the User that it has been saved
		Intent myIntent = new Intent(this, BluetoothConfigActivity.class);
		startActivity(myIntent);


	}
	
	
	  
	
	/*public void turnOnBluetooth(View v){
		if (!bluetoothAdapter.isEnabled()) {
	         Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	         startActivityForResult(turnOn, 0);
	         Toast.makeText(getApplicationContext(),"Bluetooth On" 
	         ,Toast.LENGTH_LONG).show();
	      }
	      else{
	         Toast.makeText(getApplicationContext(),"Already on",
	         Toast.LENGTH_LONG).show();
	         }
	}
	
	public void turnOffBluetooth(View v){
		 if(bluetoothAdapter.isEnabled())
	        {
	            bluetoothAdapter.disable();
	            Toast.makeText(getApplicationContext(),"Bluetooth Off",
	       		Toast.LENGTH_LONG).show();
	        }
		 else{
			 Toast.makeText(getApplicationContext(),"Already off",
			 Toast.LENGTH_LONG).show();
		 }
	}
	
	
	 private BroadcastReceiver myReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            Message msg = Message.obtain();
	            String action = intent.getAction();
	            if(BluetoothDevice.ACTION_FOUND.equals(action)){
	                Toast.makeText(context, "ACTION_FOUND", Toast.LENGTH_SHORT).show();

	                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	                try
	                {
	                    //device.getClass().getMethod("setPairingConfirmation", boolean.class).invoke(device, true);
	                    //device.getClass().getMethod("cancelPairingUserInput", boolean.class).invoke(device);
	                }
	                catch (Exception e) {
	                    Log.i("Log", "Inside the exception: ");
	                    e.printStackTrace();
	                }

	                if(arrayListBluetoothDevices.size()<1) // this checks if the size of bluetooth device is 0,then add the
	                {                                           // device to the arraylist.
	                    detectedAdapter.add(device.getName()+"\n"+device.getAddress());
	                    arrayListBluetoothDevices.add(device);
	                    detectedAdapter.notifyDataSetChanged();
	                }
	                else
	                {
	                    boolean flag = true;    // flag to indicate that particular device is already in the arlist or not
	                    for(int i = 0; i<arrayListBluetoothDevices.size();i++)
	                    {
	                        if(device.getAddress().equals(arrayListBluetoothDevices.get(i).getAddress()))
	                        {
	                            flag = false;
	                        }
	                    }
	                    if(flag == true)
	                    {
	                        detectedAdapter.add(device.getName()+"\n"+device.getAddress());
	                        arrayListBluetoothDevices.add(device);
	                        detectedAdapter.notifyDataSetChanged();
	                    }
	                }
	            }           
	        }
	    };
	    
    private void startSearching() {
        Log.i("Log", "in the start searching method");
        IntentFilter intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        ExportPatientDataActivity.this.registerReceiver(myReceiver, intentFilter);
        bluetoothAdapter.startDiscovery();
    }
	
    
    //NOT IMPLEMENTED YET
    public void findDevices(View v){ 
    	Toast toast = Toast.makeText(this, "Listing Available Devices...", Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		LinearLayout toastLayout = (LinearLayout) toast.getView();
		TextView toastTV = (TextView) toastLayout.getChildAt(0);
		toastTV.setTextSize(30);
		toast.show();
        /*arrayListBluetoothDevices.clear();
        startSearching();
    }
    
	  public void makeDiscoverable(View v) {
		  Toast toast = Toast.makeText(this, "Making device discoverable by others", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			LinearLayout toastLayout = (LinearLayout) toast.getView();
			TextView toastTV = (TextView) toastLayout.getChildAt(0);
			toastTV.setTextSize(30);
			toast.show();
	       Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
	        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
	        startActivity(discoverableIntent);
	        Log.i("Log", "Discoverable "); 
	    }
	  
	  class HandleSeacrh extends Handler
	    {
	        @Override
	        public void handleMessage(Message msg) {
	            switch (msg.what) {
	            case 111:

	                break;

	            default:
	                break;
	            }
	        }
	    }*/
	  
	
	
}

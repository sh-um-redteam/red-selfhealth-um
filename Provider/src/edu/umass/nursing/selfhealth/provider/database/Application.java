/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.database;

import android.content.Context;

/**
 *	A bypass for instantiating the DatabaseController using a Context
 */
public class Application extends android.app.Application {

    private static Application instance;

    public Application() {
    	instance = this;
    }

    public static Context getContext() {
    	return instance;
    }
}

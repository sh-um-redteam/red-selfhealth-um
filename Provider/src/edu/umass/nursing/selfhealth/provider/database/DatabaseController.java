/**
 * Copyright (c) 2014 College of Nursing University of Massachusetts. All rights
 * reserved.
 **/
package edu.umass.nursing.selfhealth.provider.database;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import edu.umass.nursing.selfhealth.provider.models.Configuration;
import edu.umass.nursing.selfhealth.provider.models.DayEnum;
import edu.umass.nursing.selfhealth.provider.models.Medication;
import edu.umass.nursing.selfhealth.provider.models.MedicationConfiguration;
import edu.umass.nursing.selfhealth.provider.models.PRNMedication;
import edu.umass.nursing.selfhealth.provider.models.PRNMedicationConfiguration;
import edu.umass.nursing.selfhealth.provider.models.Patient;
import edu.umass.nursing.selfhealth.provider.models.PhysicalReportConfiguration;
import edu.umass.nursing.selfhealth.provider.models.Schedule;
import edu.umass.nursing.selfhealth.provider.models.SensorEnum;
import edu.umass.nursing.selfhealth.provider.models.SurveyConfiguration;
import edu.umass.nursing.selfhealth.provider.models.SurveyEnum;
import edu.umass.nursing.selfhealth.provider.models.TypeEnum;

public class DatabaseController extends SQLiteAssetHelper {

	// TODO Normalize the Database--table needed for times
	// 		Days of the week can be columns on each configuration. and more and more and more... I was a wee-n00b when making this DB.

	//////// DATABASE TABLE CONSTANTS /////////

	private final String VERSION_TABLE = 		"Configuration_Version";
	private final String VERSION = 				"version";

	private final String PATIENT_TABLE = 		"Patients";
	private final String PATIENT_ID = 			"Patient_ID";
	private final String FIRST_NAME = 			"First_Name";
	private final String LAST_NAME = 			"Last_Name";
	private final String GENDER = 				"Gender";
	private final String DATE_OF_BIRTH = 		"Date_Of_Birth";

	private final String MEDICATION_TABLE = 	"Medication_Configurations";
	private final String PRN_TABLE =            "PRN_Configurations" ;
	private final String MEDICATION_ID = 		"Medication_ID";
	private final String ENABLED = 				"Enabled";
	private final String FREQUENCY = 			"Frequency";
	private final String TIMES = 				"Times";
	private final String EVERYXWEEKS =			"EveryXWeeks";
	private final String NAME = 				"Name";
	private final String FORM = 				"Form";
	private final String DOSAGE = 				"Dosage";
	private final String UNITS = 				"Units";
	private final String QUANTITY = 			"Quantity";

	private final String MEDICATION_LOOKUP = 	"Medication_Lookup";

	private final String SURVEY_TABLE = 		"Survey_Configurations";
	private final String SURVEY_ENUM_ID = 		"Survey_Enum_ID";
	private final String REMINDERS_ENABLED =	"Reminders_Enabled";

	private final String PHYSICAL_REPORT_TABLE ="Sensor_Configurations";
	private final String SENSOR_ENUM_ID = 		"Sensor_Enum_ID";
	@SuppressWarnings("unused")
	private final String PERIPHERAL_ID = 		"Peripheral_UDID";

	////////DATABASE TABLE CONSTANTS END /////////

	/**
	 * Enforces singleton Database Controller for Provider use
	 */
	private static DatabaseController instance = null;

	public static synchronized DatabaseController getInstance(Context c) {
		if (instance == null) {
			instance = new DatabaseController(c.getApplicationContext());
		}
		return instance;
	}

	// Database Properties
	private static final String DATABASE_NAME = "noob.db";
	private static final int DATABASE_VERSION = 3;
	private static SQLiteDatabase database;

	private DatabaseController(Context c) {
		super(c, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Database must be opened for each set of operations 
	private void openDatabase() {
		if(database == null) {
			database = this.getWritableDatabase();
		} 
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	/***************************** CONTROLLER LOGIC *****************************/
	/***************************** CONTROLLER LOGIC *****************************/
	/***************************** CONTROLLER LOGIC *****************************/

	// Used for upserting a patient (when id is negative, add a new one, otherwise, overwrite info)
	public int updatePatient(Patient patient) {

		if(patient == null) {
			return -1;
		}
		openDatabase();

		ContentValues values = new ContentValues();
		values.put(this.FIRST_NAME, patient.getFirstName());
		values.put(this.LAST_NAME, patient.getLastName());
		values.put(this.GENDER, patient.getGender());

		LocalDate dob = patient.getDateOfBirth();
		String dateOfBirth = 	dob.getMonthOfYear()+"-"+
				dob.getDayOfMonth()+"-"+
				dob.getYear();

		values.put(this.DATE_OF_BIRTH, dateOfBirth);

		int patientID = patient.getId();

		if(patientID < 0) { // A NEW patient must be inserted

			String query = "SELECT MAX(" + this.PATIENT_ID + ") FROM " + this.PATIENT_TABLE;
			Cursor c = database.rawQuery(query, null);

			if (c.moveToFirst()){
				patientID = c.getInt(0);
				patientID++;
			} 
			c.close();
			values.put(this.PATIENT_ID, patientID);
			database.insert(this.PATIENT_TABLE, null, values);

			this.initializePatientConfigurations(patientID);

		} else { // An existing patient must be updated

			values.put(this.PATIENT_ID, patient.getId());
			database.update(this.PATIENT_TABLE, 
					values, 
					this.PATIENT_ID + " = " + patient.getId(), 
					null);

			Log.i("PATIENT", "Patient updated in the database.");
		}
		return patientID;
	}

	public Patient getPatient(int patientID) {

		String query = "SELECT * FROM " + PATIENT_TABLE + " WHERE " + PATIENT_ID + " = " + patientID;

		Cursor c = database.rawQuery(query, null);

		if (c.moveToFirst()){

			String dob = c.getString(4);
			String[] parts = dob.split("-");
			int month = Integer.parseInt(parts[0]); 
			int day = Integer.parseInt(parts[1]);
			int year = Integer.parseInt(parts[2]);
			LocalDate dateOfBirth = new LocalDate(year, month, day);

			// Reconstruct a Patient
			Patient patient = new Patient(c.getInt(0), c.getString(1), 
					c.getString(2), c.getString(3), dateOfBirth);

			c.close();
			return patient;
		}
		c.close();
		return null;
	}

	// Does what it says.
	public List<Patient> getAllPatients() {

		openDatabase();

		String query = "SELECT * FROM " + this.PATIENT_TABLE;
		Cursor c = database.rawQuery(query, null);

		ArrayList<Patient> patients = new ArrayList<Patient>();

		if (c.moveToFirst()){
			while (!c.isAfterLast()) {

				// Reconstruct the LocalDate Date of Birth
				String dob = c.getString(4);
				String[] parts = dob.split("-");
				int month = Integer.parseInt(parts[0]); 
				int day = Integer.parseInt(parts[1]);
				int year = Integer.parseInt(parts[2]);
				LocalDate dateOfBirth = new LocalDate(year, month, day);

				// Reconstruct a Patient
				Patient p = new Patient(c.getInt(0), 
						c.getString(1), 
						c.getString(2), 
						c.getString(3), 
						dateOfBirth);

				patients.add(p);
				Log.i("PATIENT", "Patient retrieved from the database");
				c.moveToNext();
			}
		} 
		c.close();
		return patients;
	}

	// Used to upsert survey/medication/sensor configurations.  
	public void setConfiguration(int patientID, Configuration configuration) {

		openDatabase();
		ContentValues values = new ContentValues();

		values.put(this.PATIENT_ID, patientID);
		values.put(this.ENABLED, configuration.isEnabled());


		//TODO : Normalize into multiple columns--recommended that you follow the format of Patient app

		// String representation of the days of the week for the reminder
		// Set the character for the weekday index (0==Sun, 6==Sat) as 1 if it 
		// is in the frequency, and 0 if it is not.
		Schedule schedule = configuration.getSchedule();
		values.put(this.EVERYXWEEKS, schedule.getWeekRecurrence());

		StringBuilder frequency = new StringBuilder("0000000");
		for(DayEnum day : schedule.getDays()) {
			frequency.setCharAt(day.getWeekPosition(), '1');
		}
		values.put(this.FREQUENCY, frequency.toString());

		// String representation of the times for the reminder.
		// Format == "HH:MM,HH:MM,HH:MM" e.g. CSV of <hour>:<minute> strings
		String times = "";
		for(LocalTime time : schedule.getTimes()) {
			times += (time.getHourOfDay() + ":" + time.getMinuteOfHour() + ",");
		}
		times = times.substring(0, times.length() - 1); // Cutoff the final comma
		values.put(this.TIMES, times);

		switch (configuration.getType()) {

		case MEDICATION:

			Medication medication = ((MedicationConfiguration)configuration).getMedication();
			int medicationID = configuration.getId();

			// This is a new MedicationConfiguration
			if(medicationID < 0) {

				medicationID = 0;

				// The medicationID must be incremented
				String medicationIDQuery = "SELECT MAX(" + this.MEDICATION_ID + ") FROM " + this.MEDICATION_TABLE;
				Cursor c1 = database.rawQuery(medicationIDQuery, null);

				if (c1.moveToFirst()){
					medicationID = c1.getInt(0); 	
					medicationID++;
				}
				c1.close();
			}

			values.put(this.MEDICATION_ID, medicationID);
			values.put(this.NAME, medication.getName());
			values.put(this.FORM, medication.getForm());
			values.put(this.DOSAGE, medication.getDosage());
			values.put(this.UNITS, medication.getUnits());
			values.put(this.QUANTITY, medication.getQuantity());

			database.insertWithOnConflict(this.MEDICATION_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
			break;

		case PHYSICAL_REPORT:

			// TODO : ADD UDID INTO TABLE

			values.put(this.SENSOR_ENUM_ID, configuration.getId());
			values.put(this.REMINDERS_ENABLED, configuration.isRemindersEnabled());

			database.insertWithOnConflict(this.PHYSICAL_REPORT_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
			break;

		case SURVEY:

			values.put(this.SURVEY_ENUM_ID, configuration.getId());
			values.put(this.REMINDERS_ENABLED, configuration.isRemindersEnabled());

			database.insertWithOnConflict(this.SURVEY_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
			break;

		default:
			break;
		}
	}

	// Does what it says.  The configs will be of the same type when retrieved. (retrieved for currentPatient)
	public List<? extends Configuration> getConfigurations(int patientID, TypeEnum type) {

		// Assumes the local currentPatientID is to be used when retrieving Configurations

		openDatabase();
		Cursor c;

		switch (type) {

		case MEDICATION:

			c = database.query(this.MEDICATION_TABLE, 
					null, 
					this.PATIENT_ID + " = " + patientID, 
					null, 
					null, null, null);

			ArrayList<MedicationConfiguration> medicationConfigurations = new ArrayList<MedicationConfiguration>();

			if (c.moveToFirst()){
				while (!c.isAfterLast()) {

					int id = c.getInt(1);

					boolean enabled = false;
					if (c.getInt(2) == 1) {
						enabled = true;
					}

					Schedule schedule = new Schedule(c.getInt(10), getDaysFromString(c.getString(3)),getTimesFromString(c.getString(4)));
					Medication medication = new Medication(
							c.getString(5), 
							c.getString(6), 
							c.getDouble(7), 
							c.getString(8), 
							c.getDouble(9));

					medicationConfigurations.add(new MedicationConfiguration(patientID, id, enabled, schedule, medication));

					Log.i("CONFIGURATION", "MedicationConfiguration retrieved from the database");
					c.moveToNext();
				}
			} 
			c.close();
			return medicationConfigurations;

		case PHYSICAL_REPORT:

			c = database.query(this.PHYSICAL_REPORT_TABLE, 
					null, 
					this.PATIENT_ID + " = " + patientID, 
					null, 
					null, null, null);

			ArrayList<PhysicalReportConfiguration> sensorConfigurations = new ArrayList<PhysicalReportConfiguration>();
			if (c.moveToFirst()){
				while (!c.isAfterLast()) {
					int id = c.getInt(1);

					boolean enabled = false;
					if (c.getInt(2) == 1) {
						enabled = true;
					}
					boolean remindersEnabled = false;
					if (c.getInt(3) == 1) {
						remindersEnabled = true;
					}

					Schedule schedule = new Schedule(c.getInt(7), getDaysFromString(c.getString(4)),getTimesFromString(c.getString(5)));
					sensorConfigurations.add(new PhysicalReportConfiguration(patientID, id, enabled, remindersEnabled, schedule));

					Log.i("CONFIGURATION", "PhysicalReportConfiguration retrieved from the database");
					c.moveToNext();
				}
			} 
			c.close();
			return sensorConfigurations;

		case SURVEY:

			c = database.query(this.SURVEY_TABLE, 
					null, 
					this.PATIENT_ID + " = " + patientID, 
					null, 
					null, null, null);

			ArrayList<SurveyConfiguration> surveyConfigurations = new ArrayList<SurveyConfiguration>();
			if (c.moveToFirst()){
				while (!c.isAfterLast()) {

					int id = c.getInt(1);

					boolean enabled = false;
					if (c.getInt(2) == 1) {
						enabled = true;
					}
					boolean remindersEnabled = false;
					if (c.getInt(3) == 1) {
						remindersEnabled = true;
					}

					Schedule schedule = new Schedule(c.getInt(6), getDaysFromString(c.getString(4)),getTimesFromString(c.getString(5)));
					surveyConfigurations.add(new SurveyConfiguration(patientID, id, enabled, remindersEnabled, schedule));

					Log.i("CONFIGURATION", "SurveyConfiguration retrieved from the database");
					c.moveToNext();
				}
			} 
			c.close();
			return surveyConfigurations;

		case PRN:

			c = database.query(this.PRN_TABLE,
					null, 
					this.PATIENT_ID + " = " + patientID, 
					null, 
					null, null, null);
			ArrayList<PRNMedicationConfiguration> prnMedicationConfigurations = new ArrayList<PRNMedicationConfiguration>();

			if (c.moveToFirst()){
				while (!c.isAfterLast()) {

					int id = c.getInt(1);

					boolean enabled = false;
					if (c.getInt(2) == 1) {
						enabled = true;
					}

					Schedule schedule = new Schedule(c.getInt(10), getDaysFromString(c.getString(3)),getTimesFromString(c.getString(4)));
					PRNMedication prnMedication = new PRNMedication(
							c.getString(5),
							c.getString(6), 
							c.getDouble(7), 
							c.getString(8), 
							c.getDouble(9), 
							c.getDouble(10),
							c.getDouble(11));

					prnMedicationConfigurations.add(new PRNMedicationConfiguration(patientID, id, enabled, schedule, prnMedication));

					Log.i("CONFIGURATION", "MedicationConfiguration retrieved from the database");
					c.moveToNext();
				}
			} 
			c.close();
			return prnMedicationConfigurations;

		default:
			return null;
		}
	}

	//Convenience method from the DB entry to a list of days
	private ArrayList<DayEnum> getDaysFromString(String frequency) {
		ArrayList<DayEnum> days = new ArrayList<DayEnum>();
		for(int i=0; i<7; i++) {
			if(frequency.charAt(i) == '1') {
				DayEnum day = DayEnum.getEnum(i);
				days.add(day);
			}
		}
		return days;
	}

	//Convenience method from the DB entry to a list of localTimes
	private ArrayList<LocalTime> getTimesFromString(String timeString) {
		ArrayList<LocalTime> times = new ArrayList<LocalTime>();
		String[] parts = timeString.split(",");
		for(String time : parts) {

			String[] timeComponents = time.split(":");
			int hour = Integer.parseInt(timeComponents[0]);
			int minute = Integer.parseInt(timeComponents[1]);
			LocalTime t = new LocalTime(hour, minute);
			times.add(t);
		}
		return times;
	}

	// TODO : DELETION
	public void removeMedication(MedicationConfiguration medication) {

	}

	// TODO : Replace this with the lookup provided by Michael
	public List<Medication> lookupMedication(String s) {

		ArrayList<Medication> matches = new ArrayList<Medication>();

		String query = "SELECT * FROM " + this.MEDICATION_LOOKUP + " WHERE " + this.NAME + " LIKE " + s +"%";
		Cursor c = database.rawQuery(query, null);

		if (c.moveToFirst()) {
			while(!c.isAfterLast()) {

				Medication medication = new Medication(
						c.getString(1), 
						c.getString(2), 
						c.getInt(3), 
						c.getString(4), 
						c.getInt(5));

				matches.add(medication);
				c.moveToNext();
			}
			c.close();
			return matches;

		} else {
			c.close();
			return null;
		}
	}

	// These should all be setup when a new patient is created
	private void initializePatientConfigurations(int patientID) {

		for(SurveyEnum surveyID : SurveyEnum.values()) {
			ContentValues surveyValues = new ContentValues();
			surveyValues.put(this.PATIENT_ID, patientID);
			surveyValues.put(this.SURVEY_ENUM_ID, surveyID.getsurveyID());
			surveyValues.put(this.ENABLED, 0);
			surveyValues.put(this.REMINDERS_ENABLED, 0);
			surveyValues.put(this.FREQUENCY, "1111111");
			surveyValues.put(this.TIMES, "12:0");
			surveyValues.put(this.EVERYXWEEKS, 1);
			database.insert(this.SURVEY_TABLE, null, surveyValues);
			Log.i("PATIENT", "Configuration added!");
		}
		for(SensorEnum sensorID : SensorEnum.values()) {
			ContentValues sensorValues = new ContentValues();
			sensorValues.put(this.PATIENT_ID, patientID);
			sensorValues.put(this.SENSOR_ENUM_ID, sensorID.getsensorID());
			sensorValues.put(this.ENABLED, 0);
			sensorValues.put(this.REMINDERS_ENABLED, 0);
			sensorValues.put(this.FREQUENCY, "1111111");
			sensorValues.put(this.TIMES, "12:0");
			sensorValues.put(this.EVERYXWEEKS, 1);
			database.insert(this.PHYSICAL_REPORT_TABLE, null, sensorValues);
			Log.i("PATIENT", "Configuration added!");
		}

	}

	@SuppressWarnings("unchecked")
	public void saveConfigurationToExternal(int patientID) {

		ArrayList<SurveyConfiguration> surveyConfigurations = 
				(ArrayList<SurveyConfiguration>) this.getConfigurations(patientID, TypeEnum.SURVEY);
		ArrayList<PhysicalReportConfiguration> sensorConfigurations = 
				(ArrayList<PhysicalReportConfiguration>) this.getConfigurations(patientID, TypeEnum.PHYSICAL_REPORT);
		ArrayList<MedicationConfiguration> medicationConfigurations = 
				(ArrayList<MedicationConfiguration>) this.getConfigurations(patientID, TypeEnum.MEDICATION);

		ArrayList<Configuration> configurations = new ArrayList<Configuration>();
		configurations.addAll(surveyConfigurations);
		configurations.addAll(sensorConfigurations);
		configurations.addAll(medicationConfigurations);

		JSONObject json = new JSONObject();

		try {

			json.put("configurationVersion", updateConfigurationVersion(patientID));
			json.put("patient", getPatient(patientID).toPatientApplicationJSON());

			JSONArray jsonConfigurations = new JSONArray();

			for(Configuration c : configurations) {
				jsonConfigurations.put(c.toConfigurationJSON());
			}

			json.put("configurations", jsonConfigurations);

		} catch(JSONException jse) {
			Log.e("ERROR_WITH_JSON_EXPORT",jse.toString());
			return;
		}
		try {
			FileWriter fw = new FileWriter(getConfigFile());
			fw.write(json.toString());
			fw.close();
			System.out.println(json);
		} catch (IOException e) {
			Log.e("EXPORT", "Error exporting file :" + e.getMessage());
		}
	}

	/**
	 * This is called to set the configuration JSON version number so that the Patient application knows it should
	 * update its configurations to match any changes.
	 * 
	 * @return The updated configuration version number for the currently exporting patient
	 */
	private int updateConfigurationVersion(int patientID) {

		String getVersion = "SELECT " + VERSION + " FROM " + VERSION_TABLE + " WHERE " +  PATIENT_ID + "=" + patientID;
		Cursor c = database.rawQuery(getVersion, null);

		int newVersion = 1;
		if(c.moveToNext()) {
			newVersion = c.getInt(c.getColumnIndex(VERSION)) + 1;
		}

		ContentValues values = new ContentValues();
		values.put(this.VERSION, newVersion);
		values.put(this.PATIENT_ID, patientID);

		this.openDatabase();
		database.insertWithOnConflict(this.VERSION_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);

		System.out.println("EXPORT CONFIGURATION VERSION: " + newVersion);
		c.close();
		return newVersion;
	}

	public static File getConfigFile() {

		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			File file = new File(Environment.getExternalStorageDirectory()+"/../extSdCard", "shConfiguration");
			file.mkdirs();
			if (!file.exists()) {
				Log.e("CONFIGURE", "Directory not existing!");
				Log.e("CONFIGURE", "Switching to Internal Storage for Testing...");
				file = new File(Environment.getExternalStorageDirectory(), "shConfiguration");
				file.mkdirs();
			}
			File cfg = new File(file,"shConfiguration.json");
			return cfg;
		}
		return null;
	}

}
